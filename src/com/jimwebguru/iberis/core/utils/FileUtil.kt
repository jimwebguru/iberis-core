package com.jimwebguru.iberis.core.utils

import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import java.io.FileOutputStream

import java.io.File
import java.io.InputStream

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object FileUtil
{
	fun createFile(filePath: String, inputStream: InputStream?)
	{
		var fileOutputStream: FileOutputStream? = null

		try
		{
			fileOutputStream = FileOutputStream(File(filePath))

			val bytes = ByteArray(1024)

			var read = inputStream!!.read(bytes)

			while (read != -1)
			{
				fileOutputStream.write(bytes, 0, read)

				read = inputStream.read(bytes)
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}
		finally
		{
			if (inputStream != null)
			{
				try
				{
					inputStream.close()
				}
				catch (ex: Exception)
				{
					GeneralUtil.logError(ex)
				}

			}

			if (fileOutputStream != null)
			{
				try
				{
					fileOutputStream.close()
				}
				catch (ex: Exception)
				{
					GeneralUtil.logError(ex)
				}

			}
		}
	}

	fun createFileAndDirectory(directoryPath: String, filePath: String, inputStream: InputStream?)
	{
		var fileOutputStream: FileOutputStream? = null

		try
		{
			val dir = File(directoryPath)

			if (!dir.exists())
			{
				dir.mkdirs()
			}

			if (dir.exists())
			{
				fileOutputStream = FileOutputStream(File(filePath))

				val bytes = ByteArray(1024)

				var read = inputStream!!.read(bytes)

				while (read != -1)
				{
					fileOutputStream.write(bytes, 0, read)

					read = inputStream.read(bytes)
				}
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}
		finally
		{
			if (inputStream != null)
			{
				try
				{
					inputStream.close()
				}
				catch (ex: Exception)
				{
					GeneralUtil.logError(ex)
				}

			}

			if (fileOutputStream != null)
			{
				try
				{
					fileOutputStream.close()
				}
				catch (ex: Exception)
				{
					GeneralUtil.logError(ex)
				}

			}
		}
	}
}
