package com.jimwebguru.iberis.core.utils

import com.jimwebguru.iberis.core.persistence.FileExtension
import com.jimwebguru.iberis.core.persistence.MimeType
import com.jimwebguru.iberis.core.utils.db.DaoUtil

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class PersistenceUtil
{
	private val daoUtil = DaoUtil()

	fun getMimeType(contentType: String): MimeType
	{
		val mimeTypes = this.daoUtil.find<MimeType>("SELECT OBJECT(o) FROM MimeType o WHERE o.name = '$contentType'")

		if (mimeTypes != null && mimeTypes.size > 0)
		{
			return mimeTypes[0]
		}
		else
		{
			val mimeType = MimeType()
			mimeType.name = contentType

			this.daoUtil.create(mimeType)

			return mimeType
		}
	}

	fun getFileExtension(extension: String): FileExtension?
	{
		val fileExtensions = this.daoUtil.find<FileExtension>("SELECT OBJECT(o) FROM FileExtension o WHERE o.name = '$extension'")

		if (fileExtensions != null && fileExtensions.size > 0)
		{
			return fileExtensions[0]
		}
		else
		{
			/*FileExtension fileExtension = new FileExtension();
			fileExtension.setName(extension);

			this.daoUtil.create(fileExtension);

			return fileExtension;*/
		}

		return null
	}
}
