package com.jimwebguru.iberis.core.utils

import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.thoughtworks.xstream.XStream
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.InputSource

import javax.xml.bind.JAXBContext
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.*
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory
import java.io.StringReader
import java.io.StringWriter

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class XmlUtil
{
	private var jc: JAXBContext? = null

	constructor()
	{
		/*try
		{
			this.jc = JAXBContext.newInstance("com.jimwebguru.iberis.core")
		}
		catch(JAXBException ex)
		{
			GeneralUtil.logError(ex)
		} */
	}

	/**
	 *
	 * DOM Methods
	 *
	 */
	val newDocument: Document?
		get()
		{
			try
			{
				val df = DocumentBuilderFactory.newInstance()
				val builder = df.newDocumentBuilder()

				return builder.newDocument()
			}
			catch (ex: Exception)
			{
				GeneralUtil.logError(ex)
			}

			return null
		}

	fun nodeToXml(node: Node): String?
	{
		try
		{
			val transFactory = TransformerFactory.newInstance()
			val transformer = transFactory.newTransformer()
			val buffer = StringWriter()
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes")
			transformer.transform(DOMSource(node), StreamResult(buffer))

			return buffer.toString()
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		return null
	}

	fun getElementNodeValue(element: Element, nodeName: String): String?
	{
		val childNode = this.getChildNodeByName(element, nodeName)

		return childNode?.textContent

	}

	fun removeChildNodesByAttribute(node: Node, attributeName: String)
	{
		if (node.hasChildNodes())
		{
			for (i in 0 until node.childNodes.length)
			{
				val childNode = node.childNodes.item(i) as Element

				if (childNode.hasAttribute(attributeName))
				{
					node.removeChild(childNode)
				}
			}
		}
	}

	fun removeChildNode(node: Node, childNodeName: String)
	{
		if (node.hasChildNodes())
		{
			val childNode = this.getChildNodeByName(node, childNodeName)

			if (childNode != null)
			{
				if (childNodeName.contains("."))
				{
					childNode.parentNode.removeChild(childNode)
				}
				else
				{
					node.removeChild(childNode)
				}
			}
		}
	}

	fun getChildNodeByName(parentNode: Node?, childNodeName: String): Node?
	{
		if (parentNode != null)
		{
			val xpath = XPathFactory.newInstance().newXPath()

			try
			{
				val potentialNodeList = xpath.evaluate(childNodeName.replace("\\.".toRegex(), "/"), parentNode, XPathConstants.NODESET)

				if (potentialNodeList != null)
				{
					return (potentialNodeList as NodeList).item(0)
				}
			}
			catch (ex: Exception)
			{
				GeneralUtil.logError(ex)
			}

		}

		return null
	}

	fun createChildNode(parentElement: Node, propertyName: String): Node?
	{
		var parentElement = parentElement
		val doc = parentElement.ownerDocument

		val elementNames = propertyName.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

		var returnNode: Node? = null

		for (i in elementNames.indices)
		{
			val elementName = elementNames[i]

			var childNode = this.getChildNodeByName(parentElement, elementName)

			if (childNode == null)
			{
				childNode = doc.createElement(elementName)

				parentElement.appendChild(childNode)
			}

			returnNode = childNode
		}

		return returnNode
	}

	fun getNodePath(node: Node): String
	{
		var nodePath = node.nodeName

		if (node.parentNode != null)
		{
			nodePath = this.getNodePath(node.parentNode) + "." + nodePath
		}

		return nodePath
	}

	/**
	 *
	 * JAXB Methods
	 *
	 */

	fun objectToXml(obj: Any): String?
	{
		try
		{
			val m = this.jc!!.createMarshaller()
			//m.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );

			val sw = StringWriter()
			m.marshal(obj, sw)

			val xml = sw.toString()
			sw.close()

			return xml
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		return null
	}

	fun objectToDocument(obj: Any): Document?
	{
		try
		{
			val df = DocumentBuilderFactory.newInstance()
			val builder = df.newDocumentBuilder()

			return builder.parse(InputSource(StringReader(objectToXml(obj)!!)))
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		return null
	}

	fun xmlToObject(xml: String): Any?
	{
		try
		{
			val unmarshaller = this.jc!!.createUnmarshaller()

			return unmarshaller.unmarshal(InputSource(StringReader(xml)))
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		return null
	}

	fun documentToObject(xmlDoc: Document): Any?
	{
		try
		{
			val source = DOMSource(xmlDoc)

			val stringWriter = StringWriter()

			val result = StreamResult(stringWriter)

			val factory = TransformerFactory.newInstance()
			val transformer = factory.newTransformer()
			transformer.transform(source, result)

			return this.xmlToObject(stringWriter.buffer.toString())
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		return null
	}

	fun prettyPrint(xml: String): String
	{
		try
		{
			val dbf = DocumentBuilderFactory.newInstance()
			dbf.isValidating = false
			val db = dbf.newDocumentBuilder()
			val sourceXML = InputSource(StringReader(xml))
			val xmlDoc = db.parse(sourceXML)
			val e = xmlDoc.documentElement
			e.normalize()

			val outWriter = StringWriter()
			val result = StreamResult(outWriter)

			val tf = TransformerFactory.newInstance().newTransformer()
			tf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes")
			tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8")
			tf.setOutputProperty(OutputKeys.INDENT, "yes")
			tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2")
			tf.transform(DOMSource(e), result)

			val sb = outWriter.buffer

			return sb.toString()
		}
		catch (ex: Exception)
		{

		}

		return ""
	}

	fun isWellFormed(xml: String): Boolean
	{
		try
		{
			val dbf = DocumentBuilderFactory.newInstance()
			dbf.isValidating = false
			dbf.isNamespaceAware = true

			val db = dbf.newDocumentBuilder()
			val sourceXML = InputSource(StringReader(xml))
			db.parse(sourceXML)

			return true
		}
		catch (ex: Exception)
		{

		}

		return false
	}

	companion object
	{

		fun <T> deepCopy(entityClass: Class<T>, obj: Any): T?
		{
			val xstream = XStream()

			return entityClass.cast(xstream.fromXML(xstream.toXML(obj)))
		}
	}
}
