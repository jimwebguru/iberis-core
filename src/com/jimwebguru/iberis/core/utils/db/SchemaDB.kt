package com.jimwebguru.iberis.core.utils.db

import com.jimwebguru.iberis.core.persistence.PropertyType
import com.jimwebguru.iberis.core.persistence.SystemUnit
import com.jimwebguru.iberis.core.persistence.UnitProperty

import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class SchemaDB
{
	private val daoUtil = DaoUtil()

	val lastInsertQuery: String
		get() = "SELECT LAST_INSERT_ID()"

	fun createUnitTable(unit: SystemUnit, unitProperties: ArrayList<UnitProperty>, primaryProperty: UnitProperty, primary2Property: UnitProperty?): Boolean
	{
		var updated = 0

		if (unit.tableName != null && unit.tableName.isNotEmpty())
		{
			val createBuilder = StringBuilder()
			createBuilder.append("CREATE TABLE ")
			createBuilder.append(unit.tableName)
			createBuilder.append(" (")

			for (unitProperty in unitProperties)
			{
				createBuilder.append(this.getFieldStructure(unitProperty))

				if (unitProperty.propertyId == primaryProperty.propertyId || primary2Property != null && unitProperty.propertyId == primary2Property.propertyId)
				{
					createBuilder.append(" AUTO_INCREMENT")
				}

				createBuilder.append(",")
			}

			createBuilder.append("PRIMARY KEY (")
			createBuilder.append(primaryProperty.propertyName)

			if (primary2Property != null)
			{
				createBuilder.append(",")
				createBuilder.append(primary2Property.propertyName)
			}

			createBuilder.append(")")

			createBuilder.append(") ENGINE=InnoDB")

			updated = this.daoUtil.updateNative(createBuilder.toString())

			/*if(updated == 0)
			{
				this.daoUtil.updateNative("ALTER TABLE " + unit.getTableName() + " AUTO_INCREMENT=1");
			}*/
		}

		//Create table will return 0 updated rows
		return updated == 0
	}

	fun renameUnitTable(unit: SystemUnit, oldTableName: String): Boolean
	{
		if (unit.tableName != null && unit.tableName != oldTableName && unit.tableName.isNotEmpty())
		{
			val renameBuilder = StringBuilder()
			renameBuilder.append("RENAME TABLE ")
			renameBuilder.append(oldTableName)
			renameBuilder.append(" TO ")
			renameBuilder.append(unit.tableName)

			val updated = this.daoUtil.updateNative(renameBuilder.toString())

			//Rename table will return 0 updated rows
			return updated == 0
		}

		return true
	}

	fun removeUnitTable(tableName: String?): Boolean
	{
		if (tableName != null && tableName.isNotEmpty())
		{
			val dropBuilder = StringBuilder()
			dropBuilder.append("DROP TABLE IF EXISTS ")
			dropBuilder.append(tableName)

			val updated = this.daoUtil.updateNative(dropBuilder.toString())

			return updated == 0
		}

		return true
	}

	fun createUnitProperty(unit: SystemUnit, unitProperty: UnitProperty): Boolean
	{
		var updated = 0

		if (unit.tableName != null && unit.tableName.isNotEmpty())
		{
			val alterBuilder = StringBuilder()
			alterBuilder.append("ALTER TABLE ")
			alterBuilder.append(unit.tableName)
			alterBuilder.append(" ADD ")
			alterBuilder.append(this.getFieldStructure(unitProperty))

			updated = this.daoUtil.updateNative(alterBuilder.toString())
		}

		//add column will return 0 updated rows
		return updated == 0
	}


	fun modifyUnitProperty(unit: SystemUnit, unitProperty: UnitProperty, oldPropertyName: String?): Boolean
	{
		var updated = 0

		if (unit.tableName != null && unit.tableName.isNotEmpty())
		{
			val alterBuilder = StringBuilder()
			alterBuilder.append("ALTER TABLE ")
			alterBuilder.append(unit.tableName)

			if (oldPropertyName != null && oldPropertyName.isNotEmpty() && unitProperty.propertyName != oldPropertyName)
			{
				alterBuilder.append(" CHANGE ")
				alterBuilder.append(oldPropertyName)
				alterBuilder.append(" ")
			}
			else
			{
				alterBuilder.append(" MODIFY COLUMN ")
			}

			alterBuilder.append(this.getFieldStructure(unitProperty))

			updated = this.daoUtil.updateNative(alterBuilder.toString())
		}

		//modify column will return 0 updated rows
		return updated == 0
	}

	fun removeUnitProperty(unit: SystemUnit, unitProperty: UnitProperty): Boolean
	{
		var updated = 0

		if (unit.tableName != null && unit.tableName.isNotEmpty())
		{
			val alterBuilder = StringBuilder()
			alterBuilder.append("ALTER TABLE ")
			alterBuilder.append(unit.tableName)

			alterBuilder.append(" DROP ")
			alterBuilder.append(unitProperty.propertyName)

			updated = this.daoUtil.updateNative(alterBuilder.toString())
		}

		//drop column will return 0 updated rows
		return updated == 0
	}

	private fun getFieldStructure(unitProperty: UnitProperty): String
	{
		val fieldBuilder = StringBuilder()

		fieldBuilder.append(unitProperty.propertyName)

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId)
		{
			fieldBuilder.append(" BIGINT(20) UNSIGNED DEFAULT 0")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.BOOLEAN.propertyTypeId)
		{
			fieldBuilder.append(" BIT(1) DEFAULT b'0'")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.BYTE.propertyTypeId)
		{
			fieldBuilder.append(" TEXT")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.CALENDAR.propertyTypeId)
		{
			fieldBuilder.append(" DATETIME")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.DATE.propertyTypeId)
		{
			fieldBuilder.append(" DATETIME")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
		{
			fieldBuilder.append(" BIGINT(20) UNSIGNED DEFAULT 0,")
			fieldBuilder.append(unitProperty.propertyName + "InstanceId BIGINT(20) UNSIGNED DEFAULT 0")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.READONLY.propertyTypeId)
		{
			fieldBuilder.append(" BIGINT(20) UNSIGNED")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.MONEY.propertyTypeId)
		{
			fieldBuilder.append(" DECIMAL(12,2)")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.MULTI_LOOKUP.propertyTypeId)
		{

		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.NUMBER.propertyTypeId)
		{
			fieldBuilder.append(" BIGINT(20)")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.SELECTION.propertyTypeId)
		{
			fieldBuilder.append(" VARCHAR(200)")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.TEXT.propertyTypeId)
		{
			fieldBuilder.append(" VARCHAR(" + unitProperty.textLength + ")")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.TEXT_AREA.propertyTypeId)
		{
			fieldBuilder.append(" VARCHAR(" + unitProperty.textLength + ")")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.EDITOR.propertyTypeId)
		{
			fieldBuilder.append(" TEXT")
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.CODE.propertyTypeId)
		{
			fieldBuilder.append(" TEXT")
		}

		return fieldBuilder.toString()
	}
}
