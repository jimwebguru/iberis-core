package com.jimwebguru.iberis.core.utils.db

import com.jimwebguru.iberis.core.persistence.JPA_EMF
import org.eclipse.persistence.config.QueryHints
import org.eclipse.persistence.config.ResultType

import java.io.Serializable
import java.util.ArrayList
import java.util.logging.Level
import java.util.logging.Logger
/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class DaoUtil : Serializable
{
	fun <T> find(entityClass: Class<T>, id: Any): T?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val result = em.find(entityClass, id)

			if (result != null)
			{
				return entityClass.cast(result)
			}
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun <T> findAll(entityClass: Class<T>): List<T>?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val cq = em.criteriaBuilder.createQuery()
			cq.select(cq.from(entityClass))

			val q = em.createQuery(cq)

			return q.resultList as List<T>
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun <T> find(jpaQuery: String): List<T>?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val q = em.createQuery(jpaQuery)

			return q.resultList as List<T>
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun <T> find(jpaQuery: String, limit: Int): List<T>?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val q = em.createQuery(jpaQuery).setMaxResults(limit)

			return q.resultList as List<T>
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun <T> find(jpaQuery: String, parameters: ArrayList<QueryParameter>): List<T>?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val q = em.createQuery(jpaQuery)

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(parameter.parameterName, parameter.parameterValue)
				}
			}

			return q.resultList as List<T>
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun <T> findRange(range: IntArray, entityClass: Class<*>): List<T>?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val cq = em.criteriaBuilder.createQuery()
			cq.select(cq.from(entityClass))

			val q = em.createQuery(cq)
			q.maxResults = range[1]
			q.firstResult = range[0]

			return q.resultList as List<T>
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun <T> findRange(range: IntArray, jpaQuery: String): List<T>?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val q = em.createQuery(jpaQuery)

			q.maxResults = range[1]
			q.firstResult = range[0]

			return q.resultList as List<T>
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun <T> findRange(range: IntArray, jpaQuery: String, parameters: ArrayList<QueryParameter>): List<T>?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val q = em.createQuery(jpaQuery)

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(parameter.parameterName, parameter.parameterValue)
				}
			}

			q.maxResults = range[1]
			q.firstResult = range[0]

			return q.resultList as List<T>
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun count(entityClass: Class<*>): Long
	{
		val em = JPA_EMF.get().createEntityManager()

		var count: Long = 0

		try
		{
			val cq = em.criteriaBuilder.createQuery()
			val rt = cq.from(entityClass)

			cq.select(em.criteriaBuilder.count(rt))

			val q = em.createQuery(cq)

			count = q.singleResult as Long
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return count
	}

	@JvmOverloads
	fun count(jpaQuery: String, parameters: ArrayList<QueryParameter> = ArrayList()): Long
	{
		val em = JPA_EMF.get().createEntityManager()

		var count: Long = 0

		try
		{
			val q = em.createQuery(jpaQuery)

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(parameter.parameterName, parameter.parameterValue)
				}
			}

			count = q.singleResult as Long
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return count
	}

	fun delete(`object`: Any): Boolean
	{
		val em = JPA_EMF.get().createEntityManager()

		var success = false

		try
		{
			em.transaction.begin()
			val oRemove = em.merge(`object`)
			em.remove(oRemove)
			em.transaction.commit()

			success = true
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
			em.transaction.rollback()
		}
		finally
		{
			em.close()
		}

		return success
	}

	fun update(`object`: Any): Boolean
	{
		val em = JPA_EMF.get().createEntityManager()

		var success = false

		try
		{
			em.transaction.begin()
			em.merge(`object`)
			em.transaction.commit()

			success = true
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
			em.transaction.rollback()
		}
		finally
		{
			em.close()
		}

		return success
	}

	fun updateMultiple(entities: ArrayList<*>): Boolean
	{
		val em = JPA_EMF.get().createEntityManager()

		var success = false

		try
		{
			em.transaction.begin()

			for (entity in entities)
			{
				em.merge(entity)
			}

			em.transaction.commit()

			success = true
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
			em.transaction.rollback()
		}
		finally
		{
			em.close()
		}

		return success
	}

	fun create(`object`: Any): Boolean
	{
		val em = JPA_EMF.get().createEntityManager()

		var success = false

		try
		{
			em.transaction.begin()
			em.persist(`object`)
			em.transaction.commit()

			success = true
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
			em.transaction.rollback()
		}
		finally
		{
			em.close()
		}

		return success
	}

	fun createMultiple(entities: ArrayList<*>): Boolean
	{
		val em = JPA_EMF.get().createEntityManager()

		var success = false

		try
		{
			em.transaction.begin()

			for (entity in entities)
			{
				em.persist(entity)
			}

			em.transaction.commit()

			success = true
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
			em.transaction.rollback()
		}
		finally
		{
			em.close()
		}

		return success
	}

	fun findNative(query: String): List<*>?
	{
		val em = JPA_EMF.get().createEntityManager()

		var results: List<*>? = null

		try
		{
			val q = em.createNativeQuery(query)
			q.setHint(QueryHints.RESULT_TYPE, ResultType.Map)

			results = q.resultList
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return results
	}

	fun findNative(entityClass: Class<*>, query: String): List<*>?
	{
		val em = JPA_EMF.get().createEntityManager()

		var results: List<*>? = null

		try
		{
			val q = em.createNativeQuery(query, entityClass)

			results = q.resultList
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return results
	}

	fun findNative(entityClass: Class<*>, query: String, parameters: ArrayList<QueryParameter>): List<*>?
	{
		val em = JPA_EMF.get().createEntityManager()

		var results: List<*>? = null

		try
		{
			val q = em.createNativeQuery(query)

			var count = 1

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(count, parameter.parameterValue)
					count++
				}
			}

			q.setHint(QueryHints.RESULT_TYPE, ResultType.Map)

			results = q.resultList
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return results
	}

	fun findNativeSingle(query: String, useMap: Boolean): Any?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val q = em.createNativeQuery(query)

			if (useMap)
			{
				q.setHint(QueryHints.RESULT_TYPE, ResultType.Map)
			}

			return q.singleResult
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun findNativeSingle(query: String, parameters: ArrayList<QueryParameter>, useMap: Boolean): Any?
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val q = em.createNativeQuery(query)

			var count = 1

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(count, parameter.parameterValue)
					count++
				}
			}

			if (useMap)
			{
				q.setHint(QueryHints.RESULT_TYPE, ResultType.Map)
			}

			return q.singleResult
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return null
	}

	fun findNativeRange(range: IntArray, query: String): List<*>?
	{
		val em = JPA_EMF.get().createEntityManager()

		var results: List<*>? = null

		try
		{
			val q = em.createNativeQuery(query)
			q.maxResults = range[1]
			q.firstResult = range[0]
			q.setHint(QueryHints.RESULT_TYPE, ResultType.Map)

			results = q.resultList
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return results
	}

	fun findNativeRange(range: IntArray, query: String, parameters: ArrayList<QueryParameter>): List<*>?
	{
		val em = JPA_EMF.get().createEntityManager()

		var results: List<*>? = null

		try
		{
			val q = em.createNativeQuery(query)

			var count = 1

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(count, parameter.parameterValue)
					count++
				}
			}

			q.maxResults = range[1]
			q.firstResult = range[0]
			q.setHint(QueryHints.RESULT_TYPE, ResultType.Map)

			results = q.resultList
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return results
	}

	fun insertNative(schemaDB: SchemaDB, query: String, parameters: ArrayList<QueryParameter>): Long?
	{
		val em = JPA_EMF.get().createEntityManager()

		var updateCount = 0
		var rowId = java.lang.Long.valueOf(0)

		try
		{
			em.transaction.begin()

			var q: javax.persistence.Query = em.createNativeQuery(query)

			var count = 1

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(count, parameter.parameterValue)
					count++
				}
			}

			updateCount = q.executeUpdate()

			if (updateCount == 1)
			{
				q = em.createNativeQuery(schemaDB.lastInsertQuery)

				rowId = java.lang.Long.valueOf(q.singleResult.toString())
			}

			em.transaction.commit()
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return rowId
	}

	fun insertNative(schemaDB: SchemaDB, query: String): Long?
	{
		val em = JPA_EMF.get().createEntityManager()

		var updateCount = 0
		var rowId = java.lang.Long.valueOf(0)

		try
		{
			em.transaction.begin()

			var q: javax.persistence.Query = em.createNativeQuery(query)

			updateCount = q.executeUpdate()

			if (updateCount == 1)
			{
				q = em.createNativeQuery(schemaDB.lastInsertQuery)

				rowId = java.lang.Long.valueOf(q.singleResult.toString())
			}

			em.transaction.commit()
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}

		return rowId
	}

	fun updateNative(query: String, parameters: ArrayList<QueryParameter>): Int
	{
		val em = JPA_EMF.get().createEntityManager()

		var updateCount = 0

		try
		{
			em.transaction.begin()

			val q = em.createNativeQuery(query)

			var count = 1

			for (parameter in parameters)
			{
				if (parameter.parameterName != null)
				{
					q.setParameter(count, parameter.parameterValue)
					count++
				}
			}

			updateCount = q.executeUpdate()

			em.transaction.commit()
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)

			updateCount = -1
		}
		finally
		{
			em.close()
		}

		return updateCount
	}

	fun updateNative(query: String): Int
	{
		val em = JPA_EMF.get().createEntityManager()

		var updateCount = 0

		try
		{
			em.transaction.begin()

			val q = em.createNativeQuery(query)

			updateCount = q.executeUpdate()

			em.transaction.commit()
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)

			updateCount = -1
		}
		finally
		{
			em.close()
		}

		return updateCount
	}

	fun refresh(entityClass: Class<*>, id: Any)
	{
		val em = JPA_EMF.get().createEntityManager()

		try
		{
			val result = em.find(entityClass, id)

			if (result != null)
			{
				em.refresh(result)
			}
		}
		catch (e: Exception)
		{
			Logger.getLogger(javaClass.name).log(Level.SEVERE, "exception caught", e)
		}
		finally
		{
			em.close()
		}
	}
}