package com.jimwebguru.iberis.core.utils.db
/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class QueryParameter
{
	var parameterName: String? = null
	var parameterValue: Any? = null
	var objectProperty: String? = null
	var transformPropertyFunction: String? = null
	var conditionOperator: String? = null
	var explicitCondition: String? = null
	var joinClause: String? = null
	var isExcludeFromCountQuery = false

	constructor()

	constructor(parameterName: String, parameterValue: Any)
	{
		this.parameterName = parameterName
		this.parameterValue = parameterValue
	}
}
