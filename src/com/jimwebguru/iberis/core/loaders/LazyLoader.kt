package com.jimwebguru.iberis.core.loaders

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import org.primefaces.model.LazyDataModel

import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
open class LazyLoader : LazyDataModel<Any>()
{
	var resultList: List<Any>? = null
		protected set

	protected var dUtil = DaoUtil()

	var additionalQueryParams: ArrayList<QueryParameter>? = null

	var customQuery: String? = null
	var customCountQuery: String? = null
}
