/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jimwebguru.iberis.core.loaders

import com.jimwebguru.iberis.core.persistence.*

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object DataListUtil
{
	fun getRowKey(`object`: Any): Any?
	{


		if (`object` is AdditionalEmailRecipient)
		{
			return `object`.emailRecipientId
		}

		if(`object` is Blog)
		{
			return `object`.id
		}

		if(`object` is BlogEntry)
		{
			return `object`.id
		}

		if (`object` is Canvas)
		{
			return `object`.id
		}

		if (`object` is ConditionOperator)
		{
			return `object`.operatorId.name
		}

		if (`object` is Content)
		{
			return `object`.id
		}

		if (`object` is DynamicForm)
		{
			return `object`.id
		}

		if (`object` is DynamicFormSection)
		{
			return `object`.id
		}

		if (`object` is DynamicFormField)
		{
			return `object`.id
		}

		if (`object` is DynamicFieldListItem)
		{
			return `object`.id
		}

		if (`object` is FAQ)
		{
			return `object`.id
		}

		if (`object` is File)
		{
			return `object`.id
		}

		if (`object` is FileExtension)
		{
			return `object`.extensionid
		}

		if (`object` is Form)
		{
			return `object`.formId
		}

		if (`object` is FormSubmission)
		{
			return `object`.id
		}

		if (`object` is Gallery)
		{
			return `object`.id
		}

		if (`object` is Grid)
		{
			return `object`.gridId
		}

		if (`object` is GridColumn)
		{
			return `object`.gridColumnId
		}

		if (`object` is Icon)
		{
			return `object`.id
		}

		if (`object` is Image)
		{
			return `object`.imageid
		}

		if (`object` is Layout)
		{
			return `object`.id
		}

		if (`object` is Manufacturer)
		{
			return `object`.manufacturerId
		}

		if (`object` is Menu)
		{
			return `object`.id
		}

		if (`object` is MenuItem)
		{
			return `object`.id
		}

		if (`object` is MimeType)
		{
			return `object`.typeid
		}

		if (`object` is PageItemAssignment)
		{
			return `object`.id
		}

		if (`object` is PageRegion)
		{
			return `object`.id
		}

		if (`object` is Permission)
		{
			return `object`.id
		}

		if (`object` is Presentation)
		{
			return `object`.id
		}

		if (`object` is Product)
		{
			return `object`.productId
		}

		if (`object` is ProductCatalog)
		{
			return `object`.id
		}

		if (`object` is ProductGroup)
		{
			return `object`.groupId
		}

		if (`object` is ProductImage)
		{
			return `object`.id
		}

		if (`object` is ResourceBundleItem)
		{
			return `object`.id
		}

		if (`object` is Role)
		{
			return `object`.id
		}

		if (`object` is SearchColumn)
		{
			return `object`.searchID
		}

		if (`object` is Site)
		{
			return `object`.siteID
		}

		if (`object` is SiteType)
		{
			return `object`.typeID
		}

		if (`object` is SiteMembership)
		{
			return `object`.membershipID
		}

		if (`object` is Slide)
		{
			return `object`.id
		}

		if (`object` is SystemArea)
		{
			return `object`.areaId
		}

		if (`object` is SystemUnit)
		{
			return `object`.unitID
		}

		if (`object` is Tab)
		{
			return `object`.tabId
		}

		if (`object` is Transaction)
		{
			return `object`.transID
		}

		if (`object` is UnitProperty)
		{
			return `object`.propertyId
		}

		if (`object` is UnitPropertyListItem)
		{
			return `object`.itemId
		}

		if (`object` is UnitPropertyType)
		{
			return `object`.typeId
		}

		if (`object` is UnitSiteAssociation)
		{
			return `object`.id
		}

		if (`object` is User)
		{
			return `object`.userId
		}

		if (`object` is UserGroup)
		{
			return `object`.groupId
		}

		if (`object` is Video)
		{
			return `object`.id
		}

		return if (`object` is VocabTerm)
		{
			`object`.id
		}
		else (`object` as? Vocabulary)?.id

	}

	fun getRowData(rowKey: String, listOfObjects: List<*>): Any?
	{
		for (instance in listOfObjects)
		{
			if (instance is AdditionalEmailRecipient)
			{
				if (instance.emailRecipientId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if(instance is Blog)
			{
				if(instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if(instance is BlogEntry)
			{
				if(instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if(instance is Canvas)
			{
				if(instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is ConditionOperator)
			{
				if (instance.operatorId.name == rowKey)
				{
					return instance
				}
			}

			if (instance is Content)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is DynamicForm)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is DynamicFormSection)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is DynamicFormField)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is DynamicFieldListItem)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is FAQ)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is File)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is FileExtension)
			{
				if (instance.extensionid!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Form)
			{
				if (instance.formId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is FormSubmission)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Gallery)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Grid)
			{
				if (instance.gridId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is GridColumn)
			{
				if (instance.gridColumnId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Icon)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Image)
			{
				if (instance.imageid!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Layout)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Manufacturer)
			{
				if (instance.manufacturerId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Menu)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is MenuItem)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is MimeType)
			{
				if (instance.typeid!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is PageItemAssignment)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is PageRegion)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Permission)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Presentation)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Product)
			{
				if (instance.productId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is ProductCatalog)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is ProductGroup)
			{
				if (instance.groupId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is ProductImage)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is ResourceBundleItem)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Role)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is SearchColumn)
			{
				if (instance.searchID!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Site)
			{
				if (instance.siteID!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is SiteType)
			{
				if (instance.typeID!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is SiteMembership)
			{
				if (instance.membershipID!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Slide)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is SystemArea)
			{
				if (instance.areaId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is SystemUnit)
			{
				if (instance.unitID!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Tab)
			{
				if (instance.tabId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Transaction)
			{
				if (instance.transID!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is UnitProperty)
			{
				if (instance.propertyId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is UnitPropertyListItem)
			{
				if (instance.itemId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is UnitPropertyType)
			{
				if (instance.typeId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is UnitSiteAssociation)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is User)
			{
				if (instance.userId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is UserGroup)
			{
				if (instance.groupId!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Video)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is VocabTerm)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}

			if (instance is Vocabulary)
			{
				if (instance.id!!.toString() == rowKey)
				{
					return instance
				}
			}
		}

		return null
	}
}
