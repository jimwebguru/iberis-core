package com.jimwebguru.iberis.core.loaders

import com.jimwebguru.iberis.core.utils.db.QueryParameter
import org.eclipse.persistence.internal.sessions.ArrayRecord
import org.primefaces.model.SortOrder

import java.util.ArrayList
import java.util.logging.Level
import java.util.logging.Logger

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class NativeLazyLoader(private var tableName: String, private var primaryKey: String, private var primaryKey2: String?) : LazyLoader()
{
	override fun load(first: Int, pageSize: Int, sortField: String?, sortOrder: SortOrder?, filters: Map<String, Any>?): List<Any>?
	{
		try
		{
			val nativeList: List<*>?

			val range = IntArray(2)
			range[0] = first
			range[1] = first + pageSize

			if (this.customQuery == null)
			{
				val queryBuilder = StringBuilder()
				val countBuilder = StringBuilder()

				queryBuilder.append("SELECT * FROM ")
				queryBuilder.append(this.tableName)
				queryBuilder.append(" AS o")

				countBuilder.append("SELECT COUNT(*) FROM ")
				countBuilder.append(this.tableName)
				countBuilder.append(" AS o")

				val queryParameters = ArrayList<QueryParameter>()

				if (filters != null)
				{
					if (!filters.isEmpty())
					{
						for ((key, value) in filters)
						{

							val parameterName = key.replace("\\.".toRegex(), "_")

							queryBuilder.append(if (queryBuilder.indexOf(" WHERE ") < 0) " WHERE " else " AND ")
							queryBuilder.append("UPPER(o.")
							queryBuilder.append(key)
							queryBuilder.append(") LIKE ?")

							countBuilder.append(if (countBuilder.indexOf(" WHERE ") < 0) " WHERE " else " AND ")
							countBuilder.append("UPPER(o.")
							countBuilder.append(key)
							countBuilder.append(") LIKE ?")

							queryParameters.add(QueryParameter(parameterName, value.toString().toUpperCase() + "%"))
						}
					}
				}

				if (this.additionalQueryParams != null && this.additionalQueryParams!!.size > 0)
				{
					for (queryParameter in this.additionalQueryParams!!)
					{
						if (queryParameter.joinClause != null)
						{
							if (queryBuilder.indexOf(" WHERE ") > -1)
							{
								queryBuilder.replace(queryBuilder.indexOf(" WHERE "), queryBuilder.indexOf(" WHERE ") + 7, " " + queryParameter.joinClause + " WHERE ")

								if (!queryParameter.isExcludeFromCountQuery)
								{
									countBuilder.replace(countBuilder.indexOf(" WHERE "), countBuilder.indexOf(" WHERE ") + 7, " " + queryParameter.joinClause + " WHERE ")
								}
							}
							else
							{
								queryBuilder.append(" ")
								queryBuilder.append(queryParameter.joinClause)

								if (!queryParameter.isExcludeFromCountQuery)
								{
									countBuilder.append(" ")
									countBuilder.append(queryParameter.joinClause)
								}
							}
						}

						if (queryParameter.explicitCondition != null)
						{
							queryBuilder.append(if (queryBuilder.indexOf(" WHERE ") < 0) " WHERE " else " AND ")
							queryBuilder.append(queryParameter.explicitCondition)

							if (!queryParameter.isExcludeFromCountQuery)
							{
								countBuilder.append(if (countBuilder.indexOf(" WHERE ") < 0) " WHERE " else " AND ")
								countBuilder.append(queryParameter.explicitCondition)
							}
						}
						else if (queryParameter.objectProperty != null)
						{
							queryBuilder.append(if (queryBuilder.indexOf(" WHERE ") < 0) " WHERE " else " AND ")
							queryBuilder.append(if (queryParameter.transformPropertyFunction != null) queryParameter.transformPropertyFunction!! + "(" else "")
							queryBuilder.append(if (queryParameter.joinClause == null) "o." else "")
							queryBuilder.append(queryParameter.objectProperty)
							queryBuilder.append(if (queryParameter.transformPropertyFunction != null) ")" else "")
							queryBuilder.append(" ")
							queryBuilder.append(if (queryParameter.conditionOperator != null) queryParameter.conditionOperator else "LIKE")
							queryBuilder.append(" ?")

							if (!queryParameter.isExcludeFromCountQuery)
							{
								countBuilder.append(if (queryBuilder.indexOf(" WHERE ") < 0) " WHERE " else " AND ")
								countBuilder.append(if (queryParameter.transformPropertyFunction != null) queryParameter.transformPropertyFunction!! + "(" else "")
								countBuilder.append(if (queryParameter.joinClause == null) "o." else "")
								countBuilder.append(queryParameter.objectProperty)
								countBuilder.append(if (queryParameter.transformPropertyFunction != null) ")" else "")
								countBuilder.append(" ")
								countBuilder.append(if (queryParameter.conditionOperator != null) queryParameter.conditionOperator else "LIKE")
								countBuilder.append(" ?")
							}
						}

						queryParameters.add(queryParameter)
					}
				}

				var queryOrder = " DESC"

				if (sortOrder != null && sortOrder.name == "ASCENDING")
				{
					queryOrder = " ASC"
				}

				if (sortField != null && !sortField.trim { it <= ' ' }.isEmpty())
				{
					queryBuilder.append(" ORDER BY o.")
					queryBuilder.append(sortField)
					queryBuilder.append(queryOrder)

					countBuilder.append(" ORDER BY o.")
					countBuilder.append(sortField)
					countBuilder.append(queryOrder)
				}

				nativeList = this.dUtil.findNativeRange(range, queryBuilder.toString(), queryParameters)

				this.rowCount = Integer.valueOf(this.dUtil.findNativeSingle(countBuilder.toString(), queryParameters, false)!!.toString())!!
			}
			else
			{
				nativeList = this.dUtil.findNativeRange(range, this.customQuery!!)

				this.rowCount = Integer.valueOf(this.dUtil.findNativeSingle(this.customCountQuery!!, false)!!.toString())!!
			}

			if (nativeList != null)
			{
				this.resultList = nativeList as List<Any>?
			}

		}
		catch (ex: Exception)
		{
			Logger.getLogger(GenericLazyLoader::class.java.name).log(Level.SEVERE, "exception caught", ex)
		}

		return this.resultList
	}

	override fun getRowKey(`object`: Any?): Any?
	{
		if (`object` != null)
		{
			if (`object` is ArrayRecord)
			{
				return if (this.primaryKey2 == null || this.primaryKey2!!.isEmpty())
				{
					`object`.get(this.primaryKey)
				}
				else
				{
					`object`.get(this.primaryKey).toString() + "|" + `object`.get(this.primaryKey2)
				}
			}
		}

		return null
	}

	override fun getRowData(rowKey: String?): Any?
	{
		for (`object` in this.resultList!!)
		{
			if (`object` is ArrayRecord)
			{
				if (this.primaryKey2 == null || this.primaryKey2!!.isEmpty())
				{
					if (`object`.get(this.primaryKey).toString() == rowKey)
					{
						return `object`
					}
				}
				else
				{
					if (`object`.get(this.primaryKey).toString() + "|" + `object`.get(this.primaryKey2) == rowKey)
					{
						return `object`
					}
				}
			}
		}

		return null
	}
}
