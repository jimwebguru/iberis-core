/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.layout

import java.io.Serializable
import java.util.ArrayList

import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class FormSection : Serializable
{
	var name: String? = null

	var sectionNode: Element? = null

	var fields = ArrayList<FormField>()

	constructor()

	constructor(sNode: Node)
	{
		this.sectionNode = sNode as Element

		this.name = this.sectionNode!!.getAttribute("name")

		this.parseFields()
	}

	private fun parseFields()
	{
		val fieldList = this.sectionNode!!.getElementsByTagName("field")

		for (i in 0 until fieldList.length)
		{
			if (fieldList.item(i) != null)
			{
				val node = fieldList.item(i)

				this.fields.add(FormField(node))
			}
		}
	}
}
