/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.layout

import java.io.Serializable
import java.io.StringReader
import java.util.ArrayList

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

import org.w3c.dom.Document
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.InputSource

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class FormLayout(private var layoutXml: String?) : Serializable
{
	private var layoutDocument: Document? = null
	var sections = ArrayList<FormSection>()

	init
	{

		this.parseLayoutDocument()
		this.parseSections()
	}

	private fun parseLayoutDocument()
	{
		if (this.layoutXml != null && this.layoutXml!!.isNotEmpty())
		{
			try
			{
				val df = DocumentBuilderFactory.newInstance()
				val builder = df.newDocumentBuilder()

				this.layoutDocument = builder.parse(InputSource(StringReader(layoutXml!!)))
			}
			catch (ex: Exception)
			{

			}

		}
	}

	private fun parseSections()
	{
		val sectionList = this.layoutDocument!!.getElementsByTagName("section")

		for (i in 0 until sectionList.length)
		{
			if (sectionList.item(i) != null)
			{
				val node = sectionList.item(i)

				this.sections.add(FormSection(node))
			}
		}
	}

	fun getLayoutXml(): String?
	{
		return this.layoutXml
	}

	fun setLayoutXml(layoutXml: String)
	{
		this.layoutXml = layoutXml

		this.parseLayoutDocument()
		this.parseSections()
	}
}
