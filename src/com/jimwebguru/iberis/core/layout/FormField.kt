/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.layout

import java.io.Serializable

import org.w3c.dom.Element
import org.w3c.dom.Node

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class FormField : Serializable
{
	var fieldNode: Element? = null

	var propertyId: Long? = null

	var isRequired: Boolean = false
	var isInlineLabel: Boolean = false

	var width: Int = 0

	constructor()

	constructor(fNode: Node)
	{
		this.fieldNode = fNode as Element

		this.propertyId = java.lang.Long.valueOf(this.fieldNode!!.getAttribute("propertyId"))

		if (this.fieldNode!!.getAttribute("required") != null && this.fieldNode!!.getAttribute("required") != "")
		{
			this.isRequired = java.lang.Boolean.valueOf(this.fieldNode!!.getAttribute("required"))!!
		}
		else
		{
			this.isRequired = false
		}

		if (this.fieldNode!!.getAttribute("width") != null && this.fieldNode!!.getAttribute("width") != "")
		{
			this.width = Integer.valueOf(this.fieldNode!!.getAttribute("width"))!!
		}
		else
		{
			this.width = 12
		}

		if (this.fieldNode!!.getAttribute("inline-label") != null && this.fieldNode!!.getAttribute("inline-label") != "")
		{
			this.isInlineLabel = java.lang.Boolean.valueOf(this.fieldNode!!.getAttribute("inline-label"))!!
		}
		else
		{
			this.isInlineLabel = true
		}
	}
}
