package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "systemsettings")
public class SystemSetting
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "settingsID")
	private Long id;

	@Basic(optional = false)
	@Column(name = "settingsName")
	private String name;

	@Basic(optional = false)
	@Column(name = "settingsValue")
	private String value;

	@Column(name = "description")
	private String description;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}
