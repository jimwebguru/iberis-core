/*
 *
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "forms")
public class Form implements Serializable 
{
    @Id
    @Column(name = "formId", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long formId;

    @JoinColumn(name = "unitId", referencedColumnName = "unitID")
    @ManyToOne
    private SystemUnit unit;

    @JoinColumn(name = "formType", referencedColumnName = "formTypeId")
    @ManyToOne
    private FormType formType;

    @Column(name = "name")
    private String name;

    @Column(name = "layoutXml")
    private String layoutXml;
   
    @OneToMany(mappedBy = "form")
    @OrderBy("sequence")
    private Collection<Tab> tabs;
    
    /** Creates a new instance of Form */
    public Form() 
    {
    }

    /**
     * Creates a new instance of Form with the specified values.
     * @param formId the formId of the Form
     */
    public Form(Long formId) 
    {
        this.formId = formId;
    }

    /**
     * Gets the formId of this Form.
     * @return the formId
     */
    public Long getFormId() 
    {
        return this.formId;
    }

    /**
     * Sets the formId of this Form to the specified value.
     * @param formId the new formId
     */
    public void setFormId(Long formId) 
    {
        this.formId = formId;
    }

    /**
     * Gets the unit of this Form.
     * @return the unit
     */
    public SystemUnit getUnit() 
    {
        return this.unit;
    }

    /**
     * Sets the unit of this Form to the specified value.
     * @param unit the new unit
     */
    public void setUnit(SystemUnit unit) 
    {
        this.unit = unit;
    }

    /**
     * Gets the formType of this Form.
     * @return the formType
     */
    public FormType getFormType() 
    {
        return this.formType;
    }

    /**
     * Sets the formType of this Form to the specified value.
     * @param formType the new formType
     */
    public void setFormType(FormType formType) 
    {
        this.formType = formType;
    }

    public String getLayoutXml()
    {
        return layoutXml;
    }

    public void setLayoutXml(String layoutXml)
    {
        this.layoutXml = layoutXml;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Collection<Tab> getTabs()
    {
            return tabs;
    }

    public void setTabs(Collection<Tab> tabs)
    {
            this.tabs = tabs;
    }

    /**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.formId != null ? this.formId.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Form.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Form object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Form)) 
        {
            return false;
        }
        
        Form other = (Form)object;
        
        if (this.formId != other.formId && (this.formId == null || !this.formId.equals(other.formId))) 
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.Form[formId=" + formId + "]";
    }
    
}
