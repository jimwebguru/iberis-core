/*
 * PropertyType.java
 *
 * Created on January 12, 2007, 3:08 PM
 */

package com.jimwebguru.iberis.core.persistence

enum class PropertyType

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
constructor(val propertyTypeId: Int)
{
	NONE(0),
	READONLY(1),
	TEXT(2),
	LOOKUP(3),
	DATE(4),
	NUMBER(5),
	CALENDAR(6),
	HIDDEN(7),
	MONEY(8),
	SELECTION(9),
	MULTI_LOOKUP(10),
	FLEXIBLE_LOOKUP(11),
	BOOLEAN(12),
	BYTE(13),
	TEXT_AREA(14),
	CODE(15),
	EDITOR(16)
}
