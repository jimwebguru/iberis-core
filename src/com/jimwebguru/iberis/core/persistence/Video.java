/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "videos")
public class Video implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Column(name = "width")
    private Long width;

    @JoinColumn(name = "mimetypeid", referencedColumnName = "typeid")
    private MimeType mimeType;

    @Column(name ="filePath")
    private String filePath;

	@Column(name = "customContent")
	private String customContent;

	@Column(name = "header")
	private String header;

	@Column(name = "footer")
	private String footer;

	@Column(name = "overlayCaption")
	private String overlayCaption;

	@Column(name = "videoAsBackground")
	private Boolean videoAsBackground;

	@Column(name = "showHeader")
	private Boolean showHeader;

	@Column(name = "showFooter")
	private Boolean showFooter;

	@Column(name = "backgroundType")
	private Integer backgroundType;

	@Column(name = "videoPlatform")
	private Integer videoPlatform;

	@Column(name = "minHeight")
	private Integer minHeight;

	@Column(name = "platformVideoId")
	private String platformVideoId;
	
	@JoinColumn(name = "fallbackImageId", referencedColumnName = "imageid")
    private Image fallbackImage;

	@Column(name ="overlayColor")
	private String overlayColor;

	@JoinTable(name = "videotags", joinColumns =
			{
					@JoinColumn(name = "videoId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "tagId", referencedColumnName = "id")
			})
	@ManyToMany
	private Collection<VocabTerm> videoTags;

	@JoinTable(name = "videosites", joinColumns =
			{
					@JoinColumn(name = "videoId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

    public Video()
    {
    }

    public Video(Long id)
    {
        this.id = id;
    }

    public Video(Long id, String name)
    {
        this.id = id;
        this.name = name;
    }


    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getWidth()
    {
        return width;
    }

    public void setWidth(Long width)
    {
        this.width = width;
    }

    public MimeType getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(MimeType mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

	public String getCustomContent()
	{
		return customContent;
	}

	public void setCustomContent(String customContent)
	{
		this.customContent = customContent;
	}

	public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public String getFooter()
	{
		return footer;
	}

	public void setFooter(String footer)
	{
		this.footer = footer;
	}

	public Collection<VocabTerm> getVideoTags()
	{
		return videoTags;
	}

	public void setVideoTags(Collection<VocabTerm> videoTags)
	{
		this.videoTags = videoTags;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public String getOverlayCaption()
	{
		return overlayCaption;
	}

	public void setOverlayCaption(String overlayCaption)
	{
		this.overlayCaption = overlayCaption;
	}

	public Boolean getVideoAsBackground()
	{
		return videoAsBackground;
	}

	public void setVideoAsBackground(Boolean videoAsBackground)
	{
		this.videoAsBackground = videoAsBackground;
	}

	public Integer getBackgroundType()
	{
		return backgroundType;
	}

	public void setBackgroundType(Integer backgroundType)
	{
		this.backgroundType = backgroundType;
	}

	public Integer getVideoPlatform()
	{
		return videoPlatform;
	}

	public void setVideoPlatform(Integer videoPlatform)
	{
		this.videoPlatform = videoPlatform;
	}

	public String getPlatformVideoId()
	{
		return platformVideoId;
	}

	public void setPlatformVideoId(String platformVideoId)
	{
		this.platformVideoId = platformVideoId;
	}
	
	public Image getFallbackImage()
    {
        return fallbackImage;
    }

    public void setFallbackImage(Image fImage)
    {
        this.fallbackImage = fImage;
    }

	public Integer getMinHeight()
	{
		return minHeight;
	}

	public void setMinHeight(Integer minHeight)
	{
		this.minHeight = minHeight;
	}

	public Boolean getShowHeader()
	{
		return showHeader;
	}

	public void setShowHeader(Boolean showHeader)
	{
		this.showHeader = showHeader;
	}

	public Boolean getShowFooter()
	{
		return showFooter;
	}

	public void setShowFooter(Boolean showFooter)
	{
		this.showFooter = showFooter;
	}

	public String getOverlayColor()
	{
		return overlayColor;
	}

	public void setOverlayColor(String overlayColor)
	{
		this.overlayColor = overlayColor;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Video))
        {
            return false;
        }
        Video other = (Video) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.Video[id=" + id + "]";
    }

}
