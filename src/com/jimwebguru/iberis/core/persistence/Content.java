package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "content")
@XmlRootElement
public class Content implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name="type")
	private String type;

	@JoinColumn(name = "bannerGraphic", referencedColumnName = "imageid")
	@ManyToOne
	private Image bannerGraphic;

	@Lob
	@Column(name = "header")
	private String header;

	@Lob
	@Column(name = "subHeader")
	private String subHeader;

	@Lob
	@Column(name = "body")
	private String body;

	@Lob
	@Column(name = "body2")
	private String body2;

	@Lob
	@Column(name = "body3")
	private String body3;

	@Lob
	@Column(name = "body4")
	private String body4;

	@Lob
	@Column(name = "footer")
	private String footer;

	@Lob
	@Column(name = "subFooter")
	private String subFooter;

	@Column(name = "urlAlias")
	private String urlAlias;

	@Column(name = "contentLayout")
	private String contentLayout;

	@Column(name = "headerLayout")
	private String headerLayout;

	@Column(name = "footerLayout")
	private String footerLayout;

	@JoinColumn(name = "pageTemplateId", referencedColumnName = "id")
	@ManyToOne
	private Layout pageTemplate;

	@Column(name = "showTitle")
	private Boolean showTitle;

	@Column(name = "titleSize")
	private String titleSize;

	@Column(name = "showBlockTitle")
	private Boolean showBlockTitle;

	@Column(name = "blockTitleSize")
	private String blockTitleSize;

	@Column(name = "showTitleAboveBanner")
	private Boolean showTitleAboveBanner;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	@Column(name = "customContentTemplate")
	private String customContentTemplate;

	@JoinColumn(name = "galleryId", referencedColumnName = "id")
	@ManyToOne
	private Gallery gallery;

	@JoinColumn(name = "presentationId", referencedColumnName = "id")
	@ManyToOne
	private Presentation presentation;

	@JoinColumn(name = "unitId", referencedColumnName = "unitID")
	@ManyToOne
	private SystemUnit unit;

	@JoinColumn(name = "unitGridId", referencedColumnName = "gridId")
	@ManyToOne
	private Grid unitGrid;

	@Column(name = "availableGlobal")
	private Boolean availableGlobal;

	@Column(name = "printable")
	private Boolean printable;

	@Column(name = "showFilesDownload")
	private Boolean showFilesDownload;

	@Column(name = "filesHeaderText")
	private String filesHeaderText;

	@OneToMany(mappedBy = "content", fetch = FetchType.EAGER)
	@OrderBy("sequence")
	private Collection<ContentFile> contentFiles;

	@JoinTable(name = "contentsites", joinColumns =
			{
					@JoinColumn(name = "contentId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	@Column(name = "metaTitle")
	private String metaTitle;

	@Column(name = "metaDescription")
	private String metaDescription;

	@Column(name = "allowEditorStyles")
	private Boolean allowEditorStyles;

	@Column(name = "enableSocialSharing")
	private Boolean enableSocialSharing;

	@Column(name = "enableFacebook")
	private Boolean enableFacebook;

	@Column(name = "enableTwitter")
	private Boolean enableTwitter;

	@Column(name = "enableLinkedin")
	private Boolean enableLinkedin;

	@Column(name = "enableEmail")
	private Boolean enableEmail;

	@Column(name = "socialTitle")
	private String socialTitle;

	@Column(name = "socialDescription")
	private String socialDescription;

	@JoinTable(name = "contenttags", joinColumns =
			{
					@JoinColumn(name = "contentId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "tagId", referencedColumnName = "id")
			})
	@ManyToMany
	private Collection<VocabTerm> contentTags;

	@Column(name = "showNotes")
	private Boolean showNotes;

	@OneToMany(mappedBy = "content")
	@OrderBy("sequence")
	private Collection<ContentBlockSection> contentBlockSections;

	@Column(name = "blocksSectionType")
	private Integer blocksSectionType;

	@Column(name = "scope")
	private String scope;

	@Column(name = "showOnlyScope")
	private Boolean showOnlyScope;

	@Column(name = "overrideRegions")
	private Boolean overrideRegions;

	@Column(name = "centerRegions")
	private Boolean centerRegions;

	@Column(name = "useCustomHtml")
	private Boolean useCustomHtml;

	public Content()
	{
	}

	public Content(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Image getBannerGraphic()
	{
		return bannerGraphic;
	}

	public void setBannerGraphic(Image bannerGraphic)
	{
		this.bannerGraphic = bannerGraphic;
	}

	public String getBody()
	{
		return body;
	}

	public void setBody(String body)
	{
		this.body = body;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public String getUrlAlias()
	{
		return urlAlias;
	}

	public void setUrlAlias(String urlAlias)
	{
		this.urlAlias = urlAlias;
	}

	public String getContentLayout()
	{
		return contentLayout;
	}

	public void setContentLayout(String contentLayout)
	{
		this.contentLayout = contentLayout;
	}

	public String getHeaderLayout()
	{
		return headerLayout;
	}

	public void setHeaderLayout(String headerLayout)
	{
		this.headerLayout = headerLayout;
	}

	public String getFooterLayout()
	{
		return footerLayout;
	}

	public void setFooterLayout(String footerLayout)
	{
		this.footerLayout = footerLayout;
	}

	public Collection<ContentFile> getContentFiles()
	{
		return contentFiles;
	}

	public void setContentFiles(Collection<ContentFile> contentFiles)
	{
		this.contentFiles = contentFiles;
	}

	public Collection<ContentBlockSection> getContentBlockSections()
	{
		return contentBlockSections;
	}

	public void setContentBlockSections(Collection<ContentBlockSection> contentBlockSections)
	{
		this.contentBlockSections = contentBlockSections;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public String getSubHeader()
	{
		return subHeader;
	}

	public void setSubHeader(String subHeader)
	{
		this.subHeader = subHeader;
	}

	public String getBody2()
	{
		return body2;
	}

	public void setBody2(String body2)
	{
		this.body2 = body2;
	}

	public String getBody3()
	{
		return body3;
	}

	public void setBody3(String body3)
	{
		this.body3 = body3;
	}

	public String getBody4()
	{
		return body4;
	}

	public void setBody4(String body4)
	{
		this.body4 = body4;
	}

	public String getFooter()
	{
		return footer;
	}

	public void setFooter(String footer)
	{
		this.footer = footer;
	}

	public String getSubFooter()
	{
		return subFooter;
	}

	public void setSubFooter(String subFooter)
	{
		this.subFooter = subFooter;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	public Layout getPageTemplate()
	{
		return pageTemplate;
	}

	public void setPageTemplate(Layout pageTemplate)
	{
		this.pageTemplate = pageTemplate;
	}

	public Boolean getShowTitle()
	{
		return showTitle;
	}

	public void setShowTitle(Boolean showTitle)
	{
		this.showTitle = showTitle;
	}

	public String getTitleSize()
	{
		return titleSize;
	}

	public void setTitleSize(String titleSize)
	{
		this.titleSize = titleSize;
	}

	public Boolean getShowTitleAboveBanner()
	{
		return showTitleAboveBanner;
	}

	public void setShowTitleAboveBanner(Boolean showTitleAboveBanner)
	{
		this.showTitleAboveBanner = showTitleAboveBanner;
	}

	public String getCustomContentTemplate()
	{
		return customContentTemplate;
	}

	public void setCustomContentTemplate(String customContentTemplate)
	{
		this.customContentTemplate = customContentTemplate;
	}

	public Gallery getGallery()
	{
		return gallery;
	}

	public void setGallery(Gallery gallery)
	{
		this.gallery = gallery;
	}

	public Presentation getPresentation()
	{
		return presentation;
	}

	public void setPresentation(Presentation presentation)
	{
		this.presentation = presentation;
	}

	public SystemUnit getUnit()
	{
		return unit;
	}

	public void setUnit(SystemUnit unit)
	{
		this.unit = unit;
	}

	public Grid getUnitGrid()
	{
		return unitGrid;
	}

	public void setUnitGrid(Grid unitGrid)
	{
		this.unitGrid = unitGrid;
	}

	public Boolean getAvailableGlobal()
	{
		return availableGlobal;
	}

	public void setAvailableGlobal(Boolean availableGlobal)
	{
		this.availableGlobal = availableGlobal;
	}

	public Boolean getPrintable()
	{
		return printable;
	}

	public void setPrintable(Boolean printable)
	{
		this.printable = printable;
	}

	public Boolean getShowFilesDownload()
	{
		return showFilesDownload;
	}

	public void setShowFilesDownload(Boolean showFilesDownload)
	{
		this.showFilesDownload = showFilesDownload;
	}

	public String getFilesHeaderText()
	{
		return filesHeaderText;
	}

	public void setFilesHeaderText(String filesHeaderText)
	{
		this.filesHeaderText = filesHeaderText;
	}

	public String getMetaTitle()
	{
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle)
	{
		this.metaTitle = metaTitle;
	}

	public String getMetaDescription()
	{
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription)
	{
		this.metaDescription = metaDescription;
	}

	public Boolean getAllowEditorStyles()
	{
		return allowEditorStyles;
	}

	public void setAllowEditorStyles(Boolean allowEditorStyles)
	{
		this.allowEditorStyles = allowEditorStyles;
	}

	public Collection<VocabTerm> getContentTags()
	{
		return contentTags;
	}

	public void setContentTags(Collection<VocabTerm> contentTags)
	{
		this.contentTags = contentTags;
	}

	public Boolean getShowNotes()
	{
		return showNotes;
	}

	public void setShowNotes(Boolean showNotes)
	{
		this.showNotes = showNotes;
	}

	public Integer getBlocksSectionType()
	{
		return blocksSectionType;
	}

	public void setBlocksSectionType(Integer blocksSectionType)
	{
		this.blocksSectionType = blocksSectionType;
	}

	public Boolean getShowBlockTitle()
	{
		return showBlockTitle;
	}

	public void setShowBlockTitle(Boolean showBlockTitle)
	{
		this.showBlockTitle = showBlockTitle;
	}

	public String getBlockTitleSize()
	{
		return blockTitleSize;
	}

	public void setBlockTitleSize(String blockTitleSize)
	{
		this.blockTitleSize = blockTitleSize;
	}

	public String getScope()
	{
		return scope;
	}

	public void setScope(String scope)
	{
		this.scope = scope;
	}

	public Boolean getShowOnlyScope()
	{
		return showOnlyScope;
	}

	public void setShowOnlyScope(Boolean showOnlyScope)
	{
		this.showOnlyScope = showOnlyScope;
	}

	public Boolean getEnableSocialSharing()
	{
		return enableSocialSharing;
	}

	public void setEnableSocialSharing(Boolean enableSocialSharing)
	{
		this.enableSocialSharing = enableSocialSharing;
	}

	public Boolean getEnableFacebook()
	{
		return enableFacebook;
	}

	public void setEnableFacebook(Boolean enableFacebook)
	{
		this.enableFacebook = enableFacebook;
	}

	public Boolean getEnableTwitter()
	{
		return enableTwitter;
	}

	public void setEnableTwitter(Boolean enableTwitter)
	{
		this.enableTwitter = enableTwitter;
	}

	public Boolean getEnableLinkedin()
	{
		return enableLinkedin;
	}

	public void setEnableLinkedin(Boolean enableLinkedin)
	{
		this.enableLinkedin = enableLinkedin;
	}

	public Boolean getEnableEmail()
	{
		return enableEmail;
	}

	public void setEnableEmail(Boolean enableEmail)
	{
		this.enableEmail = enableEmail;
	}

	public String getSocialTitle()
	{
		return socialTitle;
	}

	public void setSocialTitle(String socialTitle)
	{
		this.socialTitle = socialTitle;
	}

	public String getSocialDescription()
	{
		return socialDescription;
	}

	public void setSocialDescription(String socialDescription)
	{
		this.socialDescription = socialDescription;
	}

	public Boolean getOverrideRegions()
	{
		return overrideRegions;
	}

	public void setOverrideRegions(Boolean overrideRegions)
	{
		this.overrideRegions = overrideRegions;
	}

	public Boolean getCenterRegions()
	{
		return centerRegions;
	}

	public void setCenterRegions(Boolean centerRegions)
	{
		this.centerRegions = centerRegions;
	}

	public Boolean getUseCustomHtml()
	{
		return useCustomHtml;
	}

	public void setUseCustomHtml(Boolean useCustomHtml)
	{
		this.useCustomHtml = useCustomHtml;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Content))
		{
			return false;
		}
		Content other = (Content) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.Content[ id=" + id + " ]";
	}


}
