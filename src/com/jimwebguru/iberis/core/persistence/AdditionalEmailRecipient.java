/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "additionalemailrecipients")
public class AdditionalEmailRecipient implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emailRecipientId")
    private Long emailRecipientId;

    @Basic(optional = false)
    @Column(name = "emailAddress")
    private String emailAddress;

    @JoinColumn(name = "transactionId", referencedColumnName = "transactionId")
    @ManyToOne(optional = false)
    private EmailTransaction emailTransaction;

    public AdditionalEmailRecipient()
    {
    }

    public AdditionalEmailRecipient(Long emailRecipientId)
    {
        this.emailRecipientId = emailRecipientId;
    }

    public AdditionalEmailRecipient(Long emailRecipientId, String emailAddress)
    {
        this.emailRecipientId = emailRecipientId;
        this.emailAddress = emailAddress;
    }

    public Long getEmailRecipientId()
    {
        return emailRecipientId;
    }

    public void setEmailRecipientId(Long emailRecipientId)
    {
        this.emailRecipientId = emailRecipientId;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public EmailTransaction getEmailTransaction()
    {
        return emailTransaction;
    }

    public void setEmailTransaction(EmailTransaction emailTransaction)
    {
        this.emailTransaction = emailTransaction;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (emailRecipientId != null ? emailRecipientId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdditionalEmailRecipient))
        {
            return false;
        }
        AdditionalEmailRecipient other = (AdditionalEmailRecipient) object;
        if ((this.emailRecipientId == null && other.emailRecipientId != null) || (this.emailRecipientId != null && !this.emailRecipientId.equals(other.emailRecipientId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.AdditionalEmailRecipient[emailRecipientId=" + emailRecipientId + "]";
    }

}
