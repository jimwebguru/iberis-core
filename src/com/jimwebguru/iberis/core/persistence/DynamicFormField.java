/*
 *
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "dynamicformfields")
public class DynamicFormField implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "label")
    private String label;

	@Column(name = "cssClasses")
	private String cssClasses;

    @Column(name = "width")
    private Integer width;

    @JoinColumn(name = "formId", referencedColumnName = "id")
    @ManyToOne
    private DynamicForm form;

    @JoinColumn(name = "sectionId", referencedColumnName = "id")
    @ManyToOne
    private DynamicFormSection formSection;

    @JoinColumn(name = "fieldType", referencedColumnName = "typeId")
    @ManyToOne
    private UnitPropertyType fieldType;

    @Column(name = "sequence")
    private Integer sequence;

	@Column(name = "lookupUnitId")
	private Long lookupUnitId;

	@Column(name = "lookupGridId")
	private Long lookupGridId;

	@Column(name = "required")
	private Boolean required;

	@Column(name = "inlineLabel")
	private Boolean inlineLabel;

	@Column(name = "requiredMessage")
	private String requiredMessage;

	@Transient
	private String value;

	@Transient
	private String displayValue;

    @OneToMany(mappedBy = "field")
    @OrderBy("displayValue")
    private Collection<DynamicFieldListItem> fieldListItems;

    /** Creates a new instance of Form */
    public DynamicFormField()
    {
    }

    /**
     * Creates a new instance of Form with the specified values.
     * @param id the formId of the Form
     */
    public DynamicFormField(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public Integer getWidth()
    {
        return width;
    }

    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public DynamicForm getForm()
    {
        return form;
    }

    public void setForm(DynamicForm form)
    {
        this.form = form;
    }

    public DynamicFormSection getFormSection()
    {
        return formSection;
    }

    public void setFormSection(DynamicFormSection formSection)
    {
        this.formSection = formSection;
    }

    public UnitPropertyType getFieldType()
    {
        return fieldType;
    }

    public void setFieldType(UnitPropertyType fieldType)
    {
        this.fieldType = fieldType;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public Collection<DynamicFieldListItem> getFieldListItems()
    {
        return fieldListItems;
    }

    public void setFieldListItems(Collection<DynamicFieldListItem> fieldListItems)
    {
        this.fieldListItems = fieldListItems;
    }

	public Long getLookupUnitId()
	{
		return lookupUnitId;
	}

	public void setLookupUnitId(Long lookupUnitId)
	{
		this.lookupUnitId = lookupUnitId;
	}

	public Long getLookupGridId()
	{
		return lookupGridId;
	}

	public void setLookupGridId(Long lookupGridId)
	{
		this.lookupGridId = lookupGridId;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public String getDisplayValue()
	{
		return displayValue;
	}

	public void setDisplayValue(String displayValue)
	{
		this.displayValue = displayValue;
	}

	public Boolean getRequired()
	{
		return required;
	}

	public void setRequired(Boolean required)
	{
		this.required = required;
	}

	public Boolean getInlineLabel()
	{
		return inlineLabel;
	}

	public void setInlineLabel(Boolean inlineLabel)
	{
		this.inlineLabel = inlineLabel;
	}

	public String getRequiredMessage()
	{
		return requiredMessage;
	}

	public void setRequiredMessage(String requiredMessage)
	{
		this.requiredMessage = requiredMessage;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCssClasses()
	{
		return cssClasses;
	}

	public void setCssClasses(String cssClasses)
	{
		this.cssClasses = cssClasses;
	}

	/**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Form.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Form object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DynamicFormField))
        {
            return false;
        }
        
        DynamicFormField other = (DynamicFormField)object;
        
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.DynamicFormField[id=" + id + "]";
    }
    
}
