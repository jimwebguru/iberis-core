/*
 * Product.java
 *
 * Created on July 10, 2007, 8:04 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "products")
public class Product implements Serializable
{
        @Id
        @Column(name = "productId", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long productId;

        @Column(name = "productName", nullable = false)
        private String productName;

        @JoinColumn(name = "productGroup", referencedColumnName = "groupId")
        @ManyToOne
        private ProductGroup productGroup;
        
        @Column(name = "details")
        private String details;

        @Column(name = "details2")
        private String details2;
        
        @JoinColumn(name = "manufacturerId", referencedColumnName = "manufacturerId")
        @ManyToOne
        private Manufacturer manufacturer;
        
        @Column(name = "manufacturerPartNumber")
        private String manufacturerPartNumber;

        @JoinColumn(name = "mainImageId", referencedColumnName = "id")
        @ManyToOne
        private ProductImage mainImage;

        @Column(name = "price")
        private BigDecimal price;
        
        @Column(name = "quantity")
        private Integer quantity;

        @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
        private Collection<ProductImage> productImages;

        @JoinTable(name = "productsites", joinColumns =
                {
                        @JoinColumn(name = "productId", referencedColumnName = "productId")
                }, inverseJoinColumns =
                {
                        @JoinColumn(name = "siteId", referencedColumnName = "siteID")
                })
        @ManyToMany
        private Collection<Site> sites;

        @Column(name = "created")
        @Temporal(TemporalType.TIMESTAMP)
        private Date created;

        @Column(name = "modified")
        @Temporal(TemporalType.TIMESTAMP)
        private Date modified;
        
        /** Creates a new instance of Product */
        public Product()
        {
        }

        /**
         * Creates a new instance of Product with the specified values.
         * @param productId the productId of the Product
         */
        public Product(Long productId)
        {
                this.productId = productId;
        }

        /**
         * Creates a new instance of Product with the specified values.
         * @param productId the productId of the Product
         * @param productName the productName of the Product
         */
        public Product(Long productId, String productName)
        {
                this.productId = productId;
                this.productName = productName;
        }

        /**
         * Gets the productId of this Product.
         * @return the productId
         */
        public Long getProductId()
        {
                return this.productId;
        }

        /**
         * Sets the productId of this Product to the specified value.
         * @param productId the new productId
         */
        public void setProductId(Long productId)
        {
                this.productId = productId;
        }

        /**
         * Gets the productName of this Product.
         * @return the productName
         */
        public String getProductName()
        {
                return this.productName;
        }

        /**
         * Sets the productName of this Product to the specified value.
         * @param productName the new productName
         */
        public void setProductName(String productName)
        {
                this.productName = productName;
        }

        /**
         * Gets the productGroup of this Product.
         * @return the productGroup
         */
        public ProductGroup getProductGroup()
        {
                return this.productGroup;
        }

        /**
         * Sets the productGroup of this Product to the specified value.
         * @param productGroup the new productGroup
         */
        public void setProductGroup(ProductGroup productGroup)
        {
                this.productGroup = productGroup;
        }

        public String getDetails()
        {
                return details;
        }

        public void setDetails(String details)
        {
                this.details = details;
        }

        public String getDetails2()
        {
            return details2;
        }

        public void setDetails2(String details2)
        {
            this.details2 = details2;
        }

        public ProductImage getMainImage()
        {
            return mainImage;
        }

        public void setMainImage(ProductImage image)
        {
            this.mainImage = image;
        }

        public BigDecimal getPrice()
        {
            return price;
        }

        public void setPrice(BigDecimal price)
        {
            this.price = price;
        }

        public Integer getQuantity()
        {
            return quantity;
        }

        public void setQuantity(Integer quantity)
        {
            this.quantity = quantity;
        }

        public Manufacturer getManufacturer()
        {
            return manufacturer;
        }

        public void setManufacturer(Manufacturer manufacturer)
        {
            this.manufacturer = manufacturer;
        }

        public String getManufacturerPartNumber()
        {
            return manufacturerPartNumber;
        }

        public void setManufacturerPartNumber(String manufacturerPartNumber)
        {
            this.manufacturerPartNumber = manufacturerPartNumber;
        }

        public Collection<ProductImage> getProductImages()
        {
                return productImages;
        }

        public void setProductImages(Collection<ProductImage> productImages)
        {
                this.productImages = productImages;
        }

		public Collection<Site> getSites()
		{
			return sites;
		}

		public void setSites(Collection<Site> sites)
		{
			this.sites = sites;
		}

		public Date getCreated()
		{
			return created;
		}

		public void setCreated(Date created)
		{
			this.created = created;
		}

		public Date getModified()
		{
			return modified;
		}

		public void setModified(Date modified)
		{
			this.modified = modified;
		}

		@SuppressWarnings("unused")
		@PrePersist
		private void onInsert() {
			this.created = new Date();
			this.modified = this.created;
		}

		@SuppressWarnings("unused")
		@PreUpdate
		private void onUpdate() {
			this.modified = new Date();
		}

		/**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.productId != null ? this.productId.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this Product.  The result is 
         * <code>true</code> if and only if the argument is not null and is a Product object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof Product)) {
                        return false;
                }
                Product other = (Product)object;
                if (this.productId != other.productId && (this.productId == null || !this.productId.equals(other.productId))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.Product[productId=" + productId + "]";
        }
        
}
