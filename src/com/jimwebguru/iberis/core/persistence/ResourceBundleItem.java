package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "resourcebundle")
public class ResourceBundleItem
{
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "bundleName")
	private String bundleName;

	@Column(name = "bundleKey")
	private String bundleKey;

	@Column(name = "bundleValue")
	private String bundleValue;

	@Column(name = "countryCode")
	private String countryCode;

	@Column(name = "languageCode")
	private String languageCode;

	@Column(name = "variant")
	private String variant;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getBundleName()
	{
		return bundleName;
	}

	public void setBundleName(String bundleName)
	{
		this.bundleName = bundleName;
	}

	public String getBundleKey()
	{
		return bundleKey;
	}

	public void setBundleKey(String key)
	{
		this.bundleKey = key;
	}

	public String getBundleValue()
	{
		return bundleValue;
	}

	public void setBundleValue(String value)
	{
		this.bundleValue = value;
	}

	public String getCountryCode()
	{
		return countryCode;
	}

	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}

	public String getLanguageCode()
	{
		return languageCode;
	}

	public void setLanguageCode(String languageCode)
	{
		this.languageCode = languageCode;
	}

	public String getVariant()
	{
		return variant;
	}

	public void setVariant(String variant)
	{
		this.variant = variant;
	}
}
