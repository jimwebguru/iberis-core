package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "galleryimages")
public class GalleryImage
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@JoinColumn(name = "imageId", referencedColumnName = "imageid")
	@ManyToOne
	private Image image;

	@JoinColumn(name = "galleryId", referencedColumnName = "id")
	@ManyToOne
	private Gallery gallery;

	@Column(name = "sequence")
	private Integer sequence;

	@Column(name = "caption")
	private String caption;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Image getImage()
	{
		return image;
	}

	public void setImage(Image image)
	{
		this.image = image;
	}

	public Gallery getGallery()
	{
		return gallery;
	}

	public void setGallery(Gallery gallery)
	{
		this.gallery = gallery;
	}

	public Integer getSequence()
	{
		return sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}

	public String getCaption()
	{
		return caption;
	}

	public void setCaption(String caption)
	{
		this.caption = caption;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}
}
