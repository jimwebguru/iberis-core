/*
 * Grid.java
 *
 * Created on December 14, 2006, 1:59 PM
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "grids")
public class Grid implements Serializable 
{

    @Id
    @Column(name = "gridId", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long gridId;

    @Column(name = "gridName")
    private String gridName;

    @Column(name = "unitId")
    private Long unitId;

    @OneToMany(mappedBy="grid")
    @OrderBy("sequence")
    private Collection<GridColumn> gridColumns;

    @OneToMany(mappedBy="grid")
    @OrderBy("sequence")
    private Collection<SearchColumn> searchColumns;
    
    /** Creates a new instance of Grid */
    public Grid() 
    {
    }

    /**
     * Creates a new instance of Grid with the specified values.
     * @param gridId the gridId of the Grid
     */
    public Grid(Long gridId) 
    {
        this.gridId = gridId;
    }

    /**
     * Gets the gridId of this Grid.
     * @return the gridId
     */
    public Long getGridId() 
    {
        return this.gridId;
    }

    /**
     * Sets the gridId of this Grid to the specified value.
     * @param gridId the new gridId
     */
    public void setGridId(Long gridId) 
    {
        this.gridId = gridId;
    }

    /**
     * Gets the gridName of this Grid.
     * @return the gridName
     */
    public String getGridName() 
    {
        return this.gridName;
    }

    /**
     * Sets the gridName of this Grid to the specified value.
     * @param gridName the new gridName
     */
    public void setGridName(String gridName) 
    {
        this.gridName = gridName;
    }

    /**
     * Gets the unitId of this Grid.
     * @return the unitId
     */
    public Long getUnitId() 
    {
        return this.unitId;
    }

    /**
     * Sets the unitId of this Grid to the specified value.
     * @param unitId the new unitId
     */
    public void setUnitId(Long unitId) 
    {
        this.unitId = unitId;
    }

    /**
     * Gets the gridColumnCollection of this Grid.
     * @return the gridColumnCollection
     */
    public Collection<GridColumn> getGridColumns()
    {
        return this.gridColumns;
    }

    /**
     * Sets the gridColumnCollection of this Grid to the specified value.
     * @param gridColumnCollection the new gridColumnCollection
     */
    public void setGridColumns(Collection<GridColumn> gridColumnCollection)
    {
        this.gridColumns = gridColumnCollection;
    }

    /**
     * Gets the searchColumnCollection of this Grid.
     * @return the searchColumnCollection
     */
    public Collection<SearchColumn> getSearchColumns()
    {
        return this.searchColumns;
    }

    /**
     * Sets the searchColumnCollection of this Grid to the specified value.
     * @param searchColumnCollection the new searchColumnCollection
     */
    public void setSearchColumns(Collection<SearchColumn> searchColumnCollection)
    {
        this.searchColumns = searchColumnCollection;
    }

    /**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.gridId != null ? this.gridId.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Grid.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Grid object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grid)) {
            return false;
        }
        Grid other = (Grid)object;
        if (this.gridId != other.gridId && (this.gridId == null || !this.gridId.equals(other.gridId)))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.Grid[gridId=" + gridId + "]";
    }
    
}
