/*
 * ConditionOperator.java
 *
 * Created on April 19, 2007, 3:19 PM
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "conditionoperators")
public class ConditionOperator implements Serializable
{

        @Id
        @Column(name = "operatorId", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private ConditionOperatorId operatorId;

        @Column(name = "conditionName", nullable = false)
        private String condition;

        @Column(name = "operator", nullable = false)
        private String operator;

        @OneToMany(mappedBy = "conditionOperator")
        private Collection<SearchColumn> searchColumns;
        
        /** Creates a new instance of ConditionOperator */
        public ConditionOperator()
        {
        }

        /**
         * Creates a new instance of ConditionOperator with the specified values.
         * @param operatorId the operatorId of the ConditionOperator
         */
        public ConditionOperator(ConditionOperatorId operatorId)
        {
                this.operatorId = operatorId;
        }

        /**
         * Creates a new instance of ConditionOperator with the specified values.
         * @param operatorId the operatorId of the ConditionOperator
         * @param condition the condition of the ConditionOperator
         * @param operator the operator of the ConditionOperator
         */
        public ConditionOperator(ConditionOperatorId operatorId, String condition, String operator)
        {
                this.operatorId = operatorId;
                this.condition = condition;
                this.operator = operator;
        }

        /**
         * Gets the operatorId of this ConditionOperator.
         * @return the operatorId
         */
        public ConditionOperatorId getOperatorId()
        {
                return this.operatorId;
        }

        /**
         * Sets the operatorId of this ConditionOperator to the specified value.
         * @param operatorId the new operatorId
         */
        public void setOperatorId(ConditionOperatorId operatorId)
        {
                this.operatorId = operatorId;
        }

        /**
         * Gets the condition of this ConditionOperator.
         * @return the condition
         */
        public String getCondition()
        {
                return this.condition;
        }

        /**
         * Sets the condition of this ConditionOperator to the specified value.
         * @param condition the new condition
         */
        public void setCondition(String condition)
        {
                this.condition = condition;
        }

        /**
         * Gets the operator of this ConditionOperator.
         * @return the operator
         */
        public String getOperator()
        {
                return this.operator;
        }

        /**
         * Sets the operator of this ConditionOperator to the specified value.
         * @param operator the new operator
         */
        public void setOperator(String operator)
        {
                this.operator = operator;
        }

        /**
         * Gets the searchColumnCollection of this ConditionOperator.
         * @return the searchColumnCollection
         */
        public Collection<SearchColumn> getSearchColumns()
        {
                return this.searchColumns;
        }

        /**
         * Sets the searchColumnCollection of this ConditionOperator to the specified value.
         * @param searchColumnCollection the new searchColumnCollection
         */
        public void setSearchColumns(Collection<SearchColumn> searchColumnCollection)
        {
                this.searchColumns = searchColumnCollection;
        }

        /**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.operatorId != null ? this.operatorId.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this ConditionOperator.  The result is 
         * <code>true</code> if and only if the argument is not null and is a ConditionOperator object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof ConditionOperator)) {
                        return false;
                }
                ConditionOperator other = (ConditionOperator)object;
                if (this.operatorId != other.operatorId && (this.operatorId == null || !this.operatorId.equals(other.operatorId))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.ConditionOperator[operatorId=" + operatorId + "]";
        }
        
}
