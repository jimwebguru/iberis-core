package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "vocabterms")
@XmlRootElement
public class VocabTerm implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	@JoinColumn(name = "vocab_id", referencedColumnName = "id")
	@ManyToOne
	private Vocabulary vocabulary;

	@JoinColumn(name = "parentTermId", referencedColumnName = "id")
	@ManyToOne
	private VocabTerm parentTerm;

	@Column(name = "sequenceIndex")
	private Integer sequenceIndex;

	@OneToMany(mappedBy = "parentTerm", fetch = FetchType.EAGER)
	@OrderBy("sequenceIndex")
	private Collection<VocabTerm> subTerms;

	@ManyToMany(mappedBy = "vocabTermCollection", fetch = FetchType.EAGER)
	private Collection<FAQ> relatedFaqs;

	public VocabTerm()
	{
	}

	public VocabTerm(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public Vocabulary getVocabulary()
	{
		return vocabulary;
	}

	public void setVocabulary(Vocabulary vocab)
	{
		this.vocabulary = vocab;
	}

	public VocabTerm getParentTerm()
	{
		return this.parentTerm;
	}

	public void setParentTerm(VocabTerm parentTerm)
	{
		this.parentTerm = parentTerm;
	}

	public Integer getSequenceIndex()
	{
		return this.sequenceIndex;
	}

	public void setSequenceIndex(Integer sequenceIndex)
	{
		this.sequenceIndex = sequenceIndex;
	}

	public Collection<VocabTerm> getSubTerms()
	{
		return this.subTerms;
	}

	public void setSubTerms(Collection<VocabTerm> subTerms)
	{
		this.subTerms = subTerms;
	}


	public Collection<FAQ> getRelatedFaqs()
	{
		return relatedFaqs;
	}


	public void setRelatedFaqs(Collection<FAQ> relatedFaqs)
	{
		this.relatedFaqs = relatedFaqs;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof VocabTerm))
		{
			return false;
		}
		VocabTerm other = (VocabTerm) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.VocabTerm[ id=" + id + " ]";
	}

}