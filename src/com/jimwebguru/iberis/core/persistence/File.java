package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "files")
@XmlRootElement
public class File implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "file_name")
	private String fileName;

	@Column(name = "display_name")
	private String displayName;

	@Lob
	@Column(name = "data")
	private byte[] data;

	@JoinColumn(name = "mimetypeid", referencedColumnName = "typeid")
	@ManyToOne(optional = false)
	private MimeType mimeType;

	@JoinColumn(name = "fileextensionid", referencedColumnName = "extensionid")
	@ManyToOne(optional = false)
	private FileExtension fileExtension;

	@Column(name ="filePath")
	private String filePath;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	@JoinTable(name = "filetags", joinColumns =
			{
					@JoinColumn(name = "fileId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "tagId", referencedColumnName = "id")
			})
	@ManyToMany
	private Collection<VocabTerm> fileTags;

	@JoinTable(name = "filesites", joinColumns =
			{
					@JoinColumn(name = "fileId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	public File()
	{
	}

	public File(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getFileName()
	{
		return fileName;
	}

	public String getEncodedFileName()
	{
		String encoded;
		try
		{
			encoded = URLEncoder.encode(this.fileName, "UTF-8");
		}
		catch (UnsupportedEncodingException e)
		{
			encoded = this.fileName;
		}

		return encoded;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public byte[] getData()
	{
		return data;
	}

	public void setData(byte[] data)
	{
		this.data = data;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public MimeType getMimeType()
	{
		return mimeType;
	}

	public void setMimeType(MimeType mimeType)
	{
		this.mimeType = mimeType;
	}

	public FileExtension getFileExtension()
	{
		return fileExtension;
	}

	public void setFileExtension(FileExtension fileExtension)
	{
		this.fileExtension = fileExtension;
	}

	public String getFilePath()
	{
		return filePath;
	}

	public void setFilePath(String filePath)
	{
		this.filePath = filePath;
	}

	public Collection<VocabTerm> getFileTags()
	{
		return fileTags;
	}

	public void setFileTags(Collection<VocabTerm> fileTags)
	{
		this.fileTags = fileTags;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

/*@XmlTransient
	public Collection<Content> getContentCollection()
	{
		return contentCollection;
	}

	public void setContentCollection(Collection<Content> contentCollection)
	{
		this.contentCollection = contentCollection;
	} */

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof File))
		{
			return false;
		}
		File other = (File) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.File[ id=" + id + " ]";
	}

}