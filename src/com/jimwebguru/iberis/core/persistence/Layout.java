package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "layouts")
@XmlRootElement
public class Layout implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name="templateDirName")
	private String templateDirName;

	@JoinTable(name = "layoutpageregions", joinColumns =
			{
					@JoinColumn(name = "layoutId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "pageRegionId", referencedColumnName = "id")
			})
	@ManyToMany
	@OrderBy("name")
	private Collection<PageRegion> pageRegions;

	public Layout()
	{
	}

	public Layout(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getTemplateDirName()
	{
		return templateDirName;
	}

	public void setTemplateDirName(String templateDirName)
	{
		this.templateDirName = templateDirName;
	}

	public Collection<PageRegion> getPageRegions()
	{
		return pageRegions;
	}

	public void setPageRegions(Collection<PageRegion> pageRegions)
	{
		this.pageRegions = pageRegions;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Layout))
		{
			return false;
		}
		Layout other = (Layout) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	/*@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.Layout[ id=" + id + " ]";
	} */


}
