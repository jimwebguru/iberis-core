/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "transactions")
public class Transaction implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transID")
    private Long transID;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private BigDecimal amount;
    
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    
    @Column(name = "accountNumber")
    private String accountNumber;
    
    @Column(name = "expiration")
    private String expiration;
    
    @Column(name = "cvv2")
    private String cvv2;
    
    @Column(name = "firstName")
    private String firstName;
    
    @Column(name = "lastName")
    private String lastName;
    
    @Column(name = "payflowTransactionType")
    private String payflowTransactionType;
    
    @Column(name = "street")
    private String street;
    
    @Column(name = "city")
    private String city;
    
    @Column(name = "zipCode")
    private String zipCode;

    @Column(name = "payflowResult")
    private String payflowResult;

    @Column(name = "paymentResponseMessage")
    private String paymentResponseMessage;
 
    @Column(name = "paymentResponse")
    private String paymentResponse;
    
    @Column(name = "paymentType")
    private String paymentType;

    @Column(name = "state")
    private String state;
    
    @Column(name = "countryId")
    private String country;

    @Column(name = "payflowAuthCode")
    private String payflowAuthCode;

    @Column(name = "payflowPnref")
    private String payflowPnref;

    @Column(name = "payflowAvsAddr")
    private String payflowAvsAddr;

    @Column(name = "payflowCvvMatch")
    private String payflowCvvMatch;

    @Column(name = "payflowAvsZip")
    private String payflowAvsZip;
 
    @Column(name = "payflowIavs")
    private String payflowIavs;

    @Column(name = "payflowStatus")
    private String payflowStatus;  
    
    @Column(name = "payflowFraudTriggered")
    private Boolean payflowFraudTriggered;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "emailAddress")
    private String emailAddress;

    @Column(name = "purchasedItems")
    private String purchasedItems;
    
    @Column(name = "saleId")
    private Long saleId;
    
    public Transaction()
    {
    }

    public Transaction(Long transID)
    {
        this.transID = transID;
    }

    public Long getTransID()
    {
        return transID;
    }

    public void setTransID(Long transID)
    {
        this.transID = transID;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }

    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public String getExpiration()
    {
        return expiration;
    }

    public void setExpiration(String expiration)
    {
        this.expiration = expiration;
    }

    public String getCvv2()
    {
        return cvv2;
    }

    public void setCvv2(String cvv2)
    {
        this.cvv2 = cvv2;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getPayflowTransactionType()
    {
        return payflowTransactionType;
    }

    public void setPayflowTransactionType(String payflowTransactionType)
    {
        this.payflowTransactionType = payflowTransactionType;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getPayflowResult()
    {
        return payflowResult;
    }

    public void setPayflowResult(String payflowResult)
    {
        this.payflowResult = payflowResult;
    }

    public String getPaymentResponseMessage()
    {
        return paymentResponseMessage;
    }

    public void setPaymentResponseMessage(String paymentResponseMessage)
    {
        this.paymentResponseMessage = paymentResponseMessage;
    }

    public String getPaymentResponse()
    {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse)
    {
        this.paymentResponse = paymentResponse;
    }

    public String getPaymentType()
    {
        return paymentType;
    }

    public void setPaymentType(String paymentType)
    {
        this.paymentType = paymentType;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getPayflowAuthCode()
    {
        return payflowAuthCode;
    }

    public void setPayflowAuthCode(String payflowAuthCode)
    {
        this.payflowAuthCode = payflowAuthCode;
    }

    public String getPayflowAvsAddr()
    {
        return payflowAvsAddr;
    }

    public void setPayflowAvsAddr(String payflowAvsAddr)
    {
        this.payflowAvsAddr = payflowAvsAddr;
    }

    public String getPayflowAvsZip()
    {
        return payflowAvsZip;
    }

    public void setPayflowAvsZip(String payflowAvsZip)
    {
        this.payflowAvsZip = payflowAvsZip;
    }

    public String getPayflowCvvMatch()
    {
        return payflowCvvMatch;
    }

    public void setPayflowCvvMatch(String payflowCvvMatch)
    {
        this.payflowCvvMatch = payflowCvvMatch;
    }

    public Boolean getPayflowFraudTriggered()
    {
        return payflowFraudTriggered;
    }

    public void setPayflowFraudTriggered(Boolean payflowFraudTriggered)
    {
        this.payflowFraudTriggered = payflowFraudTriggered;
    }

    public String getPayflowIavs()
    {
        return payflowIavs;
    }

    public void setPayflowIavs(String payflowIavs)
    {
        this.payflowIavs = payflowIavs;
    }

    public String getPayflowPnref()
    {
        return payflowPnref;
    }

    public void setPayflowPnref(String payflowPnref)
    {
        this.payflowPnref = payflowPnref;
    }

    public String getPayflowStatus()
    {
        return payflowStatus;
    }

    public void setPayflowStatus(String payflowStatus)
    {
        this.payflowStatus = payflowStatus;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPurchasedItems()
    {
        return purchasedItems;
    }

    public void setPurchasedItems(String purchasedItems)
    {
        this.purchasedItems = purchasedItems;
    }

    public Long getSaleId()
    {
        return saleId;
    }

    public void setSaleId(Long saleId)
    {
        this.saleId = saleId;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (transID != null ? transID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaction))
        {
            return false;
        }
        Transaction other = (Transaction) object;
        if ((this.transID == null && other.transID != null) || (this.transID != null && !this.transID.equals(other.transID)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.Transaction[ transID=" + transID + " ]";
    }
    
}
