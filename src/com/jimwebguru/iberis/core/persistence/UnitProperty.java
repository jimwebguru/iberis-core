/*
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "unitproperties")
public class UnitProperty implements Serializable
{
    @Id
    @Column(name = "propertyId", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long propertyId;

    @Column(name = "propertyName")
    private String propertyName;

    @Column(name = "bundleLabelId")
    private String bundleLabelId;

    @JoinColumn(name = "propertyType", referencedColumnName = "typeId")
    @ManyToOne
    private UnitPropertyType propertyType;

    @JoinColumn(name = "unitId", referencedColumnName = "unitID")
    @ManyToOne
    private SystemUnit unit;

    @Column(name = "lookupUnitId")
    private Long lookupUnitId;

    @Column(name = "lookupGridId")
    private Long lookupGridId;

    @Column(name = "fakeLookup")
    private Boolean fakeLookup;

	@Column(name = "booleanWidget")
	private String booleanWidget;

	@Column(name = "textLength")
	private Integer textLength;

	@Column(name = "codeType")
	private String codeType;

    @OneToMany(mappedBy = "unitProperty", cascade = CascadeType.PERSIST)
    @OrderBy("displayValue")
    private Collection<UnitPropertyListItem> unitPropertyListItems;

    /** Creates a new instance of UnitProperty */
    public UnitProperty()
    {
    }

    /**
     * Creates a new instance of UnitProperty with the specified values.
     * @param propertyId the propertyId of the UnitProperty
     */
    public UnitProperty(Long propertyId)
    {
            this.propertyId = propertyId;
    }

    /**
     * Gets the propertyId of this UnitProperty.
     * @return the propertyId
     */
    public Long getPropertyId()
    {
            return this.propertyId;
    }

    /**
     * Sets the propertyId of this UnitProperty to the specified value.
     * @param propertyId the new propertyId
     */
    public void setPropertyId(Long propertyId)
    {
            this.propertyId = propertyId;
    }

    /**
     * Gets the propertyName of this UnitProperty.
     * @return the propertyName
     */
    public String getPropertyName()
    {
            return this.propertyName;
    }

    /**
     * Sets the propertyName of this UnitProperty to the specified value.
     * @param propertyName the new propertyName
     */
    public void setPropertyName(String propertyName)
    {
            this.propertyName = propertyName;
    }

    /**
     * Gets the bundleLabelId of this UnitProperty.
     * @return the bundleLabelId
     */
    public String getBundleLabelId()
    {
            return this.bundleLabelId;
    }

    /**
     * Sets the bundleLabelId of this UnitProperty to the specified value.
     * @param bundleLabelId the new bundleLabelId
     */
    public void setBundleLabelId(String bundleLabelId)
    {
            this.bundleLabelId = bundleLabelId;
    }

    /**
     * Gets the propertyType of this UnitProperty.
     * @return the propertyType
     */
    public UnitPropertyType getPropertyType()
    {
            return this.propertyType;
    }

    /**
     * Sets the propertyType of this UnitProperty to the specified value.
     * @param propertyType the new propertyType
     */
    public void setPropertyType(UnitPropertyType propertyType)
    {
            this.propertyType = propertyType;
    }

    /**
     * Gets the unit of this UnitProperty.
     * @return the unit
     */
    public SystemUnit getUnit()
    {
            return this.unit;
    }

    /**
     * Sets the unit of this UnitProperty to the specified value.
     * @param unit the new unit
     */
    public void setUnit(SystemUnit unit)
    {
            this.unit = unit;
    }

    public Long getLookupGridId()
    {
            return lookupGridId;
    }

    public void setLookupGridId(Long lookupGridId)
    {
            this.lookupGridId = lookupGridId;
    }

    public Long getLookupUnitId()
    {
            return lookupUnitId;
    }

    public void setLookupUnitId(Long lookupUnitId)
    {
            this.lookupUnitId = lookupUnitId;
    }

    public Boolean isFakeLookup()
    {
        return fakeLookup;
    }

    public Boolean getFakeLookup()
    {
        return fakeLookup;
    }

    public void setFakeLookup(Boolean fakeLookup)
    {
        this.fakeLookup = fakeLookup;
    }

	public String getBooleanWidget()
	{
		return booleanWidget;
	}

	public void setBooleanWidget(String booleanWidget)
	{
		this.booleanWidget = booleanWidget;
	}

	public Integer getTextLength()
	{
		return textLength;
	}

	public void setTextLength(Integer textLength)
	{
		this.textLength = textLength;
	}

	public String getCodeType()
	{
		return codeType;
	}

	public void setCodeType(String codeType)
	{
		this.codeType = codeType;
	}

	public Collection<UnitPropertyListItem> getUnitPropertyListItems()
        {
            return unitPropertyListItems;
        }

        public void setUnitPropertyListItems(Collection<UnitPropertyListItem> unitPropertyListItems)
        {
            this.unitPropertyListItems = unitPropertyListItems;
        }

        /**
         * Returns a hash code value for the object.  This implementation computes
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.propertyId != null ? this.propertyId.hashCode() : 0);
                return hash;
        }
        
        /**
         * Determines whether another object is equal to this UnitProperty.  The result is
         * <code>true</code> if and only if the argument is not null and is a UnitProperty object that
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof UnitProperty))
                {
                        return false;
                }
                UnitProperty other = (UnitProperty)object;
                if (this.propertyId != other.propertyId && (this.propertyId == null || !this.propertyId.equals(other.propertyId)))
                {
                        return false;
                }
                return true;
        }
        
        /**
         * Returns a string representation of the object.  This implementation constructs
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.UnitProperty[propertyId=" + propertyId + "]";
        }
        
}
