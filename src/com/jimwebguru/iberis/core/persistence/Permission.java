package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "permissions")
@XmlRootElement
public class Permission implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "globalPermission")
	private Boolean globalPermission;

	@JoinColumn(name = "siteId", referencedColumnName = "siteID")
	@ManyToOne
	private Site site;

	@Column(name = "canRead")
	private Boolean canRead;

	@Column(name = "canCreate")
	private Boolean canCreate;

	@Column(name = "canUpdate")
	private Boolean canUpdate;

	@Column(name = "canDelete")
	private Boolean canDelete;

	@JoinColumn(name = "roleId", referencedColumnName = "id")
	@ManyToOne
	private Role role;

	@JoinColumn(name = "unitId", referencedColumnName = "unitID")
	@ManyToOne
	private SystemUnit unit;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Permission()
	{
	}

	public Permission(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Boolean getGlobalPermission()
	{
		return globalPermission;
	}

	public void setGlobalPermission(Boolean globalPermission)
	{
		this.globalPermission = globalPermission;
	}

	public Boolean getCanRead()
	{
		return canRead;
	}

	public void setCanRead(Boolean canRead)
	{
		this.canRead = canRead;
	}

	public Boolean getCanCreate()
	{
		return canCreate;
	}

	public void setCanCreate(Boolean canCreate)
	{
		this.canCreate = canCreate;
	}

	public Boolean getCanUpdate()
	{
		return canUpdate;
	}

	public void setCanUpdate(Boolean canUpdate)
	{
		this.canUpdate = canUpdate;
	}

	public Boolean getCanDelete()
	{
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete)
	{
		this.canDelete = canDelete;
	}

	public Role getRole()
	{
		return role;
	}

	public void setRole(Role role)
	{
		this.role = role;
	}

	public SystemUnit getUnit()
	{
		return unit;
	}

	public void setUnit(SystemUnit unit)
	{
		this.unit = unit;
	}

	public Site getSite()
	{
		return site;
	}

	public void setSite(Site site)
	{
		this.site = site;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Permission))
		{
			return false;
		}
		Permission other = (Permission) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.Role[ id=" + id + " ]";
	}

}