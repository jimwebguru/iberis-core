/*
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "unitpropertytypes")
public class UnitPropertyType implements Serializable 
{

    @Id
    @Column(name = "typeId", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long typeId;

    @Column(name = "typeName")
    private String typeName;
    
    /** Creates a new instance of UnitPropertyType */
    public UnitPropertyType() 
    {
    }

    /**
     * Creates a new instance of UnitPropertyType with the specified values.
     * @param typeId the typeId of the UnitPropertyType
     */
    public UnitPropertyType(Long type)
    {
        this.typeId = type;
    }

    /**
     * Gets the typeId of this UnitPropertyType.
     * @return the typeId
     */
    public Long getTypeId()
    {
        return this.typeId;
    }

    /**
     * Sets the typeId of this UnitPropertyType to the specified value.
     * @param typeId the new typeId
     */
    public void setTypeId(Long type)
    {
        this.typeId = type;
    }

    /**
     * Gets the typeName of this UnitPropertyType.
     * @return the typeName
     */
    public String getTypeName() 
    {
        return this.typeName;
    }

    /**
     * Sets the typeName of this UnitPropertyType to the specified value.
     * @param typeName the new typeName
     */
    public void setTypeName(String typeName) 
    {
        this.typeName = typeName;
    }

    /**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.typeId != null ? this.typeId.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this UnitPropertyType.  The result is 
     * <code>true</code> if and only if the argument is not null and is a UnitPropertyType object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnitPropertyType)) 
        {
            return false;
        }
        UnitPropertyType other = (UnitPropertyType)object;
        if (this.typeId != other.typeId && (this.typeId == null || !this.typeId.equals(other.typeId)))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.UnitPropertyType[typeId=" + typeId + "]";
    }
    
}
