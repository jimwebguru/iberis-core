package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "unitsiteassociations")
@XmlRootElement
public class UnitSiteAssociation implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@JoinColumn(name = "unitId", referencedColumnName = "unitID")
	@ManyToOne
	private SystemUnit unit;

	@JoinColumn(name = "siteId", referencedColumnName = "siteID")
	@ManyToOne
	private Site site;

	public UnitSiteAssociation()
	{
	}

	public UnitSiteAssociation(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public SystemUnit getUnit()
	{
		return unit;
	}

	public void setUnit(SystemUnit unit)
	{
		this.unit = unit;
	}

	public Site getSite()
	{
		return site;
	}

	public void setSite(Site site)
	{
		this.site = site;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof UnitSiteAssociation))
		{
			return false;
		}
		UnitSiteAssociation other = (UnitSiteAssociation) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.Role[ id=" + id + " ]";
	}

}