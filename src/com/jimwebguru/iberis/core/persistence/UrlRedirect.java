package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "urlredirects")
@XmlRootElement
public class UrlRedirect implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "redirectFrom")
	private String redirectFrom;

	@Column(name = "redirectTo")
	private String redirectTo;

	@JoinColumn(name = "siteId", referencedColumnName = "siteID")
	@ManyToOne
	private Site site;

	@Column(name = "enabled")
	private Boolean enabled;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getRedirectFrom()
	{
		return redirectFrom;
	}

	public void setRedirectFrom(String redirectFrom)
	{
		this.redirectFrom = redirectFrom;
	}

	public String getRedirectTo()
	{
		return redirectTo;
	}

	public void setRedirectTo(String redirectTo)
	{
		this.redirectTo = redirectTo;
	}

	public Site getSite()
	{
		return site;
	}

	public void setSite(Site site)
	{
		this.site = site;
	}

	public Boolean getEnabled()
	{
		return enabled;
	}

	public void setEnabled(Boolean enabled)
	{
		this.enabled = enabled;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof UrlRedirect))
		{
			return false;
		}
		UrlRedirect other = (UrlRedirect) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.UrlRedirect[ id=" + id + " ]";
	}

}