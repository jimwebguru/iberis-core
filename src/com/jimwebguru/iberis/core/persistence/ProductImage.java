/*
 * Product.java
 *
 * Created on July 10, 2007, 8:04 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "productimages")
public class ProductImage implements Serializable
{
        @Id
        @Column(name = "id", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long id;

        @JoinColumn(name = "productId", referencedColumnName = "productId")
        @ManyToOne
        private Product product;

        @JoinColumn(name = "imageId", referencedColumnName = "imageid")
        @ManyToOne
        private Image image;

        public Long getId()
        {
                return id;
        }

        public void setId(Long id)
        {
                this.id = id;
        }

        public Product getProduct()
        {
                return product;
        }

        public void setProduct(Product product)
        {
                this.product = product;
        }

        public Image getImage()
        {
                return image;
        }

        public void setImage(Image image)
        {
                this.image = image;
        }

        /**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.id != null ? this.id.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this Product.  The result is 
         * <code>true</code> if and only if the argument is not null and is a Product object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof ProductImage)) {
                        return false;
                }
                ProductImage other = (ProductImage)object;
                if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.ProductImage[Id=" + id + "]";
        }
        
}
