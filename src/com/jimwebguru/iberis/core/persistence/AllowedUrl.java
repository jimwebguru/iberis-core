package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "allowedurls")
public class AllowedUrl
{
	@Column(name = "id")
	@Id
	private Long id;

	@JoinColumn(name = "pageItemAssignmentId", referencedColumnName = "id")
	@ManyToOne
	private PageItemAssignment pageItemAssignment;

	@Column(name = "url")
	private String url;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public PageItemAssignment getPageItemAssignment()
	{
		return pageItemAssignment;
	}

	public void setPageItemAssignment(PageItemAssignment pageItemAssignment)
	{
		this.pageItemAssignment = pageItemAssignment;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}
}
