package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "menuitems")
@XmlRootElement
public class MenuItem implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "url")
	private String url;

	@Column(name = "displayName")
	private String displayName;

	@Column(name = "cssClassNames")
	private String cssClassNames;

	@ManyToOne
	@JoinColumn(name = "parentMenu", referencedColumnName = "id")
	private Menu parentMenu;

	@JoinColumn(name = "parentMenuItem", referencedColumnName = "id")
	@ManyToOne
	private MenuItem parentMenuItem;

	@ManyToOne
	@JoinColumn(name = "menuColumn", referencedColumnName = "id")
	private MenuColumn menuColumn;

	@Column(name = "subMenuGroup")
	private Boolean subMenuGroup;

	@Column(name = "sequenceIndex")
	private Integer sequenceIndex;

	@Column(name = "icon")
	private String icon;

	@Column(name = "anonymousOnly")
	private Boolean anonymousOnly;

	@Column(name = "separatorBefore")
	private Boolean separatorBefore;

	@Column(name = "separatorAfter")
	private Boolean separatorAfter;

	@Column(name = "externalLink")
	private Boolean externalLink;

	@Column(name = "openNewTab")
	private Boolean openNewTab;

	@Column(name = "width")
	private Integer width;

	@OneToMany(mappedBy = "parentMenuItem", fetch = FetchType.EAGER)
	@OrderBy("sequenceIndex")
	private Collection<MenuItem> subMenuItems;

	@OneToMany(mappedBy = "parentMenuItem", fetch = FetchType.EAGER)
	@OrderBy("sequenceIndex")
	private Collection<MenuColumn> subMenuColumns;

	@JoinTable(name="menuitemsroles", joinColumns = {
			@JoinColumn(name="MenuItem_id", referencedColumnName="id")},
			inverseJoinColumns = {
					@JoinColumn(name="Role_id", referencedColumnName = "id")
			})
	@OneToMany(fetch = FetchType.EAGER)
	private Collection<Role> itemRoles;

	public MenuItem()
	{
	}

	public MenuItem(Long id)
	{
		this.id = id;
	}

	public MenuItem(Long id, String url)
	{
		this.id = id;
		this.url = url;
	}

	public MenuItem(Long id,String displayName,String url,String classNames)
	{
		this.id = id;
		this.displayName = displayName;
		this.url = url;
		this.cssClassNames = classNames;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getCssClassNames()
	{
		return cssClassNames;
	}

	public void setCssClassNames(String cssClassNames)
	{
		this.cssClassNames = cssClassNames;
	}

	public Menu getParentMenu()
	{
		return parentMenu;
	}

	public void setParentMenu(Menu parentMenu)
	{
		this.parentMenu = parentMenu;
	}

	public MenuItem getParentMenuItem()
	{
		return parentMenuItem;
	}

	public void setParentMenuItem(MenuItem parentMenuItem)
	{
		this.parentMenuItem = parentMenuItem;
	}

	public Collection<MenuItem> getSubMenuItems()
	{
		return subMenuItems;
	}

	public Boolean isSubMenuGroup()
	{
		return subMenuGroup;
	}

	public Boolean getSubMenuGroup()
	{
		return subMenuGroup;
	}

	public void setSubMenuGroup(Boolean subMenuGroup)
	{
		this.subMenuGroup = subMenuGroup;
	}

	public Integer getSequenceIndex()
	{
		return sequenceIndex;
	}

	public void setSequenceIndex(Integer sequenceIndex)
	{
		this.sequenceIndex = sequenceIndex;
	}

	public void setSubMenuItems(Collection<MenuItem> subMenuItems)
	{
		this.subMenuItems = subMenuItems;
	}

	public String getIcon()
	{
		return icon;
	}

	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	public void addSubMenuItem(MenuItem item)
	{
		item.setParentMenuItem(this);

		this.subMenuItems.add(item);
	}

	public void addSubMenuItem(Long id,String displayName,String url,String classNames)
	{
		MenuItem item = new MenuItem(id,displayName,url,classNames);

		this.addSubMenuItem(item);
	}

	public Collection<Role> getItemRoles()
	{
		return itemRoles;
	}

	public void setItemRoles(Collection<Role> itemRoles)
	{
		this.itemRoles = itemRoles;
	}

	public Boolean getAnonymousOnly()
	{
		return anonymousOnly;
	}

	public void setAnonymousOnly(Boolean anonymousOnly)
	{
		this.anonymousOnly = anonymousOnly;
	}

	public MenuColumn getMenuColumn()
	{
		return menuColumn;
	}

	public void setMenuColumn(MenuColumn menuColumn)
	{
		this.menuColumn = menuColumn;
	}

	public Collection<MenuColumn> getSubMenuColumns()
	{
		return subMenuColumns;
	}

	public void setSubMenuColumns(Collection<MenuColumn> subMenuColumns)
	{
		this.subMenuColumns = subMenuColumns;
	}

	public Boolean getSeparatorBefore()
	{
		return separatorBefore;
	}

	public void setSeparatorBefore(Boolean separatorBefore)
	{
		this.separatorBefore = separatorBefore;
	}

	public Boolean getSeparatorAfter()
	{
		return separatorAfter;
	}

	public void setSeparatorAfter(Boolean separatorAfter)
	{
		this.separatorAfter = separatorAfter;
	}

	public Boolean getExternalLink()
	{
		return externalLink;
	}

	public void setExternalLink(Boolean externalLink)
	{
		this.externalLink = externalLink;
	}

	public Boolean getOpenNewTab()
	{
		return openNewTab;
	}

	public void setOpenNewTab(Boolean openNewTab)
	{
		this.openNewTab = openNewTab;
	}

	public Integer getWidth()
	{
		return width;
	}

	public void setWidth(Integer width)
	{
		this.width = width;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof MenuItem))
		{
			return false;
		}
		MenuItem other = (MenuItem) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.MenuItem[ id=" + id + " ]";
	}

}
