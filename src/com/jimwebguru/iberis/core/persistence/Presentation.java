package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "presentations")
@XmlRootElement
public class Presentation
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "presentationType")
	private Integer presentationType;

	@Column(name = "animationSpeed")
	private Integer animationSpeed;

	@Column(name = "toggleable")
	private Boolean toggleable;

	@Column(name = "orientation")
	private String orientation;

	@Column(name = "scrollable")
	private Boolean scrollable;

	@Column(name = "sectionsClosable")
	private Boolean sectionsClosable;

	@Column(name = "slideMode")
	private String slideMode;

	@Column(name = "startSlide")
	private Integer startSlide;

	@Column(name = "showPager")
	private Boolean showPager;

	@Column(name = "showControls")
	private Boolean showControls;

	@Column(name = "autoSlide")
	private Boolean autoSlide;

	@Column(name = "autoStart")
	private Boolean autoStart;

	@Column(name = "pauseTime")
	private Integer pauseTime;

	@Column(name = "minHeight")
	private Integer minHeight;

	@Column(name = "minWidth")
	private Integer minWidth;

	@Column(name = "maxWidth")
	private Integer maxWidth;

	@Column(name = "maxHeight")
	private Integer maxHeight;

	@Column(name = "showName")
	private Boolean showName;

	@Column(name = "showSlideTitle")
	private Boolean showSlideTitle;

	@Column(name = "heroSliderMode")
	private Boolean heroSliderMode;

	@Column(name = "titleHeroStyle")
	private String titleHeroStyle;

	@Column(name = "imageHeroStyle")
	private String imageHeroStyle;

	@Column(name = "captionHeroStyle")
	private String captionHeroStyle;

	@Column(name = "linkHeroStyle")
	private String linkHeroStyle;

	@Column(name = "linkContainerHeroStyle")
	private String linkContainerHeroStyle;

	@Column(name = "collapsed")
	private Boolean collapsed;

	@Column(name = "imageMaxWidth")
	private Boolean imageMaxWidth;

	@Column(name = "blockSize")
	private Integer blockSize;

	@Column(name = "alternatingBlockSize")
	private Integer alternatingBlockSize;

	@Column(name = "allowEditorStyles")
	private Boolean allowEditorStyles;

	@Column(name = "splitPane")
	private Boolean splitPane;

	@Column(name = "paneReversed")
	private Boolean paneReversed;

	@Column(name = "leftPaneSize")
	private Integer leftPaneSize;

	@Column(name = "flipSlides")
	private Boolean flipSlides;

	@Column(name = "originalFormat")
	private Boolean originalFormat;

	@Column(name = "primaryColor")
	private String primaryColor;

	@Column(name = "secondaryColor")
	private String secondaryColor;

	@Column(name = "useVwFont")
	private Boolean useVwFont;

	@Column(name = "useOverlay")
	private Boolean useOverlay;

	@Column(name = "autoHideControls")
	private Boolean autoHideControls;

	@OneToMany(mappedBy = "presentation", fetch = FetchType.EAGER)
	@OrderBy("sequence")
	private Collection<PresentationSlide> presentationSlides;

	@JoinTable(name = "presentationsites", joinColumns =
			{
					@JoinColumn(name = "presentationId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getAnimationSpeed()
	{
		return animationSpeed;
	}

	public void setAnimationSpeed(Integer animationSpeed)
	{
		this.animationSpeed = animationSpeed;
	}

	public Boolean getToggleable()
	{
		return toggleable;
	}

	public void setToggleable(Boolean toggleable)
	{
		this.toggleable = toggleable;
	}

	public String getOrientation()
	{
		return orientation;
	}

	public void setOrientation(String orientation)
	{
		this.orientation = orientation;
	}

	public Boolean getScrollable()
	{
		return scrollable;
	}

	public void setScrollable(Boolean scrollable)
	{
		this.scrollable = scrollable;
	}

	public Boolean getSectionsClosable()
	{
		return sectionsClosable;
	}

	public void setSectionsClosable(Boolean sectionsClosable)
	{
		this.sectionsClosable = sectionsClosable;
	}

	public String getSlideMode()
	{
		return slideMode;
	}

	public void setSlideMode(String slideMode)
	{
		this.slideMode = slideMode;
	}

	public Integer getStartSlide()
	{
		return startSlide;
	}

	public void setStartSlide(Integer startSlide)
	{
		this.startSlide = startSlide;
	}

	public Boolean getShowPager()
	{
		return showPager;
	}

	public void setShowPager(Boolean showPager)
	{
		this.showPager = showPager;
	}

	public Boolean getShowControls()
	{
		return showControls;
	}

	public void setShowControls(Boolean showControls)
	{
		this.showControls = showControls;
	}

	public Boolean getAutoSlide()
	{
		return autoSlide;
	}

	public void setAutoSlide(Boolean autoSlide)
	{
		this.autoSlide = autoSlide;
	}

	public Boolean getAutoStart()
	{
		return autoStart;
	}

	public void setAutoStart(Boolean autoStart)
	{
		this.autoStart = autoStart;
	}

	public Integer getPauseTime()
	{
		return pauseTime;
	}

	public void setPauseTime(Integer pauseTime)
	{
		this.pauseTime = pauseTime;
	}

	public Integer getMinHeight()
	{
		return minHeight;
	}

	public void setMinHeight(Integer minHeight)
	{
		this.minHeight = minHeight;
	}

	public Integer getMinWidth()
	{
		return minWidth;
	}

	public void setMinWidth(Integer minWidth)
	{
		this.minWidth = minWidth;
	}

	public Integer getMaxWidth()
	{
		return maxWidth;
	}

	public void setMaxWidth(Integer maxWidth)
	{
		this.maxWidth = maxWidth;
	}

	public Integer getMaxHeight()
	{
		return maxHeight;
	}

	public void setMaxHeight(Integer maxHeight)
	{
		this.maxHeight = maxHeight;
	}

	public Integer getPresentationType()
	{
		return presentationType;
	}

	public void setPresentationType(Integer presentationType)
	{
		this.presentationType = presentationType;
	}

	public Boolean getShowName()
	{
		return showName;
	}

	public void setShowName(Boolean showName)
	{
		this.showName = showName;
	}

	public Boolean getShowSlideTitle()
	{
		return showSlideTitle;
	}

	public void setShowSlideTitle(Boolean showSlideTitle)
	{
		this.showSlideTitle = showSlideTitle;
	}

	public Boolean getHeroSliderMode()
	{
		return heroSliderMode;
	}

	public void setHeroSliderMode(Boolean heroSliderMode)
	{
		this.heroSliderMode = heroSliderMode;
	}

	public String getTitleHeroStyle()
	{
		return titleHeroStyle;
	}

	public void setTitleHeroStyle(String titleHeroStyle)
	{
		this.titleHeroStyle = titleHeroStyle;
	}

	public String getImageHeroStyle()
	{
		return imageHeroStyle;
	}

	public void setImageHeroStyle(String imageHeroStyle)
	{
		this.imageHeroStyle = imageHeroStyle;
	}

	public String getCaptionHeroStyle()
	{
		return captionHeroStyle;
	}

	public void setCaptionHeroStyle(String captionHeroStyle)
	{
		this.captionHeroStyle = captionHeroStyle;
	}

	public String getLinkHeroStyle()
	{
		return linkHeroStyle;
	}

	public void setLinkHeroStyle(String linkHeroStyle)
	{
		this.linkHeroStyle = linkHeroStyle;
	}

	public Boolean getCollapsed()
	{
		return collapsed;
	}

	public void setCollapsed(Boolean collapsed)
	{
		this.collapsed = collapsed;
	}

	public Collection<PresentationSlide> getPresentationSlides()
	{
		return presentationSlides;
	}

	public void setPresentationSlides(Collection<PresentationSlide> presentationSlides)
	{
		this.presentationSlides = presentationSlides;
	}

	public String getLinkContainerHeroStyle()
	{
		return linkContainerHeroStyle;
	}

	public void setLinkContainerHeroStyle(String linkContainerHeroStyle)
	{
		this.linkContainerHeroStyle = linkContainerHeroStyle;
	}

	public Boolean getImageMaxWidth()
	{
		return imageMaxWidth;
	}

	public void setImageMaxWidth(Boolean imageMaxWidth)
	{
		this.imageMaxWidth = imageMaxWidth;
	}

	public Integer getBlockSize()
	{
		return blockSize;
	}

	public void setBlockSize(Integer blockSize)
	{
		this.blockSize = blockSize;
	}

	public Integer getAlternatingBlockSize()
	{
		return alternatingBlockSize;
	}

	public void setAlternatingBlockSize(Integer alternatingBlockSize)
	{
		this.alternatingBlockSize = alternatingBlockSize;
	}

	public Boolean getAllowEditorStyles()
	{
		return allowEditorStyles;
	}

	public void setAllowEditorStyles(Boolean allowEditorStyles)
	{
		this.allowEditorStyles = allowEditorStyles;
	}

	public Boolean getSplitPane()
	{
		return splitPane;
	}

	public void setSplitPane(Boolean splitPane)
	{
		this.splitPane = splitPane;
	}

	public Integer getLeftPaneSize()
	{
		return leftPaneSize;
	}

	public void setLeftPaneSize(Integer leftPaneSize)
	{
		this.leftPaneSize = leftPaneSize;
	}

	public Boolean getFlipSlides()
	{
		return flipSlides;
	}

	public void setFlipSlides(Boolean flipSlides)
	{
		this.flipSlides = flipSlides;
	}

	public Boolean getPaneReversed()
	{
		return paneReversed;
	}

	public void setPaneReversed(Boolean paneReversed)
	{
		this.paneReversed = paneReversed;
	}

	public Boolean getOriginalFormat()
	{
		return originalFormat;
	}

	public void setOriginalFormat(Boolean originalFormat)
	{
		this.originalFormat = originalFormat;
	}

	public String getPrimaryColor()
	{
		return primaryColor;
	}

	public void setPrimaryColor(String primaryColor)
	{
		this.primaryColor = primaryColor;
	}

	public String getSecondaryColor()
	{
		return secondaryColor;
	}

	public void setSecondaryColor(String secondaryColor)
	{
		this.secondaryColor = secondaryColor;
	}

	public Boolean getUseVwFont()
	{
		return useVwFont;
	}

	public void setUseVwFont(Boolean useVwFont)
	{
		this.useVwFont = useVwFont;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	public Boolean getUseOverlay()
	{
		return useOverlay;
	}

	public void setUseOverlay(Boolean useOverlay)
	{
		this.useOverlay = useOverlay;
	}

	public Boolean getAutoHideControls()
	{
		return autoHideControls;
	}

	public void setAutoHideControls(Boolean autoHideControls)
	{
		this.autoHideControls = autoHideControls;
	}
}
