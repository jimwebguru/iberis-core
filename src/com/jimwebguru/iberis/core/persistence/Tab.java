package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "tabs")
public class Tab implements Serializable
{

        @Id
        @Column(name = "tabId", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long tabId;

        @Column(name = "tabName", nullable = false)
        private String tabName;
        
        @JoinColumn(name="formId",referencedColumnName = "formId")
        @ManyToOne
        private Form form;
        
        @Column(name="unitId")
        private Long unitId;
        
        @Column(name="referencedUnitProperty")
        private Long referencedUnitProperty;
        
        @Column(name="gridId")
        private Long gridId;
        
        @Column(name="sequence")
        private Long sequence;

        @Column(name="customQuery")
        private String customQuery;

        /** Creates a new instance of Tab */
        public Tab()
        {
        }

        /**
         * Creates a new instance of Tab with the specified values.
         * @param tabId the tabId of the Tab
         */
        public Tab(Long tabId)
        {
                this.tabId = tabId;
        }

        /**
         * Creates a new instance of Tab with the specified values.
         * @param tabId the tabId of the Tab
         * @param tabName the tabName of the Tab
         */
        public Tab(Long tabId, String tabName)
        {
                this.tabId = tabId;
                this.tabName = tabName;
        }

        /**
         * Gets the tabId of this Tab.
         * @return the tabId
         */
        public Long getTabId()
        {
                return this.tabId;
        }

        /**
         * Sets the tabId of this Tab to the specified value.
         * @param tabId the new tabId
         */
        public void setTabId(Long tabId)
        {
                this.tabId = tabId;
        }

        /**
         * Gets the tabName of this Tab.
         * @return the tabName
         */
        public String getTabName()
        {
                return this.tabName;
        }

        /**
         * Sets the tabName of this Tab to the specified value.
         * @param tabName the new tabName
         */
        public void setTabName(String tabName)
        {
                this.tabName = tabName;
        }

        public Form getForm()
        {
                return form;
        }

        public void setForm(Form form)
        {
                this.form = form;
        }

        public Long getReferencedUnitProperty()
        {
                return referencedUnitProperty;
        }

        public void setReferencedUnitProperty(Long referencedUnitProperty)
        {
                this.referencedUnitProperty = referencedUnitProperty;
        }

        public Long getUnitId()
        {
                return unitId;
        }

        public void setUnitId(Long unitId)
        {
                this.unitId = unitId;
        }

        public Long getGridId()
        {
                return gridId;
        }

        public void setGridId(Long gridId)
        {
                this.gridId = gridId;
        }

        public Long getSequence()
        {
            return sequence;
        }

        public void setSequence(Long sequence)
        {
            this.sequence = sequence;
        }

        public String getCustomQuery()
        {
            return customQuery;
        }

        public void setCustomQuery(String customQuery)
        {
            this.customQuery = customQuery;
        }

        /**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.tabId != null ? this.tabId.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this Tab.  The result is 
         * <code>true</code> if and only if the argument is not null and is a Tab object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof Tab)) {
                        return false;
                }
                Tab other = (Tab)object;
                if (this.tabId != other.tabId && (this.tabId == null || !this.tabId.equals(other.tabId))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.Tab[tabId=" + tabId + "]";
        }
        
}
