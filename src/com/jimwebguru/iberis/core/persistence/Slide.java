/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "slides")
@XmlRootElement
public class Slide implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "caption")
	private String caption;

	@JoinColumn(name = "imageId", referencedColumnName = "imageid")
	@ManyToOne
	private Image image;

	@JoinColumn(name = "videoId", referencedColumnName = "id")
	@ManyToOne
	private Video video;

	@Column(name = "learnMoreText")
	private String learnMoreText;

	@Column(name = "learn_more_link")
	private String learnMoreUrl;

	@Lob
	@Column(name = "customContent")
	private String customContent;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Slide()
	{
	}

	public Slide(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getCaption()
	{
		return caption;
	}

	public void setCaption(String caption)
	{
		this.caption = caption;
	}

	public Image getImage()
	{
		return image;
	}

	public void setImage(Image slideImage)
	{
		this.image = slideImage;
	}

	public String getLearnMoreUrl()
	{
		return learnMoreUrl;
	}

	public void setLearnMoreUrl(String learnMoreLink)
	{
		this.learnMoreUrl = learnMoreLink;
	}

	public String getCustomContent()
	{
		return customContent;
	}

	public void setCustomContent(String customContent)
	{
		this.customContent = customContent;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public String getLearnMoreText()
	{
		return learnMoreText;
	}

	public void setLearnMoreText(String learnMoreText)
	{
		this.learnMoreText = learnMoreText;
	}

	public Video getVideo()
	{
		return video;
	}

	public void setVideo(Video video)
	{
		this.video = video;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Slide))
		{
			return false;
		}
		Slide other = (Slide) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "org.msep.persistence.Slide[ id=" + id + " ]";
	}

}
