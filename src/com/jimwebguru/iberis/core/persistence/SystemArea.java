package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "systemareas")
public class SystemArea implements Serializable 
{
        @Id
        @Column(name = "areaId", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long areaId;

        @Column(name = "areaName")
        private String areaName;

        @Column(name = "enabled")
        private Boolean enabled;

		@JoinTable(name = "systemunitsareas",
			joinColumns =
					{
							@JoinColumn(name = "systemAreaId", referencedColumnName = "areaId")
					},
			inverseJoinColumns =
					{
							@JoinColumn(name = "systemUnitId", referencedColumnName = "unitID")
					})
		@ManyToMany
		@OrderBy("unitName ASC")
		private Collection<SystemUnit> systemUnits;

        /** Creates a new instance of SystemArea */
        public SystemArea() 
        {
        }

        /**
         * Creates a new instance of SystemArea with the specified values.
         * @param areaId the areaId of the SystemArea
         */
        public SystemArea(Long areaId) 
        {
                this.areaId = areaId;
        }

        /**
         * Gets the areaId of this SystemArea.
         * @return the areaId
         */
        public Long getAreaId() 
        {
                return this.areaId;
        }

        /**
         * Sets the areaId of this SystemArea to the specified value.
         * @param areaId the new areaId
         */
        public void setAreaId(Long areaId) 
        {
                this.areaId = areaId;
        }

        /**
         * Gets the areaName of this SystemArea.
         * @return the areaName
         */
        public String getAreaName() 
        {
                return this.areaName;
        }

        /**
         * Sets the areaName of this SystemArea to the specified value.
         * @param areaName the new areaName
         */
        public void setAreaName(String areaName) 
        {
                this.areaName = areaName;
        }

        /**
         * Gets the enabled of this SystemArea.
         * @return the enabled
         */
        public Boolean getEnabled() 
        {
                return this.enabled;
        }

        /**
         * Sets the enabled of this SystemArea to the specified value.
         * @param enabled the new enabled
         */
        public void setEnabled(Boolean enabled) 
        {
                this.enabled = enabled;
        }

		public Collection<SystemUnit> getSystemUnits()
		{
			return systemUnits;
		}

		public void setSystemUnits(Collection<SystemUnit> systemUnits)
		{
			this.systemUnits = systemUnits;
		}

	/**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode() 
        {
                int hash = 0;
                hash += (this.areaId != null ? this.areaId.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this SystemArea.  The result is 
         * <code>true</code> if and only if the argument is not null and is a SystemArea object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object) 
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof SystemArea)) 
                {
                        return false;
                }
                SystemArea other = (SystemArea)object;
                if (this.areaId != other.areaId && (this.areaId == null || !this.areaId.equals(other.areaId))) 
                {
                        return false;
                }
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString() 
        {
                return "com.bizznetworx.iberis.core.SystemArea[areaId=" + areaId + "]";
        }
        
}
