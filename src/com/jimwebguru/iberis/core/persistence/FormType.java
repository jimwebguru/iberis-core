/*
 * Created on January 11, 2007, 2:27 PM
 *
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "formtypes")
public class FormType implements Serializable 
{

    @Id
    @Column(name = "formTypeId", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long formTypeId;

    @Column(name = "formTypeName")
    private String formTypeName;
    
    /** Creates a new instance of FormType */
    public FormType() 
    {
    }

    /**
     * Creates a new instance of FormType with the specified values.
     * @param formTypeId the formTypeId of the FormType
     */
    public FormType(Long formTypeId) 
    {
        this.formTypeId = formTypeId;
    }

    /**
     * Gets the formTypeId of this FormType.
     * @return the formTypeId
     */
    public Long getFormTypeId() 
    {
        return this.formTypeId;
    }

    /**
     * Sets the formTypeId of this FormType to the specified value.
     * @param formTypeId the new formTypeId
     */
    public void setFormTypeId(Long formTypeId) 
    {
        this.formTypeId = formTypeId;
    }

    /**
     * Gets the formTypeName of this FormType.
     * @return the formTypeName
     */
    public String getFormTypeName() 
    {
        return this.formTypeName;
    }

    /**
     * Sets the formTypeName of this FormType to the specified value.
     * @param formTypeName the new formTypeName
     */
    public void setFormTypeName(String formTypeName) 
    {
        this.formTypeName = formTypeName;
    }

    /**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.formTypeId != null ? this.formTypeId.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this FormType.  The result is 
     * <code>true</code> if and only if the argument is not null and is a FormType object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormType)) 
        {
            return false;
        }
        FormType other = (FormType)object;
        if (this.formTypeId != other.formTypeId && (this.formTypeId == null || !this.formTypeId.equals(other.formTypeId))) 
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.FormType[formTypeId=" + formTypeId + "]";
    }
    
}
