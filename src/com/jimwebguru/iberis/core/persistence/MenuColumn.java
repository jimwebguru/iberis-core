package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "menucolumns")
@XmlRootElement
public class MenuColumn implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "sequenceIndex")
	private Integer sequenceIndex;

	@ManyToOne
	@JoinColumn(name = "parentMenuItem", referencedColumnName = "id")
	private MenuItem parentMenuItem;

	@OneToMany(mappedBy = "menuColumn", fetch = FetchType.EAGER)
	@OrderBy("sequenceIndex")
	private Collection<MenuItem> subMenuItems;

	@Column(name = "customContent")
	private String customContent;

	public MenuColumn()
	{
	}

	public MenuColumn(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Integer getSequenceIndex()
	{
		return sequenceIndex;
	}

	public void setSequenceIndex(Integer sequenceIndex)
	{
		this.sequenceIndex = sequenceIndex;
	}

	public void setSubMenuItems(Collection<MenuItem> subMenuItems)
	{
		this.subMenuItems = subMenuItems;
	}

	public MenuItem getParentMenuItem()
	{
		return parentMenuItem;
	}

	public void setParentMenuItem(MenuItem parentMenu)
	{
		this.parentMenuItem = parentMenu;
	}

	public Collection<MenuItem> getSubMenuItems()
	{
		return subMenuItems;
	}

	public String getCustomContent()
	{
		return customContent;
	}

	public void setCustomContent(String customContent)
	{
		this.customContent = customContent;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof MenuColumn))
		{
			return false;
		}
		MenuColumn other = (MenuColumn) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.MenuColumn[ id=" + id + " ]";
	}

}
