package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "blogentries")
@XmlRootElement
public class BlogEntry implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "title")
	private String title;

	@JoinColumn(name = "blogId", referencedColumnName = "id")
	@ManyToOne
	private Blog blog;

	@JoinColumn(name = "headerImage", referencedColumnName = "imageid")
	@ManyToOne
	private Image headerImage;

	@JoinColumn(name = "bodyImage", referencedColumnName = "imageid")
	@ManyToOne
	private Image bodyImage;

	@Lob
	@Column(name = "body")
	private String body;

	@Lob
	@Column(name = "header")
	private String header;

	@Column(name = "author")
	private String author;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	@Column(name = "publishedDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date publishedDate;

	@Column(name = "bodyImageAlignment")
	private Integer bodyImageAlignment;

	@Column(name = "bodyImageWidth")
	private Integer bodyImageWidth;

	@Column(name = "headerImageMax")
	private Boolean headerImageMax;

	@Column(name = "showAuthorInfo")
	private Boolean showAuthorInfo;

	@Column(name = "published")
	private Boolean published;
	
	@Column(name = "metaTitle")
	private String metaTitle;

	@Column(name = "metaDescription")
	private String metaDescription;

	@Column(name = "urlAlias")
	private String urlAlias;

	@Column(name = "allowEditorStyles")
	private Boolean allowEditorStyles;
	
	@Column(name = "enableSocialSharing")
	private Boolean enableSocialSharing;

	@Column(name = "enableFacebook")
	private Boolean enableFacebook;

	@Column(name = "enableTwitter")
	private Boolean enableTwitter;

	@Column(name = "enableLinkedin")
	private Boolean enableLinkedin;

	@Column(name = "enableEmail")
	private Boolean enableEmail;

	@Column(name = "socialTitle")
	private String socialTitle;

	@Column(name = "socialDescription")
	private String socialDescription;

	public BlogEntry()
	{
	}

	public BlogEntry(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Blog getBlog()
	{
		return blog;
	}

	public void setBlog(Blog blog)
	{
		this.blog = blog;
	}

	public Image getHeaderImage()
	{
		return headerImage;
	}

	public void setHeaderImage(Image headerImage)
	{
		this.headerImage = headerImage;
	}

	public Image getBodyImage()
	{
		return bodyImage;
	}

	public void setBodyImage(Image bodyImage)
	{
		this.bodyImage = bodyImage;
	}

	public String getBody()
	{
		return body;
	}

	public void setBody(String body)
	{
		this.body = body;
	}

	public String getHeader()
	{
		return header;
	}

	public void setHeader(String header)
	{
		this.header = header;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public Date getPublishedDate()
	{
		return publishedDate;
	}

	public void setPublishedDate(Date publishedDate)
	{
		this.publishedDate = publishedDate;
	}

	public Integer getBodyImageAlignment()
	{
		return bodyImageAlignment;
	}

	public void setBodyImageAlignment(Integer bodyImageAlignment)
	{
		this.bodyImageAlignment = bodyImageAlignment;
	}

	public Integer getBodyImageWidth()
	{
		return bodyImageWidth;
	}

	public void setBodyImageWidth(Integer bodyImageWidth)
	{
		this.bodyImageWidth = bodyImageWidth;
	}

	public Boolean getHeaderImageMax()
	{
		return headerImageMax;
	}

	public void setHeaderImageMax(Boolean headerImageMax)
	{
		this.headerImageMax = headerImageMax;
	}

	public Boolean getShowAuthorInfo()
	{
		return showAuthorInfo;
	}

	public void setShowAuthorInfo(Boolean showAuthorInfo)
	{
		this.showAuthorInfo = showAuthorInfo;
	}

	public Boolean getPublished()
	{
		return published;
	}

	public void setPublished(Boolean published)
	{
		this.published = published;
	}

	public String getUrlAlias()
	{
		return urlAlias;
	}

	public void setUrlAlias(String urlAlias)
	{
		this.urlAlias = urlAlias;
	}

	public Boolean getAllowEditorStyles()
	{
		return allowEditorStyles;
	}

	public void setAllowEditorStyles(Boolean allowEditorStyles)
	{
		this.allowEditorStyles = allowEditorStyles;
	}
	
	public String getMetaTitle()
	{
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle)
	{
		this.metaTitle = metaTitle;
	}

	public String getMetaDescription()
	{
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription)
	{
		this.metaDescription = metaDescription;
	}
	
	public Boolean getEnableSocialSharing()
	{
		return enableSocialSharing;
	}

	public void setEnableSocialSharing(Boolean enableSocialSharing)
	{
		this.enableSocialSharing = enableSocialSharing;
	}

	public Boolean getEnableFacebook()
	{
		return enableFacebook;
	}

	public void setEnableFacebook(Boolean enableFacebook)
	{
		this.enableFacebook = enableFacebook;
	}

	public Boolean getEnableTwitter()
	{
		return enableTwitter;
	}

	public void setEnableTwitter(Boolean enableTwitter)
	{
		this.enableTwitter = enableTwitter;
	}

	public Boolean getEnableLinkedin()
	{
		return enableLinkedin;
	}

	public void setEnableLinkedin(Boolean enableLinkedin)
	{
		this.enableLinkedin = enableLinkedin;
	}

	public Boolean getEnableEmail()
	{
		return enableEmail;
	}

	public void setEnableEmail(Boolean enableEmail)
	{
		this.enableEmail = enableEmail;
	}

	public String getSocialTitle()
	{
		return socialTitle;
	}

	public void setSocialTitle(String socialTitle)
	{
		this.socialTitle = socialTitle;
	}

	public String getSocialDescription()
	{
		return socialDescription;
	}

	public void setSocialDescription(String socialDescription)
	{
		this.socialDescription = socialDescription;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof BlogEntry))
		{
			return false;
		}
		BlogEntry other = (BlogEntry) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}
}
