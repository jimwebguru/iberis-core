package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "notes")
public class Note
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "subject")
	private String subject;

	@Column(name = "content")
	private String content;

	@JoinColumn(name = "unitId", referencedColumnName = "unitID")
	@ManyToOne
	private SystemUnit systemUnit;

	@Column(name = "instanceId")
	private Long instanceId;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public SystemUnit getSystemUnit()
	{
		return systemUnit;
	}

	public void setSystemUnit(SystemUnit systemUnit)
	{
		this.systemUnit = systemUnit;
	}

	public Long getInstanceId()
	{
		return instanceId;
	}

	public void setInstanceId(Long instanceId)
	{
		this.instanceId = instanceId;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}
}
