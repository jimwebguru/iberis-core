package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "blogs")
@XmlRootElement
public class Blog implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "urlAlias")
	private String urlAlias;

	@Column(name = "readMoreLinkText")
	private String readMoreLinkText;
	
	@Column(name = "showListThumbnails")
	private Boolean showListThumbnails;
	
	@Column(name = "listThumbnailWidth")
	private Integer listThumbnailWidth;

	@JoinTable(name = "blogsites", joinColumns =
			{
					@JoinColumn(name = "blogId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	public Blog()
	{
	}

	public Blog(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUrlAlias()
	{
		return urlAlias;
	}

	public void setUrlAlias(String urlAlias)
	{
		this.urlAlias = urlAlias;
	}

	public String getReadMoreLinkText()
	{
		return readMoreLinkText;
	}

	public void setReadMoreLinkText(String readMoreLinkText)
	{
		this.readMoreLinkText = readMoreLinkText;
	}
	
	public Boolean getShowListThumbnails()
	{
		return showListThumbnails;
	}

	public void setShowListThumbnails(Boolean showThumbs)
	{
		this.showListThumbnails = showThumbs;
	}
	
	public Integer getListThumbnailWidth()
	{
		return listThumbnailWidth;
	}

	public void setListThumbnailWidth(Integer thumbWidth)
	{
		this.listThumbnailWidth = thumbWidth;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Blog))
		{
			return false;
		}
		Blog other = (Blog) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}
}
