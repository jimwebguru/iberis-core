package com.jimwebguru.iberis.core.persistence

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
enum class MenuType
/** Creates a new instance of PhoneType  */
constructor(val typeId: Int)
{
	Menu(0),
	SlideMenu(1),
	TieredMenu(2),
	MenuBar(3),
	PanelMenu(4),
	BreadCrumb(6),
	MegaMenu(7),
	MenuButton(8),
	FlatMenu(9)
}
