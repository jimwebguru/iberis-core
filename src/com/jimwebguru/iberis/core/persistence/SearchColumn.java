/*
 * SearchColumn.java
 *
 * Created on November 28, 2006, 2:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "searchcolumns")
public class SearchColumn implements Serializable 
{

    @Id
    @Column(name = "searchID", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long searchID;

    @JoinColumn(name = "unitProperty", referencedColumnName = "propertyId")
    @ManyToOne
    private UnitProperty unitProperty;
    
    @ManyToOne
    @JoinColumn(name="gridId")
    private Grid grid;

	@Column(name = "sequence")
	private Integer sequence;
    
    @JoinColumn(name = "conditionOperatorId",referencedColumnName = "operatorId")
    @ManyToOne
    private ConditionOperator conditionOperator;
    
    /** Creates a new instance of SearchColumn */
    public SearchColumn() 
    {
    }

    /**
     * Creates a new instance of SearchColumn with the specified values.
     * @param searchID the searchID of the SearchColumn
     */
    public SearchColumn(Long searchID) 
    {
        this.searchID = searchID;
    }

    /**
     * Creates a new instance of SearchColumn with the specified values.
     * @param searchID the searchID of the SearchColumn
     */
    public SearchColumn(Long searchID, UnitProperty unitProperty)
    {
        this.searchID = searchID;
        this.unitProperty = unitProperty;
    }

    /**
     * Gets the searchID of this SearchColumn.
     * @return the searchID
     */
    public Long getSearchID() 
    {
        return this.searchID;
    }

    /**
     * Sets the searchID of this SearchColumn to the specified value.
     * @param searchID the new searchID
     */
    public void setSearchID(Long searchID) 
    {
        this.searchID = searchID;
    }

    /**
     * Gets the columnName of this SearchColumn.
     * @return the columnName
     */
    public UnitProperty getUnitProperty() 
    {
        return this.unitProperty;
    }

    /**
     * Sets the columnName of this SearchColumn to the specified value.
     *
     */
    public void setUnitProperty(UnitProperty unitProperty) 
    {
        this.unitProperty = unitProperty;
    }

    public Grid getGrid() 
    {
        return grid;
    }

    public void setGrid(Grid grid) 
    {
        this.grid = grid;
    }

        public ConditionOperator getConditionOperator()
        {
                return conditionOperator;
        }

        public void setConditionOperator(ConditionOperator conditionOperator)
        {
                this.conditionOperator = conditionOperator;
        }

	public Integer getSequence()
	{
		return sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}

	/**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.searchID != null ? this.searchID.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this SearchColumn.  The result is 
     * <code>true</code> if and only if the argument is not null and is a SearchColumn object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SearchColumn)) 
        {
            return false;
        }
        SearchColumn other = (SearchColumn)object;
        if (this.searchID != other.searchID && (this.searchID == null || !this.searchID.equals(other.searchID))) 
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.SearchColumn[searchID=" + searchID + "]";
    }
    
}
