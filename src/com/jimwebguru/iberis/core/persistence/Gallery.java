package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "galleries")
public class Gallery
{
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private Integer type;

	@Column(name = "imagesPerSlide")
	private Integer imagesPerSlide;

	@Column(name = "showThumbnails")
	private Boolean showThumbnails;

	@Column(name = "showCaptions")
	private Boolean showCaptions;

	@Column(name = "slideMode")
	private String slideMode;

	@Column(name = "startSlide")
	private Integer startSlide;

	@Column(name = "showPager")
	private Boolean showPager;

	@Column(name = "showControls")
	private Boolean showControls;

	@Column(name = "autoSlide")
	private Boolean autoSlide;

	@Column(name = "autoStart")
	private Boolean autoStart;

	@Column(name = "pauseTime")
	private Integer pauseTime;

	@Column(name = "minHeight")
	private Integer minHeight;

	@Column(name = "minWidth")
	private Integer minWidth;

	@Column(name = "maxWidth")
	private Integer maxWidth;

	@Column(name = "maxHeight")
	private Integer maxHeight;

	@Column(name = "animationSpeed")
	private Integer animationSpeed;

	@Column(name = "showName")
	private Boolean showName;

	@Column(name = "imageMaxWidth")
	private Boolean imageMaxWidth;

	@Column(name = "imageWidth")
	private Integer imageWidth;

	@Column(name = "blockSize")
	private Integer blockSize;

	@Column(name = "alternatingBlockSize")
	private Integer alternatingBlockSize;

	@Column(name = "visibleThumbs")
	private Integer visibleThumbs;

	@Column(name = "flipSlides")
	private Boolean flipSlides;

	@Column(name = "captionAsOverlay")
	private Boolean captionAsOverlay;

	@Column(name = "autoHideControls")
	private Boolean autoHideControls;
	
	@Column(name = "allowEditorStyles")
	private Boolean allowEditorStyles;
	
	@Column(name = "showCaptionsBelowGallery")
	private Boolean showCaptionsBelowGallery;

	@OneToMany(mappedBy = "gallery", fetch = FetchType.EAGER)
	@OrderBy("sequence")
	private Collection<GalleryImage> galleryImages;

	@JoinTable(name = "gallerysites", joinColumns =
			{
					@JoinColumn(name = "galleryId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getType()
	{
		return type;
	}

	public void setType(Integer type)
	{
		this.type = type;
	}

	public Integer getImagesPerSlide()
	{
		return imagesPerSlide;
	}

	public void setImagesPerSlide(Integer imagesPerSlide)
	{
		this.imagesPerSlide = imagesPerSlide;
	}

	public Boolean getShowThumbnails()
	{
		return showThumbnails;
	}

	public void setShowThumbnails(Boolean showThumbnails)
	{
		this.showThumbnails = showThumbnails;
	}

	public Boolean getShowCaptions()
	{
		return showCaptions;
	}

	public void setShowCaptions(Boolean showCaptions)
	{
		this.showCaptions = showCaptions;
	}

	public Integer getAnimationSpeed()
	{
		return animationSpeed;
	}

	public void setAnimationSpeed(Integer animationSpeed)
	{
		this.animationSpeed = animationSpeed;
	}

	public Integer getStartSlide()
	{
		return startSlide;
	}

	public void setStartSlide(Integer startSlide)
	{
		this.startSlide = startSlide;
	}

	public Boolean getShowPager()
	{
		return showPager;
	}

	public void setShowPager(Boolean showPager)
	{
		this.showPager = showPager;
	}

	public Boolean getShowControls()
	{
		return showControls;
	}

	public void setShowControls(Boolean showControls)
	{
		this.showControls = showControls;
	}

	public Boolean getAutoSlide()
	{
		return autoSlide;
	}

	public void setAutoSlide(Boolean autoSlide)
	{
		this.autoSlide = autoSlide;
	}

	public Boolean getAutoStart()
	{
		return autoStart;
	}

	public void setAutoStart(Boolean autoStart)
	{
		this.autoStart = autoStart;
	}

	public Integer getPauseTime()
	{
		return pauseTime;
	}

	public void setPauseTime(Integer pauseTime)
	{
		this.pauseTime = pauseTime;
	}

	public Integer getMinHeight()
	{
		return minHeight;
	}

	public void setMinHeight(Integer minHeight)
	{
		this.minHeight = minHeight;
	}

	public Integer getMinWidth()
	{
		return minWidth;
	}

	public void setMinWidth(Integer minWidth)
	{
		this.minWidth = minWidth;
	}

	public Integer getMaxWidth()
	{
		return maxWidth;
	}

	public void setMaxWidth(Integer maxWidth)
	{
		this.maxWidth = maxWidth;
	}

	public Integer getMaxHeight()
	{
		return maxHeight;
	}

	public void setMaxHeight(Integer maxHeight)
	{
		this.maxHeight = maxHeight;
	}

	public Boolean getShowName()
	{
		return showName;
	}

	public void setShowName(Boolean showName)
	{
		this.showName = showName;
	}

	public Integer getImageWidth()
	{
		return imageWidth;
	}

	public void setImageWidth(Integer imageWidth)
	{
		this.imageWidth = imageWidth;
	}

	public Collection<GalleryImage> getGalleryImages()
	{
		return galleryImages;
	}

	public void setGalleryImages(Collection<GalleryImage> galleryImages)
	{
		this.galleryImages = galleryImages;
	}

	public Boolean getImageMaxWidth()
	{
		return imageMaxWidth;
	}

	public void setImageMaxWidth(Boolean imageMaxWidth)
	{
		this.imageMaxWidth = imageMaxWidth;
	}

	public Integer getBlockSize()
	{
		return blockSize;
	}

	public void setBlockSize(Integer blockSize)
	{
		this.blockSize = blockSize;
	}

	public Integer getAlternatingBlockSize()
	{
		return alternatingBlockSize;
	}

	public void setAlternatingBlockSize(Integer alternatingBlockSize)
	{
		this.alternatingBlockSize = alternatingBlockSize;
	}

	public Integer getVisibleThumbs()
	{
		return visibleThumbs;
	}

	public void setVisibleThumbs(Integer visibleThumbs)
	{
		this.visibleThumbs = visibleThumbs;
	}

	public Boolean getFlipSlides()
	{
		return flipSlides;
	}

	public void setFlipSlides(Boolean flipSlides)
	{
		this.flipSlides = flipSlides;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public String getSlideMode()
	{
		return slideMode;
	}

	public void setSlideMode(String slideMode)
	{
		this.slideMode = slideMode;
	}

	public Boolean getAutoHideControls()
	{
		return autoHideControls;
	}

	public void setAutoHideControls(Boolean autoHideControls)
	{
		this.autoHideControls = autoHideControls;
	}
	
	public Boolean getAllowEditorStyles()
	{
		return allowEditorStyles;
	}

	public void setAllowEditorStyles(Boolean allowStyles)
	{
		this.allowEditorStyles = allowStyles;
	}
	
	public Boolean getShowCaptionsBelowGallery()
	{
		return showCaptionsBelowGallery;
	}

	public void setShowCaptionsBelowGallery(Boolean showCaptionsBelow)
	{
		this.showCaptionsBelowGallery = showCaptionsBelow;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	public Boolean getCaptionAsOverlay()
	{
		return captionAsOverlay;
	}

	public void setCaptionAsOverlay(Boolean captionAsOverlay)
	{
		this.captionAsOverlay = captionAsOverlay;
	}
}
