/*
 * Product.java
 *
 * Created on July 10, 2007, 8:04 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import static org.primefaces.behavior.confirm.ConfirmBehavior.PropertyKeys.header;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "productcatalogs")
public class ProductCatalog implements Serializable
{
        @Id
        @Column(name = "id", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long id;

        @Column(name = "name", nullable = false)
        private String name;

        @Column(name = "urlAlias")
        private String urlAlias;

		@Column(name = "columns")
		private Integer columns;

		@Column(name = "layoutMode")
		private Integer layoutMode;

		@Column(name = "customTemplate")
		private String customTemplate;

        @JoinColumn(name = "siteId", referencedColumnName = "siteID")
        @ManyToOne
        private Site site;

		@Column(name = "showTitle")
		private Boolean showTitle;

		@Column(name = "titleSize")
		private String titleSize;

		@Lob
		@Column(name = "header")
		private String header;

		@Lob
		@Column(name = "subHeader")
		private String subHeader;

		@Lob
		@Column(name = "footer")
		private String footer;

		@Lob
		@Column(name = "subFooter")
		private String subFooter;

		@Column(name = "initialRows")
		private Integer initialRows;

		@Column(name = "pagerTemplate")
		private String pagerTemplate;

        @JoinTable(name = "productcataloggroups", joinColumns =
        {
            @JoinColumn(name = "catalogId", referencedColumnName = "id")
        }, inverseJoinColumns =
        {
            @JoinColumn(name = "groupId", referencedColumnName = "groupId")
        })
        @ManyToMany
        private Collection<ProductGroup> productGroups;

		@JoinTable(name = "productcatalogproducts", joinColumns =
				{
						@JoinColumn(name = "catalogId", referencedColumnName = "id")
				}, inverseJoinColumns =
				{
						@JoinColumn(name = "productId", referencedColumnName = "productId")
				})
		@ManyToMany
		private Collection<Product> products;

		@JoinTable(name = "productcatalogsites", joinColumns =
				{
						@JoinColumn(name = "catalogId", referencedColumnName = "id")
				}, inverseJoinColumns =
				{
						@JoinColumn(name = "siteId", referencedColumnName = "siteID")
				})
		@ManyToMany
		private Collection<Site> sites;

		@Column(name = "created")
		@Temporal(TemporalType.TIMESTAMP)
		private Date created;

		@Column(name = "modified")
		@Temporal(TemporalType.TIMESTAMP)
		private Date modified;

		public ProductCatalog()
		{

		}

        /**
         * Creates a new instance of Product with the specified values.
         * @param catalogId the productId of the Product
         */
        public ProductCatalog(Long catalogId)
        {
                this.id = catalogId;
        }

		public Long getId()
		{
			return id;
		}

		public void setId(Long id)
		{
			this.id = id;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public String getUrlAlias()
		{
			return urlAlias;
		}

		public void setUrlAlias(String urlAlias)
		{
			this.urlAlias = urlAlias;
		}

		public Integer getColumns()
		{
			return columns;
		}

		public void setColumns(Integer columns)
		{
			this.columns = columns;
		}

		public Site getSite()
		{
			return site;
		}

		public void setSite(Site site)
		{
			this.site = site;
		}

		public Collection<ProductGroup> getProductGroups()
		{
			return productGroups;
		}

		public void setProductGroups(Collection<ProductGroup> productGroups)
		{
			this.productGroups = productGroups;
		}

		public Collection<Product> getProducts()
		{
			return products;
		}

		public void setProducts(Collection<Product> products)
		{
			this.products = products;
		}

		public String getCustomTemplate()
		{
			return customTemplate;
		}

		public void setCustomTemplate(String customTemplate)
		{
			this.customTemplate = customTemplate;
		}

		public Boolean getShowTitle()
		{
			return showTitle;
		}

		public void setShowTitle(Boolean showTitle)
		{
			this.showTitle = showTitle;
		}

		public String getTitleSize()
		{
			return titleSize;
		}

		public void setTitleSize(String titleSize)
		{
			this.titleSize = titleSize;
		}

		public String getHeader()
		{
			return header;
		}

		public void setHeader(String header)
		{
			this.header = header;
		}

		public String getSubHeader()
		{
			return subHeader;
		}

		public void setSubHeader(String subHeader)
		{
			this.subHeader = subHeader;
		}

		public String getFooter()
		{
			return footer;
		}

		public void setFooter(String footer)
		{
			this.footer = footer;
		}

		public String getSubFooter()
		{
			return subFooter;
		}

		public void setSubFooter(String subFooter)
		{
			this.subFooter = subFooter;
		}

		public Integer getLayoutMode()
		{
			return layoutMode;
		}

		public void setLayoutMode(Integer layoutMode)
		{
			this.layoutMode = layoutMode;
		}

		public Integer getInitialRows()
		{
			return initialRows;
		}

		public void setInitialRows(Integer initialRows)
		{
			this.initialRows = initialRows;
		}

		public String getPagerTemplate()
		{
			return pagerTemplate;
		}

		public void setPagerTemplate(String pagerTemplate)
		{
			this.pagerTemplate = pagerTemplate;
		}

		public Collection<Site> getSites()
		{
			return sites;
		}

		public void setSites(Collection<Site> sites)
		{
			this.sites = sites;
		}

		public Date getCreated()
		{
			return created;
		}

		public void setCreated(Date created)
		{
			this.created = created;
		}

		public Date getModified()
		{
			return modified;
		}

		public void setModified(Date modified)
		{
			this.modified = modified;
		}

		@SuppressWarnings("unused")
		@PrePersist
		private void onInsert() {
			this.created = new Date();
			this.modified = this.created;
		}

		@SuppressWarnings("unused")
		@PreUpdate
		private void onUpdate() {
			this.modified = new Date();
		}

		/**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.id != null ? this.id.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this Product.  The result is 
         * <code>true</code> if and only if the argument is not null and is a Product object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof ProductCatalog)) {
                        return false;
                }
                ProductCatalog other = (ProductCatalog)object;
                if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.ProductCatalog[id=" + id + "]";
        }
        
}
