package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "pageitemassignments")
@XmlRootElement
public class PageItemAssignment
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@JoinColumn(name = "pageRegion", referencedColumnName = "id")
	@ManyToOne
	private PageRegion pageRegion;

	@Column(name = "pageItem", nullable = false)
	private Long pageItem;

	@Column(name = "pageItemInstanceId", nullable = false)
	private Long pageItemInstanceId;

	@JoinColumn(name = "siteId", referencedColumnName = "siteID")
	@ManyToOne
	private Site site;

	@Column(name = "sequence")
	private Integer sequence;

	@Column(name = "printable")
	private Boolean printable;

	@Column(name = "mobileOnly")
	private Boolean mobileOnly;

	@Column(name = "desktopOnly")
	private Boolean desktopOnly;

	@Column(name = "removeSpacer")
	private Boolean removeSpacer;

	@Column(name = "scope")
	private String scope;

	@Column(name = "useWrapper")
	private Boolean useWrapper;

	@Lob
	@Column(name = "beginWrapperHtml")
	private String beginWrapperHtml;

	@Lob
	@Column(name = "endWrapperHtml")
	private String endWrapperHtml;

	@OneToMany(mappedBy = "pageItemAssignment")
	/*@ElementCollection
	@CollectionTable(
			name="allowedurls",
			joinColumns=@JoinColumn(name="pageItemAssignmentId")
	)
	@Column(name="url")*/
	private Collection<AllowedUrl> allowedUrls;

	@Transient
	private String allowedUrls_temp;

	@OneToMany(mappedBy = "pageItemAssignment")
	/*@ElementCollection
	@CollectionTable(
			name="deniedurls",
			joinColumns=@JoinColumn(name="pageItemAssignmentId")
	)
	@Column(name="url")*/
	private Collection<DeniedUrl> deniedUrls;

	@Transient
	private String deniedUrls_temp;

	@Transient
	private Boolean pageRendered;

	@Transient
	private Boolean inScope;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public PageRegion getPageRegion()
	{
		return pageRegion;
	}

	public void setPageRegion(PageRegion pageRegion)
	{
		this.pageRegion = pageRegion;
	}

	public Long getPageItem()
	{
		return pageItem;
	}

	public void setPageItem(Long pageItem)
	{
		this.pageItem = pageItem;
	}

	public Long getPageItemInstanceId()
	{
		return pageItemInstanceId;
	}

	public void setPageItemInstanceId(Long pageItemInstanceId)
	{
		this.pageItemInstanceId = pageItemInstanceId;
	}

	public Site getSite()
	{
		return site;
	}

	public void setSite(Site site)
	{
		this.site = site;
	}

	public Integer getSequence()
	{
		return sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}

	public Collection<AllowedUrl> getAllowedUrls()
	{
		return allowedUrls;
	}

	public void setAllowedUrls(Collection<AllowedUrl> allowedUrls)
	{
		this.allowedUrls = allowedUrls;
	}

	public Collection<DeniedUrl> getDeniedUrls()
	{
		return deniedUrls;
	}

	public void setDeniedUrls(Collection<DeniedUrl> deniedUrls)
	{
		this.deniedUrls = deniedUrls;
	}

	public String getAllowedUrls_temp()
	{
		return allowedUrls_temp;
	}

	public void setAllowedUrls_temp(String allowedUrls_temp)
	{
		this.allowedUrls_temp = allowedUrls_temp;
	}

	public String getDeniedUrls_temp()
	{
		return deniedUrls_temp;
	}

	public void setDeniedUrls_temp(String deniedUrls_temp)
	{
		this.deniedUrls_temp = deniedUrls_temp;
	}

	public Boolean getPrintable()
	{
		return printable;
	}

	public void setPrintable(Boolean printable)
	{
		this.printable = printable;
	}

	public Boolean getMobileOnly()
	{
		return mobileOnly;
	}

	public void setMobileOnly(Boolean mobileOnly)
	{
		this.mobileOnly = mobileOnly;
	}

	public Boolean getDesktopOnly()
	{
		return desktopOnly;
	}

	public void setDesktopOnly(Boolean desktopOnly)
	{
		this.desktopOnly = desktopOnly;
	}

	public Boolean getPageRendered()
	{
		return pageRendered;
	}

	public void setPageRendered(Boolean pageRendered)
	{
		this.pageRendered = pageRendered;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public Boolean getRemoveSpacer()
	{
		return removeSpacer;
	}

	public void setRemoveSpacer(Boolean removeSpacer)
	{
		this.removeSpacer = removeSpacer;
	}

	public String getScope()
	{
		return scope;
	}

	public void setScope(String scope)
	{
		this.scope = scope;
	}

	public Boolean getInScope()
	{
		return inScope;
	}

	public void setInScope(Boolean inScope)
	{
		this.inScope = inScope;
	}

	public Boolean getUseWrapper()
	{
		return useWrapper;
	}

	public void setUseWrapper(Boolean useWrapper)
	{
		this.useWrapper = useWrapper;
	}

	public String getBeginWrapperHtml()
	{
		return beginWrapperHtml;
	}

	public void setBeginWrapperHtml(String beginWrapperHtml)
	{
		this.beginWrapperHtml = beginWrapperHtml;
	}

	public String getEndWrapperHtml()
	{
		return endWrapperHtml;
	}

	public void setEndWrapperHtml(String endWrapperHtml)
	{
		this.endWrapperHtml = endWrapperHtml;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}
}
