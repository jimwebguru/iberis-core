/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "fileextensions")
public class FileExtension implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "extensionid")
    private Long extensionid;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    public FileExtension()
    {
    }

    public FileExtension(Long extensionid)
    {
        this.extensionid = extensionid;
    }

    public FileExtension(Long extensionid, String name)
    {
        this.extensionid = extensionid;
        this.name = name;
    }

    public Long getExtensionid()
    {
        return extensionid;
    }

    public void setExtensionid(Long extensionid)
    {
        this.extensionid = extensionid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (extensionid != null ? extensionid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FileExtension))
        {
            return false;
        }
        FileExtension other = (FileExtension) object;
        if ((this.extensionid == null && other.extensionid != null) || (this.extensionid != null && !this.extensionid.equals(other.extensionid)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.FileExtension[extensionid=" + extensionid + "]";
    }

}
