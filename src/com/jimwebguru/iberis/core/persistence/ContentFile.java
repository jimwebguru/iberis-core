package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "contentfiles")
public class ContentFile
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@JoinColumn(name = "contentId", referencedColumnName = "id")
	@ManyToOne
	private Content content;

	@JoinColumn(name = "fileId", referencedColumnName = "id")
	@ManyToOne
	private File file;

	@Column(name = "sequence")
	private Integer sequence;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Content getContent()
	{
		return content;
	}

	public void setContent(Content content)
	{
		this.content = content;
	}

	public File getFile()
	{
		return file;
	}

	public void setFile(File file)
	{
		this.file = file;
	}

	public Integer getSequence()
	{
		return sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}
}
