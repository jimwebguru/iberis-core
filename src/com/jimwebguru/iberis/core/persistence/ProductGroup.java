/*
 * ProductGroup.java
 *
 * Created on July 10, 2007, 8:04 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "productgroups")
public class ProductGroup implements Serializable
{

        @Id
        @Column(name = "groupId", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long groupId;

        @Column(name = "groupName", nullable = false)
        private String groupName;

        @Column(name = "markup")
        private Double markup;

        @JoinColumn(name = "parentGroupId", referencedColumnName = "groupId")
        @ManyToOne
        private ProductGroup parentGroup;

        @JoinTable(name = "productgroupsites", joinColumns =
                {
                        @JoinColumn(name = "groupId", referencedColumnName = "groupId")
                }, inverseJoinColumns =
                {
                        @JoinColumn(name = "siteId", referencedColumnName = "siteID")
                })
        @ManyToMany
        private Collection<Site> sites;

		@Column(name = "created")
		@Temporal(TemporalType.TIMESTAMP)
		private Date created;

		@Column(name = "modified")
		@Temporal(TemporalType.TIMESTAMP)
		private Date modified;
        
        /** Creates a new instance of ProductGroup */
        public ProductGroup()
        {
        }

        /**
         * Creates a new instance of ProductGroup with the specified values.
         * @param groupId the groupId of the ProductGroup
         */
        public ProductGroup(Long groupId)
        {
                this.groupId = groupId;
        }

        /**
         * Creates a new instance of ProductGroup with the specified values.
         * @param groupId the groupId of the ProductGroup
         * @param groupName the groupName of the ProductGroup
         */
        public ProductGroup(Long groupId, String groupName)
        {
                this.groupId = groupId;
                this.groupName = groupName;
        }

        /**
         * Gets the groupId of this ProductGroup.
         * @return the groupId
         */
        public Long getGroupId()
        {
                return this.groupId;
        }

        /**
         * Sets the groupId of this ProductGroup to the specified value.
         * @param groupId the new groupId
         */
        public void setGroupId(Long groupId)
        {
                this.groupId = groupId;
        }

        /**
         * Gets the groupName of this ProductGroup.
         * @return the groupName
         */
        public String getGroupName()
        {
                return this.groupName;
        }

        /**
         * Sets the groupName of this ProductGroup to the specified value.
         * @param groupName the new groupName
         */
        public void setGroupName(String groupName)
        {
                this.groupName = groupName;
        }

        /**
         * Gets the markup of this ProductGroup.
         * @return the markup
         */
        public Double getMarkup()
        {
                return this.markup;
        }

        /**
         * Sets the markup of this ProductGroup to the specified value.
         * @param markup the new markup
         */
        public void setMarkup(Double markup)
        {
                this.markup = markup;
        }

        /**
         * Gets the parentGroupId of this ProductGroup.
         * @return the parentGroupId
         */
        public ProductGroup getParentGroup()
        {
                return this.parentGroup;
        }

        /**
         * Sets the parentGroupId of this ProductGroup to the specified value.
         * @param parentGroupId the new parentGroupId
         */
        public void setParentGroup(ProductGroup parentGroup)
        {
                this.parentGroup = parentGroup;
        }

		public Collection<Site> getSites()
		{
			return sites;
		}

		public void setSites(Collection<Site> sites)
		{
			this.sites = sites;
		}

		public Date getCreated()
		{
			return created;
		}

		public void setCreated(Date created)
		{
			this.created = created;
		}

		public Date getModified()
		{
			return modified;
		}

		public void setModified(Date modified)
		{
			this.modified = modified;
		}

		@SuppressWarnings("unused")
		@PrePersist
		private void onInsert() {
			this.created = new Date();
			this.modified = this.created;
		}

		@SuppressWarnings("unused")
		@PreUpdate
		private void onUpdate() {
			this.modified = new Date();
		}

		/**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.groupId != null ? this.groupId.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this ProductGroup.  The result is 
         * <code>true</code> if and only if the argument is not null and is a ProductGroup object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof ProductGroup)) {
                        return false;
                }
                ProductGroup other = (ProductGroup)object;
                if (this.groupId != other.groupId && (this.groupId == null || !this.groupId.equals(other.groupId))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.ProductGroup[groupId=" + groupId + "]";
        }
        
}
