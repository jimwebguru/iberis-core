package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "faqs")
@XmlRootElement
public class FAQ implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Lob
	@Column(name = "question")
	private String question;

	@Lob
	@Column(name = "answer")
	private String answer;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	@JoinTable(name = "faqcategory", joinColumns = {
			@JoinColumn(name = "faq_id", referencedColumnName = "id")}, inverseJoinColumns = {
			@JoinColumn(name = "term_id", referencedColumnName = "id")})
	@ManyToMany
	private Collection<VocabTerm> vocabTermCollection;

	public FAQ()
	{
	}

	public FAQ(Long id)
	{
		this.id = id;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getQuestion()
	{
		return question;
	}

	public void setQuestion(String question)
	{
		this.question = question;
	}

	public String getAnswer()
	{
		return answer;
	}

	public void setAnswer(String answer)
	{
		this.answer = answer;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	@XmlTransient
	public Collection<VocabTerm> getVocabTermCollection()
	{
		return vocabTermCollection;
	}

	public void setVocabTermCollection(Collection<VocabTerm> vocabTermCollection)
	{
		this.vocabTermCollection = vocabTermCollection;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof FAQ))
		{
			return false;
		}
		FAQ other = (FAQ) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.FAQ[ id=" + id + " ]";
	}

}