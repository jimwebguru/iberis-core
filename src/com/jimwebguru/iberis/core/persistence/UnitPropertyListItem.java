/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "unitpropertylistitems")
public class UnitPropertyListItem implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "itemId")
    private Long itemId;

    @Basic(optional = false)
    @Column(name = "itemValue")
    private String itemValue;
    
    @Basic(optional = false)
    @Column(name = "displayValue")
    private String displayValue;
    
    @JoinColumn(name = "unitPropertyId", referencedColumnName = "propertyId")
    @ManyToOne
    private UnitProperty unitProperty;

    public UnitPropertyListItem()
    {
    }

    public UnitPropertyListItem(Long itemId)
    {
        this.itemId = itemId;
    }

    public UnitPropertyListItem(Long itemId, String value, String displayValue)
    {
        this.itemId = itemId;
        this.itemValue = value;
        this.displayValue = displayValue;
    }

    public Long getItemId()
    {
        return itemId;
    }

    public void setItemId(Long itemId)
    {
        this.itemId = itemId;
    }

    public String getItemValue()
    {
        return itemValue;
    }

    public void setItemValue(String value)
    {
        this.itemValue = value;
    }

    public String getDisplayValue()
    {
        return displayValue;
    }

    public void setDisplayValue(String displayValue)
    {
        this.displayValue = displayValue;
    }

    public UnitProperty getUnitProperty()
    {
        return unitProperty;
    }

    public void setUnitProperty(UnitProperty unitProperty)
    {
        this.unitProperty = unitProperty;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (itemId != null ? itemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UnitPropertyListItem))
        {
            return false;
        }

        UnitPropertyListItem other = (UnitPropertyListItem) object;

        if ((this.itemId == null && other.itemId != null) || (this.itemId != null && !this.itemId.equals(other.itemId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.UnitPropertyListItem[itemId=" + itemId + "]";
    }

}
