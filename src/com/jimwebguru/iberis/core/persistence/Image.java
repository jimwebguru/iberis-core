/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "images")
public class Image implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "imageid")
    private Long imageid;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    @Column(name = "width")
    private Long width;

    @Column(name = "height")
    private Long height;

    @Basic(fetch=FetchType.LAZY)
    @Lob
    @Column(name = "imagecontents")
    private byte[] imagecontents;

    @JoinColumn(name = "mimetypeid", referencedColumnName = "typeid")
    @ManyToOne(optional = false)
    private MimeType mimeType;

    @JoinColumn(name = "fileextensionid", referencedColumnName = "extensionid")
    @ManyToOne(optional = false)
    private FileExtension fileExtension;

    @Column(name = "perspective")
    private Long perspective;

    @Column(name ="filePath")
    private String filePath;

	@Column(name ="caption")
	private String caption;

	@Column(name = "showCaption")
	private Boolean showCaption;

	@Column(name = "captionAsOverlay")
	private Boolean captionAsOverlay;

	@Column(name = "showFullWidth")
	private Boolean showFullWidth;

	@Column(name = "fullOverlay")
	private Boolean fullOverlay;

	@Column(name ="overlayColor")
	private String overlayColor;

	@Column(name ="captionTextColor")
	private String captionTextColor;

	@Column(name ="sideOverlay")
	private String sideOverlay;

    @JoinTable(name = "imagetags", joinColumns =
            {
                    @JoinColumn(name = "imageId", referencedColumnName = "imageid")
            }, inverseJoinColumns =
            {
                    @JoinColumn(name = "tagId", referencedColumnName = "id")
            })
    @ManyToMany
    private Collection<VocabTerm> imageTags;

    @JoinTable(name = "imagesites", joinColumns =
            {
                    @JoinColumn(name = "imageId", referencedColumnName = "imageid")
            }, inverseJoinColumns =
            {
                    @JoinColumn(name = "siteId", referencedColumnName = "siteID")
            })
    @ManyToMany
    private Collection<Site> sites;

    public Image()
    {
    }

    public Image(Long imageid)
    {
        this.imageid = imageid;
    }

    public Image(Long imageid, String name, byte[] imagecontents)
    {
        this.imageid = imageid;
        this.name = name;
        this.imagecontents = imagecontents;
    }

    public Long getImageid()
    {
        return imageid;
    }

    public void setImageid(Long imageid)
    {
        this.imageid = imageid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getWidth()
    {
        return width;
    }

    public void setWidth(Long width)
    {
        this.width = width;
    }

    public Long getHeight()
    {
        return height;
    }

    public void setHeight(Long height)
    {
        this.height = height;
    }

    public byte[] getImagecontents()
    {
        return imagecontents;
    }

    public void setImagecontents(byte[] imagecontents)
    {
        this.imagecontents = imagecontents;
    }

    public MimeType getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(MimeType mimeType)
    {
        this.mimeType = mimeType;
    }

    public FileExtension getFileExtension()
    {
        return fileExtension;
    }

    public void setFileExtension(FileExtension fileExtension)
    {
        this.fileExtension = fileExtension;
    }

    public Long getPerspective()
    {
        return perspective;
    }

    public void setPerspective(Long perspective)
    {
        this.perspective = perspective;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

	public Collection<VocabTerm> getImageTags()
	{
		return imageTags;
	}

	public void setImageTags(Collection<VocabTerm> imageTags)
	{
		this.imageTags = imageTags;
	}

    public Collection<Site> getSites()
    {
        return sites;
    }

    public void setSites(Collection<Site> sites)
    {
        this.sites = sites;
    }

	public String getCaption()
	{
		return caption;
	}

	public void setCaption(String caption)
	{
		this.caption = caption;
	}

	public Boolean getShowCaption()
	{
		return showCaption;
	}

	public void setShowCaption(Boolean showCaption)
	{
		this.showCaption = showCaption;
	}

	public Boolean getCaptionAsOverlay()
	{
		return captionAsOverlay;
	}

	public void setCaptionAsOverlay(Boolean captionAsOverlay)
	{
		this.captionAsOverlay = captionAsOverlay;
	}

	public String getOverlayColor()
	{
		return overlayColor;
	}

	public void setOverlayColor(String overlayColor)
	{
		this.overlayColor = overlayColor;
	}

	public String getCaptionTextColor()
	{
		return captionTextColor;
	}

	public void setCaptionTextColor(String captionTextColor)
	{
		this.captionTextColor = captionTextColor;
	}

	public Boolean getShowFullWidth()
	{
		return showFullWidth;
	}

	public void setShowFullWidth(Boolean showFullWidth)
	{
		this.showFullWidth = showFullWidth;
	}

	public Boolean getFullOverlay()
	{
		return fullOverlay;
	}

	public void setFullOverlay(Boolean fullOverlay)
	{
		this.fullOverlay = fullOverlay;
	}

	public String getSideOverlay()
	{
		return sideOverlay;
	}

	public void setSideOverlay(String sideOverlay)
	{
		this.sideOverlay = sideOverlay;
	}

	@Override
    public int hashCode()
    {
        int hash = 0;
        hash += (imageid != null ? imageid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Image))
        {
            return false;
        }
        Image other = (Image) object;
        if ((this.imageid == null && other.imageid != null) || (this.imageid != null && !this.imageid.equals(other.imageid)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.Image[imageid=" + imageid + "]";
    }

}
