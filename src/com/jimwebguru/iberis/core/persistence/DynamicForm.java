/*
 *
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "dynamicforms")
public class DynamicForm implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "useSiteEmail")
    private Boolean useSiteEmail;

    @Column(name = "email")
    private String email;

    @Column(name = "confirmation")
    private String confirmation;

    @Column(name = "showTitle")
    private Boolean showTitle;

    @Column(name = "submitButtonText")
    private String submitButtonText;

    @Column(name = "header")
    private String header;

    @Column(name = "footer")
    private String footer;

    @Column(name = "sectionsType")
    private Integer sectionsType;

    @Column(name = "sendEmail")
    private Boolean sendEmail;

	@Column(name = "sendConfirmationEmail")
	private Boolean sendConfirmationEmail;

    @Column(name = "emailTemplate")
    private String emailTemplate;

	@Column(name = "emailSubject")
	private String emailSubject;

	@Column(name = "confirmationEmailSubject")
	private String confirmationEmailSubject;

	@Column(name = "confirmationEmailTemplate")
	private String confirmationEmailTemplate;

	@Column(name = "urlAlias")
	private String urlAlias;

	@JoinColumn(name = "pageTemplateId", referencedColumnName = "id")
	@ManyToOne
	private Layout pageTemplate;

	@Column(name = "titleSize")
	private String titleSize;

    @JoinColumn(name = "recipientEmailField", referencedColumnName = "id")
    @ManyToOne
    private DynamicFormField recipientEmailField;

	@OneToMany(mappedBy = "form")
    @OrderBy("sequence")
    private Collection<DynamicFormSection> formSections;

	@OneToMany(mappedBy = "form")
	private Collection<DynamicFormField> formFields;

	@Transient
	private Boolean submitted;

	@Column(name = "metaTitle")
	private String metaTitle;

	@Column(name = "metaDescription")
	private String metaDescription;

	@Column(name = "customContentTemplate")
	private String customContentTemplate;

	@Column(name = "buttonCssClasses")
	private String buttonCssClasses;

	@Column(name = "useCaptcha")
	private Boolean useCaptcha;

	@JoinTable(name = "dynamicformsites", joinColumns =
			{
					@JoinColumn(name = "formId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

    /** Creates a new instance of Form */
    public DynamicForm()
    {
    }

    /**
     * Creates a new instance of Form with the specified values.
     * @param id the id of the Form
     */
    public DynamicForm(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Boolean getUseSiteEmail()
    {
        return useSiteEmail;
    }

    public void setUseSiteEmail(Boolean useSiteEmail)
    {
        this.useSiteEmail = useSiteEmail;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getConfirmation()
    {
        return confirmation;
    }

    public void setConfirmation(String confirmation)
    {
        this.confirmation = confirmation;
    }

    public Boolean getShowTitle()
    {
        return showTitle;
    }

    public void setShowTitle(Boolean showTitle)
    {
        this.showTitle = showTitle;
    }

    public String getSubmitButtonText()
    {
        return submitButtonText;
    }

    public void setSubmitButtonText(String submitButtonText)
    {
        this.submitButtonText = submitButtonText;
    }

    public String getHeader()
    {
        return header;
    }

    public void setHeader(String header)
    {
        this.header = header;
    }

    public String getFooter()
    {
        return footer;
    }

    public void setFooter(String footer)
    {
        this.footer = footer;
    }

    public Integer getSectionsType()
    {
        return sectionsType;
    }

    public void setSectionsType(Integer sectionsType)
    {
        this.sectionsType = sectionsType;
    }

    public Boolean getSendEmail()
    {
        return sendEmail;
    }

    public void setSendEmail(Boolean sendEmail)
    {
        this.sendEmail = sendEmail;
    }

    public String getEmailTemplate()
    {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate)
    {
        this.emailTemplate = emailTemplate;
    }

    public Collection<DynamicFormSection> getFormSections()
    {
        return formSections;
    }

    public void setFormSections(Collection<DynamicFormSection> formSections)
    {
        this.formSections = formSections;
    }

	public Collection<DynamicFormField> getFormFields()
	{
		return formFields;
	}

	public void setFormFields(Collection<DynamicFormField> formFields)
	{
		this.formFields = formFields;
	}

	public String getConfirmationEmailTemplate()
	{
		return confirmationEmailTemplate;
	}

	public void setConfirmationEmailTemplate(String confirmationEmailTemplate)
	{
		this.confirmationEmailTemplate = confirmationEmailTemplate;
	}

	public String getUrlAlias()
	{
		return urlAlias;
	}

	public void setUrlAlias(String urlAlias)
	{
		this.urlAlias = urlAlias;
	}

	public Boolean getSubmitted()
	{
		return submitted;
	}

	public void setSubmitted(Boolean submitted)
	{
		this.submitted = submitted;
	}

	public Layout getPageTemplate()
	{
		return pageTemplate;
	}

	public void setPageTemplate(Layout pageTemplate)
	{
		this.pageTemplate = pageTemplate;
	}

	public String getTitleSize()
	{
		return titleSize;
	}

	public void setTitleSize(String titleSize)
	{
		this.titleSize = titleSize;
	}

	public DynamicFormField getRecipientEmailField()
	{
		return recipientEmailField;
	}

	public void setRecipientEmailField(DynamicFormField recipientEmailField)
	{
		this.recipientEmailField = recipientEmailField;
	}

	public String getMetaTitle()
	{
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle)
	{
		this.metaTitle = metaTitle;
	}

	public String getMetaDescription()
	{
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription)
	{
		this.metaDescription = metaDescription;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	public String getCustomContentTemplate()
	{
		return customContentTemplate;
	}

	public void setCustomContentTemplate(String customContentTemplate)
	{
		this.customContentTemplate = customContentTemplate;
	}

	public String getButtonCssClasses()
	{
		return buttonCssClasses;
	}

	public void setButtonCssClasses(String buttonCssClasses)
	{
		this.buttonCssClasses = buttonCssClasses;
	}

	public Boolean getUseCaptcha()
	{
		return useCaptcha;
	}

	public void setUseCaptcha(Boolean useCaptcha)
	{
		this.useCaptcha = useCaptcha;
	}

	public Boolean getSendConfirmationEmail()
	{
		return sendConfirmationEmail;
	}

	public void setSendConfirmationEmail(Boolean sendConfirmationEmail)
	{
		this.sendConfirmationEmail = sendConfirmationEmail;
	}

	public String getEmailSubject()
	{
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject)
	{
		this.emailSubject = emailSubject;
	}

	public String getConfirmationEmailSubject()
	{
		return confirmationEmailSubject;
	}

	public void setConfirmationEmailSubject(String confirmationEmailSubject)
	{
		this.confirmationEmailSubject = confirmationEmailSubject;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	/**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Form.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Form object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DynamicForm))
        {
            return false;
        }
        
        DynamicForm other = (DynamicForm)object;
        
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.DynamicForm[id=" + id + "]";
    }
    
}
