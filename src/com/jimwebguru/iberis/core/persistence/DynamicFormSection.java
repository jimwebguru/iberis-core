/*
 *
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "dynamicformsections")
public class DynamicFormSection implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "header")
    private String header;

    @Column(name = "footer")
    private String footer;

    @Column(name = "sequence")
    private Integer sequence;

	@Column(name = "showTitle")
	private Boolean showTitle;

    @JoinColumn(name = "formId", referencedColumnName = "id")
    @ManyToOne
    private DynamicForm form;

    @OneToMany(mappedBy = "formSection")
    @OrderBy("sequence")
    private Collection<DynamicFormField> fields;

    /** Creates a new instance of Form */
    public DynamicFormSection()
    {
    }

    /**
     * Creates a new instance of Form with the specified values.
     * @param id the formId of the Form
     */
    public DynamicFormSection(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getHeader()
    {
        return header;
    }

    public void setHeader(String header)
    {
        this.header = header;
    }

    public String getFooter()
    {
        return footer;
    }

    public void setFooter(String footer)
    {
        this.footer = footer;
    }

    public  Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public DynamicForm getForm()
    {
        return form;
    }

    public void setForm(DynamicForm form)
    {
        this.form = form;
    }

    public Collection<DynamicFormField> getFields()
    {
        return fields;
    }

    public void setFields(Collection<DynamicFormField> fields)
    {
        this.fields = fields;
    }

	public Boolean getShowTitle()
	{
		return showTitle;
	}

	public void setShowTitle(Boolean showTitle)
	{
		this.showTitle = showTitle;
	}

	/**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Form.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Form object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DynamicFormSection))
        {
            return false;
        }
        
        DynamicFormSection other = (DynamicFormSection)object;
        
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.DynamicFormSection[id=" + id + "]";
    }
    
}
