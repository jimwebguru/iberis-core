/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "mimetypes")
public class MimeType implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "typeid")
    private Long typeid;

    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    public MimeType()
    {
    }

    public MimeType(Long typeid)
    {
        this.typeid = typeid;
    }

    public MimeType(Long typeid, String name)
    {
        this.typeid = typeid;
        this.name = name;
    }

    public Long getTypeid()
    {
        return typeid;
    }

    public void setTypeid(Long typeid)
    {
        this.typeid = typeid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (typeid != null ? typeid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MimeType))
        {
            return false;
        }
        MimeType other = (MimeType) object;
        if ((this.typeid == null && other.typeid != null) || (this.typeid != null && !this.typeid.equals(other.typeid)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.MimeType[typeid=" + typeid + "]";
    }

}
