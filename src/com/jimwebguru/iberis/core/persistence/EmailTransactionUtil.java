/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;
/*
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
*/
/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
public class EmailTransactionUtil
{/*
    public static boolean sendEmailTransaction(EmailTransaction email)
    {
        try
        {
            Session iberisServicesEmail = getIberisServicesEmail();

            MimeMessage message = new MimeMessage(iberisServicesEmail);
            message.setSubject(email.getSubject());
            message.setRecipients(RecipientType.TO, InternetAddress.parse(email.getToEmail(), false));

            if(email.getAdditionalEmailRecipientCollection() != null && !email.getAdditionalEmailRecipientCollection().isEmpty())
            {
                InternetAddress[] ccAddresses = new InternetAddress[email.getAdditionalEmailRecipientCollection().size()];

                int i = 0;

                for(AdditionalEmailRecipient ccEmail : email.getAdditionalEmailRecipientCollection())
                {
                    ccAddresses[i] = new InternetAddress();
                    ccAddresses[i].setAddress(ccEmail.getEmailAddress());
                    i++;
                }

                message.setRecipients(RecipientType.CC, ccAddresses);
            }

            message.addFrom(InternetAddress.parse(email.getFromEmail()));
            message.setDataHandler(new DataHandler(new ByteArrayDataSource(email.getBody(),"text/html")));
            Transport.send(message);
        }
        catch(MessagingException mex)
        {
            Logger.getLogger(EmailTransactionUtil.class.getName()).log(Level.SEVERE,"exception caught" ,mex);
            return false;
        }
        catch(NamingException nex)
        {
            Logger.getLogger(EmailTransactionUtil.class.getName()).log(Level.SEVERE,"exception caught" ,nex);
            return false;
        }
        catch(IOException ioex)
        {
            Logger.getLogger(EmailTransactionUtil.class.getName()).log(Level.SEVERE,"exception caught" ,ioex);
            return false;
        }

        return true;
    }

    private static Session getIberisServicesEmail() throws NamingException
    {
        Context c = new InitialContext();

        return (Session) c.lookup("java:comp/env/IberisServicesEmail");
    }*/
}
