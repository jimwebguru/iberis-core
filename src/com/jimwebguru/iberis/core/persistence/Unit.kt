/*
 * Unit.java
 *
 * Created on September 19, 2006, 10:28 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
enum class Unit constructor(val unitId: Int)
{
	CLIENT(1),
	COMPANY(2),
	EMPLOYEE(3),
	ACCOUNT(4),
	SLOGAN(5),
	ADDRESS(6),
	PAYMENT(7),
	FEE(8),
	UNIT(9),
	PHONE(10),
	EMAIL(11),
	WEBSITE(12),
	SITE_MEMBERSHIP(13),
	LOG(14),
	DEPARTMENT(15),
	USER_GROUP(16),
	USER(17),
	LEAD(18),
	SETTING(19),
	SITE_TYPE(20),
	SALE(21),
	PRODUCT(22),
	TRANSACTION(23),
	PRODUCT_GROUP(24),
	CREDIT_CARD(25),
	ADDRESS_MATCH(26),
	EMAIL_MATCH(27),
	PHONE_MATCH(28),
	MANUFACTURER(30),
	STATE(31),
	COUNTRY(32),
	IMAGE(33),
	PRODUCT_SOLUTION(34),
	SITE_PRODUCT_SOLUTION(35),
	SITE_PRODUCT(36),
	SITE_PRODUCT_GROUP(37),
	FILE_EXTENSION(38),
	MIME_TYPE(39),
	IMAGE_TYPE(40)
}
