package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "tabtypes")
public class TabType implements Serializable
{

        @Id
        @Column(name = "typeId", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long typeId;

        @Column(name = "type", nullable = false)
        private String type;
        
        /** Creates a new instance of TabType */
        public TabType()
        {
        }

        /**
         * Creates a new instance of TabType with the specified values.
         * @param typeId the typeId of the TabType
         */
        public TabType(Long typeId)
        {
                this.typeId = typeId;
        }

        /**
         * Creates a new instance of TabType with the specified values.
         * @param typeId the typeId of the TabType
         * @param type the type of the TabType
         */
        public TabType(Long typeId, String type)
        {
                this.typeId = typeId;
                this.type = type;
        }

        /**
         * Gets the typeId of this TabType.
         * @return the typeId
         */
        public Long getTypeId()
        {
                return this.typeId;
        }

        /**
         * Sets the typeId of this TabType to the specified value.
         * @param typeId the new typeId
         */
        public void setTypeId(Long typeId)
        {
                this.typeId = typeId;
        }

        /**
         * Gets the type of this TabType.
         * @return the type
         */
        public String getType()
        {
                return this.type;
        }

        /**
         * Sets the type of this TabType to the specified value.
         * @param type the new type
         */
        public void setType(String type)
        {
                this.type = type;
        }

        /**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.typeId != null ? this.typeId.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this TabType.  The result is 
         * <code>true</code> if and only if the argument is not null and is a TabType object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof TabType)) {
                        return false;
                }
                TabType other = (TabType)object;
                if (this.typeId != other.typeId && (this.typeId == null || !this.typeId.equals(other.typeId))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.TabType[typeId=" + typeId + "]";
        }
        
}
