package com.jimwebguru.iberis.core.persistence

import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object JPA_EMF
{
	private val emfInstance = Persistence.createEntityManagerFactory("IberisDev")

	fun get(): EntityManagerFactory
	{
		return emfInstance
	}
}
