package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "systemunits")
public class SystemUnit implements Serializable 
{
        @Id
        @Column(name = "unitID", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long unitID;

        @Column(name = "unitName")
        private String unitName;

        @Column(name = "singularName", nullable = false)
        private String singularName;

        @Column(name = "pluralName", nullable = false)
        private String pluralName;

        @Column(name = "enabled")
        private Boolean enabled;
        
        @Column(name="defaultGridId")
        private Long defaultGridId;
        
        @Column(name="defaultFormId")
        private Long defaultFormId;
        
        @Column(name="defaultFormPage")
        private String defaultFormPage;
        
        @Column(name="pluralIcon")
        private String pluralIcon;
        
        @Column(name="singularIcon")
        private String singularIcon;
        
        @Column(name="lookupDisplayPropertyId")
        private Long lookupDisplayPropertyId;
        
        @Column(name="lookupPropertyPrimaryKey")
        private Long lookupPropertyPrimaryKey;

        @Column(name = "persistenceClass", nullable = false)
        private String persistenceClass;

        @Column(name = "lookupPropertyPrimaryKey2")
        private Long lookupPropertyPrimaryKey2;

		@Column(name = "tableName")
		private String tableName;

		@Column(name = "locked")
		private Boolean locked;

		@JoinTable(name = "systemunitsareas",
			joinColumns =
					{
							@JoinColumn(name = "systemUnitId", referencedColumnName = "unitID")
					},
			inverseJoinColumns =
					{
							@JoinColumn(name = "systemAreaId", referencedColumnName = "areaId")
					})
		@ManyToMany
		private Collection<SystemArea> systemAreas;

		@OneToMany(mappedBy = "unit", fetch = FetchType.EAGER)
		private Collection<UnitSiteAssociation> unitSiteAssociations;

		@Column(name = "created")
		@Temporal(TemporalType.TIMESTAMP)
		private Date created;

		@Column(name = "modified")
		@Temporal(TemporalType.TIMESTAMP)
		private Date modified;

        /** Creates a new instance of SystemUnit */
        public SystemUnit() 
        {
        }

        /**
         * Creates a new instance of SystemUnit with the specified values.
         * @param unitID the unitID of the SystemUnit
         */
        public SystemUnit(Long unitID) 
        {
                this.unitID = unitID;
        }

        /**
         * Creates a new instance of SystemUnit with the specified values.
         * @param unitID the unitID of the SystemUnit
         * @param singularName the singularName of the SystemUnit
         * @param pluralName the pluralName of the SystemUnit
         */
        public SystemUnit(Long unitID, String singularName, String pluralName) 
        {
                this.unitID = unitID;
                this.singularName = singularName;
                this.pluralName = pluralName;
        }

        /**
         * Gets the unitID of this SystemUnit.
         * @return the unitID
         */
        public Long getUnitID() 
        {
                return this.unitID;
        }

        /**
         * Sets the unitID of this SystemUnit to the specified value.
         * @param unitID the new unitID
         */
        public void setUnitID(Long unitID) 
        {
                this.unitID = unitID;
        }

        /**
         * Gets the unitName of this SystemUnit.
         * @return the unitName
         */
        public String getUnitName() 
        {
                return this.unitName;
        }

        /**
         * Sets the unitName of this SystemUnit to the specified value.
         * @param unitName the new unitName
         */
        public void setUnitName(String unitName) 
        {
                this.unitName = unitName;
        }

        /**
         * Gets the singularName of this SystemUnit.
         * @return the singularName
         */
        public String getSingularName() 
        {
                return this.singularName;
        }

        /**
         * Sets the singularName of this SystemUnit to the specified value.
         * @param singularName the new singularName
         */
        public void setSingularName(String singularName) 
        {
                this.singularName = singularName;
        }

        /**
         * Gets the pluralName of this SystemUnit.
         * @return the pluralName
         */
        public String getPluralName() 
        {
                return this.pluralName;
        }

        /**
         * Sets the pluralName of this SystemUnit to the specified value.
         * @param pluralName the new pluralName
         */
        public void setPluralName(String pluralName) 
        {
                this.pluralName = pluralName;
        }

        public String getPersistenceClass()
        {
            return persistenceClass;
        }

        public void setPersistenceClass(String persistenceClass)
        {
            this.persistenceClass = persistenceClass;
        }
    
        /**
         * Gets the enabled of this SystemUnit.
         * @return the enabled
         */
        public Boolean getEnabled() 
        {
                return this.enabled;
        }

        /**
         * Sets the enabled of this SystemUnit to the specified value.
         * @param enabled the new enabled
         */
        public void setEnabled(Boolean enabled) 
        {
                this.enabled = enabled;
        }

        public Long getDefaultGridId() 
        {
                return defaultGridId;
        }

        public void setDefaultGridId(Long defaultGridId) 
        {
                this.defaultGridId = defaultGridId;
        }

        public Long getDefaultFormId() 
        {
                return defaultFormId;
        }

        public void setDefaultFormId(Long defaultFormId) 
        {
                this.defaultFormId = defaultFormId;
        }

        public String getDefaultFormPage()
        {
                return defaultFormPage;
        }

        public void setDefaultFormPage(String defaultFormPage)
        {
                this.defaultFormPage = defaultFormPage;
        }

        public String getPluralIcon()
        {
                return pluralIcon;
        }

        public void setPluralIcon(String pluralIcon)
        {
                this.pluralIcon = pluralIcon;
        }
        
        public String getSingularIcon()
        {
                return singularIcon;
        }

        public void setSingularIcon(String singularIcon)
        {
                this.singularIcon = singularIcon;
        }

        public Long getLookupDisplayPropertyId()
        {
            return lookupDisplayPropertyId;
        }

        public void setLookupDisplayPropertyId(Long flexibleLookupPropertyId)
        {
            this.lookupDisplayPropertyId = flexibleLookupPropertyId;
        }

        public Long getLookupPropertyPrimaryKey()
        {
            return lookupPropertyPrimaryKey;
        }

        public void setLookupPropertyPrimaryKey(Long flexibleLookupPropertyPrimaryKey)
        {
            this.lookupPropertyPrimaryKey = flexibleLookupPropertyPrimaryKey;
        }

        public Long getLookupPropertyPrimaryKey2()
        {
            return lookupPropertyPrimaryKey2;
        }

        public void setLookupPropertyPrimaryKey2(Long lookupPropertyPrimaryKey2)
        {
            this.lookupPropertyPrimaryKey2 = lookupPropertyPrimaryKey2;
        }

		public Collection<SystemArea> getSystemAreas()
		{
			return systemAreas;
		}

		public void setSystemAreas(Collection<SystemArea> systemAreas)
		{
			this.systemAreas = systemAreas;
		}

		public String getTableName()
		{
			return tableName;
		}

		public void setTableName(String tableName)
		{
			this.tableName = tableName;
		}

		public Boolean getLocked()
		{
			return locked;
		}

		public void setLocked(Boolean locked)
		{
			this.locked = locked;
		}

		public Collection<UnitSiteAssociation> getUnitSiteAssociations()
		{
			return unitSiteAssociations;
		}

		public void setUnitSiteAssociations(Collection<UnitSiteAssociation> unitSiteAssociations)
		{
			this.unitSiteAssociations = unitSiteAssociations;
		}

		public Date getCreated()
		{
			return created;
		}

		public void setCreated(Date created)
		{
			this.created = created;
		}

		public Date getModified()
		{
			return modified;
		}

		public void setModified(Date modified)
		{
			this.modified = modified;
		}

		@SuppressWarnings("unused")
		@PrePersist
		private void onInsert() {
			this.created = new Date();
			this.modified = this.created;
		}

		@SuppressWarnings("unused")
		@PreUpdate
		private void onUpdate() {
			this.modified = new Date();
		}

		/**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode() 
        {
                int hash = 0;
                hash += (this.unitID != null ? this.unitID.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this SystemUnit.  The result is 
         * <code>true</code> if and only if the argument is not null and is a SystemUnit object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object) 
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof SystemUnit)) 
                {
                        return false;
                }
                SystemUnit other = (SystemUnit)object;
                if (this.unitID != other.unitID && (this.unitID == null || !this.unitID.equals(other.unitID))) 
                {
                        return false;
                }
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString() 
        {
                return "com.bizznetworx.iberis.core.SystemUnit[unitID=" + unitID + "]";
        }
        
}
