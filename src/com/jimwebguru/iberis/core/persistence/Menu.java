package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "menus")
@XmlRootElement
public class Menu implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	@Basic(optional = false)
	@Column(name = "type")
	private Integer type;

	@Column(name = "orientation")
	private String orientation;

	@Column(name = "buttonText")
	private String buttonText;

	@Column(name = "width")
	private Integer width;

	@Column(name = "subWidth")
	private Integer subWidth;

	@Column(name = "height")
	private Integer height;

	@Column(name = "zIndex")
	private Integer zIndex;

	@Column(name = "createMobile")
	private Boolean createMobile;

	@Column(name = "toggleable")
	private Boolean toggleable;

	@OneToMany(mappedBy = "parentMenu", fetch = FetchType.EAGER)
	@OrderBy("sequenceIndex")
	private Collection<MenuItem> menuItemCollection;

	@ManyToOne
	@JoinColumn(name = "menuToCrumb", referencedColumnName = "id")
	private Menu menuToCrumb;

	@JoinTable(name = "menusites", joinColumns =
			{
					@JoinColumn(name = "menuId", referencedColumnName = "id")
			}, inverseJoinColumns =
			{
					@JoinColumn(name = "siteId", referencedColumnName = "siteID")
			})
	@ManyToMany
	private Collection<Site> sites;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Menu()
	{
	}

	public Menu(Long id)
	{
		this.id = id;
	}

	public Menu(Long id, String name)
	{
		this.id = id;
		this.name = name;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getType()
	{
		return type;
	}

	public void setType(Integer type)
	{
		this.type = type;
	}

	public String getOrientation()
	{
		return orientation;
	}

	public void setOrientation(String orientation)
	{
		this.orientation = orientation;
	}

	public String getButtonText()
	{
		return buttonText;
	}

	public void setButtonText(String buttonText)
	{
		this.buttonText = buttonText;
	}

	public Integer getWidth()
	{
		return width;
	}

	public void setWidth(Integer width)
	{
		this.width = width;
	}

	public Integer getSubWidth()
	{
		return subWidth;
	}

	public void setSubWidth(Integer subWidth)
	{
		this.subWidth = subWidth;
	}

	public Integer getHeight()
	{
		return height;
	}

	public void setHeight(Integer height)
	{
		this.height = height;
	}

	public Boolean getCreateMobile()
	{
		return createMobile;
	}

	public void setCreateMobile(Boolean createMobile)
	{
		this.createMobile = createMobile;
	}

	public Menu getMenuToCrumb()
	{
		return menuToCrumb;
	}

	public void setMenuToCrumb(Menu menuToCrumb)
	{
		this.menuToCrumb = menuToCrumb;
	}

	public Boolean getToggleable()
	{
		return toggleable;
	}

	public void setToggleable(Boolean toggleable)
	{
		this.toggleable = toggleable;
	}

	@XmlTransient
	public Collection<MenuItem> getMenuItemCollection()
	{
		return menuItemCollection;
	}

	public void setMenuItemCollection(Collection<MenuItem> menuItemCollection)
	{
		this.menuItemCollection = menuItemCollection;
	}

	public Collection<Site> getSites()
	{
		return sites;
	}

	public void setSites(Collection<Site> sites)
	{
		this.sites = sites;
	}

	public Integer getZIndex()
	{
		return zIndex;
	}

	public void setZIndex(Integer zIndex)
	{
		this.zIndex = zIndex;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public int hashCode()
	{
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object)
	{
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Menu))
		{
			return false;
		}
		Menu other = (Menu) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
		{
			return false;
		}
		return true;
	}

	@Override
	public String toString()
	{
		return "com.jimwebguru.iberis.core.persistence.Menu[ id=" + id + " ]";
	}

}
