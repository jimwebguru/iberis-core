/*
 * SiteType.java
 *
 * Created on May 19, 2007, 9:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "sitetypes")
public class SiteType implements Serializable
{

        @Id
        @Column(name = "typeID", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long typeID;

        @Lob
        @Column(name = "notes")
        private String notes;

        @Column(name = "type")
        private String type;
        
        /** Creates a new instance of SiteType */
        public SiteType()
        {
        }

        /**
         * Creates a new instance of SiteType with the specified values.
         * @param typeID the typeID of the SiteType
         */
        public SiteType(Long typeID)
        {
                this.typeID = typeID;
        }

        /**
         * Gets the typeID of this SiteType.
         * @return the typeID
         */
        public Long getTypeID()
        {
                return this.typeID;
        }

        /**
         * Sets the typeID of this SiteType to the specified value.
         * @param typeID the new typeID
         */
        public void setTypeID(Long typeID)
        {
                this.typeID = typeID;
        }

        /**
         * Gets the notes of this SiteType.
         * @return the notes
         */
        public String getNotes()
        {
                return this.notes;
        }

        /**
         * Sets the notes of this SiteType to the specified value.
         * @param notes the new notes
         */
        public void setNotes(String notes)
        {
                this.notes = notes;
        }

        /**
         * Gets the type of this SiteType.
         * @return the type
         */
        public String getType()
        {
                return this.type;
        }

        /**
         * Sets the type of this SiteType to the specified value.
         * @param type the new type
         */
        public void setType(String type)
        {
                this.type = type;
        }

        /**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.typeID != null ? this.typeID.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this SiteType.  The result is 
         * <code>true</code> if and only if the argument is not null and is a SiteType object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof SiteType)) {
                        return false;
                }
                SiteType other = (SiteType)object;
                if (this.typeID != other.typeID && (this.typeID == null || !this.typeID.equals(other.typeID))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.SiteType[typeID=" + typeID + "]";
        }
        
}
