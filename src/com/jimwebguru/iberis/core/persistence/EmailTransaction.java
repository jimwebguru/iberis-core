/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "emailtransactions")
public class EmailTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transactionId")
    private Long transactionId;

    @Basic(optional = false)
    @Column(name = "subject")
    private String subject;

    @Basic(optional = false)
    @Lob
    @Column(name = "body")
    private String body;

    @Basic(optional = false)
    @Column(name = "fromEmail")
    private String fromEmail;
    
    @Column(name = "siteId")
    private Long siteId;

    @Basic(optional = false)
    @Column(name = "toEmail")
    private String toEmail;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emailTransaction")
    private Collection<AdditionalEmailRecipient> additionalEmailRecipientCollection;

    @Column(name = "emailSent")
    private boolean emailSent;

    public EmailTransaction()
    {
    }

    public EmailTransaction(Long transactionId)
    {
        this.transactionId = transactionId;
    }

    public EmailTransaction(Long transactionId, String subject, String body, String from, String to)
    {
        this.transactionId = transactionId;
        this.subject = subject;
        this.body = body;
        this.fromEmail = from;
        this.toEmail = to;
    }

    public Long getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(Long transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }

    public String getFromEmail()
    {
        return fromEmail;
    }

    public void setFromEmail(String from)
    {
        this.fromEmail = from;
    }

    public Long getSiteId()
    {
        return siteId;
    }

    public void setSiteId(Long siteId)
    {
        this.siteId = siteId;
    }

    public String getToEmail()
    {
        return toEmail;
    }

    public void setToEmail(String to)
    {
        this.toEmail = to;
    }

    public boolean getEmailSent()
    {
        return emailSent;
    }

    public void setEmailSent(boolean emailSent)
    {
        this.emailSent = emailSent;
    }

    public Collection<AdditionalEmailRecipient> getAdditionalEmailRecipientCollection()
    {
        return additionalEmailRecipientCollection;
    }

    public void setAdditionalEmailRecipientCollection(Collection<AdditionalEmailRecipient> additionalEmailRecipientCollection)
    {
        this.additionalEmailRecipientCollection = additionalEmailRecipientCollection;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (transactionId != null ? transactionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmailTransaction))
        {
            return false;
        }
        EmailTransaction other = (EmailTransaction) object;
        if ((this.transactionId == null && other.transactionId != null) || (this.transactionId != null && !this.transactionId.equals(other.transactionId)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.EmailTransaction[transactionId=" + transactionId + "]";
    }

}
