/*
 * Manufacturer.java
 *
 * Created on November 23, 2007, 12:54 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "manufacturers")
public class Manufacturer implements Serializable
{

    @Id
    @Column(name = "manufacturerId", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long manufacturerId;

    @Column(name = "name", nullable = false)
    private String name;

    @JoinTable(name = "manufacturersites", joinColumns =
            {
                    @JoinColumn(name = "manufacturerId", referencedColumnName = "manufacturerId")
            }, inverseJoinColumns =
            {
                    @JoinColumn(name = "siteId", referencedColumnName = "siteID")
            })
    @ManyToMany
    private Collection<Site> sites;
    
    /** Creates a new instance of Manufacturer */
    public Manufacturer()
    {
    }

    /**
     * Creates a new instance of Manufacturer with the specified values.
     * @param manufacturerId the manufacturerId of the Manufacturer
     */
    public Manufacturer(Long manufacturerId)
    {
        this.manufacturerId = manufacturerId;
    }

    /**
     * Creates a new instance of Manufacturer with the specified values.
     * @param manufacturerId the manufacturerId of the Manufacturer
     * @param name the name of the Manufacturer
     */
    public Manufacturer(Long manufacturerId, String name)
    {
        this.manufacturerId = manufacturerId;
        this.name = name;
    }

    /**
     * Gets the manufacturerId of this Manufacturer.
     * @return the manufacturerId
     */
    public Long getManufacturerId()
    {
        return this.manufacturerId;
    }

    /**
     * Sets the manufacturerId of this Manufacturer to the specified value.
     * @param manufacturerId the new manufacturerId
     */
    public void setManufacturerId(Long manufacturerId)
    {
        this.manufacturerId = manufacturerId;
    }

    /**
     * Gets the name of this Manufacturer.
     * @return the name
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Sets the name of this Manufacturer to the specified value.
     * @param name the new name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    public Collection<Site> getSites()
    {
        return sites;
    }

    public void setSites(Collection<Site> sites)
    {
        this.sites = sites;
    }

    /**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (this.manufacturerId != null ? this.manufacturerId.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Manufacturer.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Manufacturer object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Manufacturer)) {
            return false;
        }
        Manufacturer other = (Manufacturer)object;
        if (this.manufacturerId != other.manufacturerId && (this.manufacturerId == null || !this.manufacturerId.equals(other.manufacturerId))) return false;
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.Manufacturer[manufacturerId=" + manufacturerId + "]";
    }
    
}
