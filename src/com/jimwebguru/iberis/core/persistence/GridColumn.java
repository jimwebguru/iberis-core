/*
 * GridColumn.java
 *
 * Created on December 7, 2006, 10:28 AM
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "gridcolumns")
public class GridColumn implements Serializable 
{

    @Id
    @Column(name = "gridColumnId", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long gridColumnId;

    @JoinColumn(name = "unitProperty", referencedColumnName = "propertyId")
    @ManyToOne
    private UnitProperty unitProperty;
    
    @ManyToOne
    @JoinColumn(name="gridId")
    private Grid grid;

	@Column(name = "sequence")
	private Integer sequence;
    
    /** Creates a new instance of GridColumn */
    public GridColumn() 
    {
    }

    /**
     * Creates a new instance of GridColumn with the specified values.
     * @param gridColumnId the gridColumnId of the GridColumn
     */
    public GridColumn(Long gridColumnId) 
    {
        this.gridColumnId = gridColumnId;
    }


    /**
     * Gets the gridColumnId of this GridColumn.
     * @return the gridColumnId
     */
    public Long getGridColumnId() 
    {
        return this.gridColumnId;
    }

    /**
     * Sets the gridColumnId of this GridColumn to the specified value.
     * @param gridColumnId the new gridColumnId
     */
    public void setGridColumnId(Long gridColumnId) 
    {
        this.gridColumnId = gridColumnId;
    }

    /**
     * Gets the unitProperty of this GridColumn.
     * @return the unitProperty
     */
    public UnitProperty getUnitProperty() 
    {
        return this.unitProperty;
    }

    /**
     * Sets the unitProperty of this GridColumn to the specified value.
     * @param unitProperty the new unitProperty
     */
    public void setUnitProperty(UnitProperty unitProperty) 
    {
        this.unitProperty = unitProperty;
    }

    public Grid getGrid() 
    {
        return grid;
    }

    public void setGrid(Grid grid) 
    {
        this.grid = grid;
    }

	public Integer getSequence()
	{
		return sequence;
	}

	public void setSequence(Integer sequence)
	{
		this.sequence = sequence;
	}

	/**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.gridColumnId != null ? this.gridColumnId.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this GridColumn.  The result is 
     * <code>true</code> if and only if the argument is not null and is a GridColumn object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GridColumn)) 
        {
            return false;
        }
        
        GridColumn other = (GridColumn)object;
        
        if (this.gridColumnId != other.gridColumnId && (this.gridColumnId == null || !this.gridColumnId.equals(other.gridColumnId))) return false;
        {
            return true;
        }
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.GridColumn[gridColumnId=" + gridColumnId + "]";
    }
    
}
