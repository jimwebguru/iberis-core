package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "usergroups")
public class UserGroup
{
	@Id
	@Column(name = "groupID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long groupId;

	@JoinColumn(name = "parentID", referencedColumnName = "groupID")
	@ManyToOne
	private UserGroup parentGroup;

	@Column(name = "description")
	private String description;

	@Column(name = "groupName")
	private String groupName;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Long getGroupId()
	{
		return groupId;
	}

	public void setGroupId(Long groupId)
	{
		this.groupId = groupId;
	}

	public UserGroup getParentGroup()
	{
		return parentGroup;
	}

	public void setParentGroup(UserGroup parentGroup)
	{
		this.parentGroup = parentGroup;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getGroupName()
	{
		return groupName;
	}

	public void setGroupName(String groupName)
	{
		this.groupName = groupName;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		UserGroup userGroup = (UserGroup) o;

		if (groupId != null ? !groupId.equals(userGroup.groupId) : userGroup.groupId != null)
		{
			return false;
		}
		if (parentGroup != null ? !parentGroup.equals(userGroup.parentGroup) : userGroup.parentGroup != null)
		{
			return false;
		}
		if (description != null ? !description.equals(userGroup.description) : userGroup.description != null)
		{
			return false;
		}
		if (groupName != null ? !groupName.equals(userGroup.groupName) : userGroup.groupName != null)
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = groupId != null ? groupId.hashCode() : 0;
		result = 31 * result + (parentGroup != null ? parentGroup.getGroupId().hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
		return result;
	}
}
