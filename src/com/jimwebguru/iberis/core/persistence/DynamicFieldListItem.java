/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "dynamicfieldlistitems")
public class DynamicFieldListItem implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @Column(name = "itemValue")
    private String itemValue;

    @Basic(optional = false)
    @Column(name = "displayValue")
    private String displayValue;

    @JoinColumn(name = "fieldId", referencedColumnName = "id")
    @ManyToOne
    private DynamicFormField field;

    public DynamicFieldListItem()
    {
    }

    public DynamicFieldListItem(Long id)
    {
        this.id = id;
    }

    public DynamicFieldListItem(Long id, String value, String displayValue)
    {
        this.id = id;
        this.itemValue = value;
        this.displayValue = displayValue;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getItemValue()
    {
        return itemValue;
    }

    public void setItemValue(String value)
    {
        this.itemValue = value;
    }

    public String getDisplayValue()
    {
        return displayValue;
    }

    public void setDisplayValue(String displayValue)
    {
        this.displayValue = displayValue;
    }

    public DynamicFormField getField()
    {
        return field;
    }

    public void setField(DynamicFormField field)
    {
        this.field = field;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DynamicFieldListItem))
        {
            return false;
        }

        DynamicFieldListItem other = (DynamicFieldListItem) object;

        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "com.bizznetworx.iberis.core.DynamicFieldListItem[id=" + id + "]";
    }

}
