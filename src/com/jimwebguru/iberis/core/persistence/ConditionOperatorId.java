/*
 * ConditionOperator.java
 *
 * Created on April 19, 2007, 1:54 PM
 */

package com.jimwebguru.iberis.core.persistence;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
public enum ConditionOperatorId
{
        NONE(0),
        GREATER_THAN_EQUAL(1),
        LESS_THAN_EQUAL(2),
        LESS_THAN(3),
        GREATER_THAN(4),
        LIKE(5),
        EQUAL(6);
        
        private final int conditionId;
        
        /** Creates a new instance of ConditionOperator */
        ConditionOperatorId(int id)
        {
                conditionId = id;
        }
        
}
