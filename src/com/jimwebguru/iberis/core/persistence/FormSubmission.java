package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "formsubmissions")
public class FormSubmission
{
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "submission")
	private String submission;

	@JoinColumn(name = "formId", referencedColumnName = "id")
	@ManyToOne
	private DynamicForm form;

	@Column(name = "created")
	//@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getSubmission()
	{
		return submission;
	}

	public void setSubmission(String submission)
	{
		this.submission = submission;
	}

	public DynamicForm getForm()
	{
		return form;
	}

	public void setForm(DynamicForm form)
	{
		this.form = form;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}
}
