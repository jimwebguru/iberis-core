/*
 *
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "contentblocksections")
public class ContentBlockSection implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "sequence")
    private Integer sequence;

	@Column(name = "showTitle")
	private Boolean showTitle;

    @JoinColumn(name = "contentId", referencedColumnName = "id")
    @ManyToOne
    private Content content;

    @OneToMany(mappedBy = "contentBlockSection")
    @OrderBy("sequence")
    private Collection<ContentBlock> contentBlocks;

    /** Creates a new instance of Form */
    public ContentBlockSection()
    {
    }

    /**
     * Creates a new instance of Form with the specified values.
     * @param id the formId of the Form
     */
    public ContentBlockSection(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public  Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public Content getContent()
    {
        return content;
    }

    public void setContent(Content content)
    {
        this.content = content;
    }

	public Boolean getShowTitle()
	{
		return showTitle;
	}

	public void setShowTitle(Boolean showTitle)
	{
		this.showTitle = showTitle;
	}

    public Collection<ContentBlock> getContentBlocks()
    {
        return contentBlocks;
    }

    public void setContentBlocks(Collection<ContentBlock> contentBlocks)
    {
        this.contentBlocks = contentBlocks;
    }

    /**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Form.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Form object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContentBlockSection))
        {
            return false;
        }
        
        ContentBlockSection other = (ContentBlockSection)object;
        
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.ContentBlockSection[id=" + id + "]";
    }
    
}
