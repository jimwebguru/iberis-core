/*
 * Site.java
 *
 * Created on May 19, 2007, 9:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "sites")
public class Site implements Serializable
{

        @Id
        @Column(name = "siteID", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long siteID;

        @Column(name = "siteName")
        private String siteName;

        @Column(name = "url")
        private String url;

        @JoinColumn(name = "siteType", referencedColumnName = "typeID")
        @ManyToOne
        private SiteType siteType;

        @Column(name = "mailFrom")
        private String mailFrom;

        @Column(name = "mailHost")
        private String mailHost;

        @Column(name = "mailPort")
        private Integer mailPort;

        @Column(name = "mailUseSSL")
        private Boolean mailUseSSL;

		@Column(name = "mailPlatform")
		private Integer mailPlatform;

		@Column(name = "mailUser")
		private String mailUser;

		@Column(name = "mailUserPassword")
		private String mailUserPassword;

        @JoinColumn(name = "frontPageTemplateId", referencedColumnName = "id")
        @ManyToOne
        private Layout frontPageTemplate;

        @JoinColumn(name = "innerPageTemplateId", referencedColumnName = "id")
        @ManyToOne
        private Layout innerPageTemplate;

        @JoinColumn(name = "frontPageContent", referencedColumnName = "id")
        @ManyToOne
        private Content frontPageContent;

        @Column(name = "showFrontPageContent")
        private Boolean showFrontPageContent;

        @JoinColumn(name = "logoId", referencedColumnName = "imageid")
        @ManyToOne
        private Image logo;

        @Column(name = "allowRegistration")
        private Boolean allowRegistration;

        @Column(name = "emailAsUsername")
        private Boolean emailAsUsername;

        @OneToMany(mappedBy = "site", fetch = FetchType.EAGER)
        private Collection<UnitSiteAssociation> unitSiteAssociations;

		@Column(name = "metaTitle")
		private String metaTitle;

		@Column(name = "metaDescription")
		private String metaDescription;

		@Lob
		@Column(name = "customLoginHtml")
		private String customLoginHtml;

		@Column(name = "created")
		@Temporal(TemporalType.TIMESTAMP)
		private Date created;

		@Column(name = "modified")
		@Temporal(TemporalType.TIMESTAMP)
		private Date modified;

		@Column(name = "centerRegions")
		private Boolean centerRegions;
        
        /** Creates a new instance of Site */
        public Site()
        {
        }

        /**
         * Creates a new instance of Site with the specified values.
         * @param siteID the siteID of the Site
         */
        public Site(Long siteID)
        {
                this.siteID = siteID;
        }

        /**
         * Gets the siteID of this Site.
         * @return the siteID
         */
        public Long getSiteID()
        {
                return this.siteID;
        }

        /**
         * Sets the siteID of this Site to the specified value.
         * @param siteID the new siteID
         */
        public void setSiteID(Long siteID)
        {
                this.siteID = siteID;
        }

        /**
         * Gets the siteName of this Site.
         * @return the siteName
         */
        public String getSiteName()
        {
                return this.siteName;
        }

        /**
         * Sets the siteName of this Site to the specified value.
         * @param siteName the new siteName
         */
        public void setSiteName(String siteName)
        {
                this.siteName = siteName;
        }

        /**
         * Gets the url of this Site.
         * @return the url
         */
        public String getUrl()
        {
                return this.url;
        }

        /**
         * Sets the url of this Site to the specified value.
         * @param url the new url
         */
        public void setUrl(String url)
        {
                this.url = url;
        }

        /**
         * Gets the siteType of this Site.
         * @return the siteType
         */
        public SiteType getSiteType()
        {
                return this.siteType;
        }

        /**
         * Sets the siteType of this Site to the specified value.
         * @param siteType the new siteType
         */
        public void setSiteType(SiteType siteType)
        {
                this.siteType = siteType;
        }

        public String getMailFrom()
        {
                return mailFrom;
        }

        public void setMailFrom(String mailFrom)
        {
                this.mailFrom = mailFrom;
        }

        public String getMailHost()
        {
                return mailHost;
        }

        public void setMailHost(String mailHost)
        {
                this.mailHost = mailHost;
        }

        public Integer getMailPort()
        {
                return mailPort;
        }

        public void setMailPort(Integer mailPort)
        {
                this.mailPort = mailPort;
        }

        public Boolean getMailUseSSL()
        {
                return mailUseSSL;
        }

        public void setMailUseSSL(Boolean mailUseSSL)
        {
                this.mailUseSSL = mailUseSSL;
        }

        public Layout getFrontPageTemplate()
        {
                return frontPageTemplate;
        }

        public void setFrontPageTemplate(Layout frontPageTemplate)
        {
                this.frontPageTemplate = frontPageTemplate;
        }

        public Layout getInnerPageTemplate()
        {
                return innerPageTemplate;
        }

        public void setInnerPageTemplate(Layout innerPageTemplate)
        {
                this.innerPageTemplate = innerPageTemplate;
        }

        public Content getFrontPageContent()
        {
                return frontPageContent;
        }

        public void setFrontPageContent(Content frontPageContent)
        {
                this.frontPageContent = frontPageContent;
        }

        public Boolean getShowFrontPageContent()
        {
                return showFrontPageContent;
        }

        public void setShowFrontPageContent(Boolean showFrontPageContent)
        {
                this.showFrontPageContent = showFrontPageContent;
        }

        public Image getLogo()
        {
                return logo;
        }

        public void setLogo(Image logo)
        {
                this.logo = logo;
        }

        public Collection<UnitSiteAssociation> getUnitSiteAssociations()
        {
                return unitSiteAssociations;
        }

        public void setUnitSiteAssociations(Collection<UnitSiteAssociation> unitSiteAssociations)
        {
                this.unitSiteAssociations = unitSiteAssociations;
        }

        public Boolean getAllowRegistration()
        {
                return allowRegistration;
        }

        public void setAllowRegistration(Boolean allowRegistration)
        {
                this.allowRegistration = allowRegistration;
        }

        public Boolean getEmailAsUsername()
        {
                return emailAsUsername;
        }

        public void setEmailAsUsername(Boolean emailAsUsername)
        {
                this.emailAsUsername = emailAsUsername;
        }

		public String getMetaTitle()
		{
			return metaTitle;
		}

		public void setMetaTitle(String metaTitle)
		{
			this.metaTitle = metaTitle;
		}

		public String getMetaDescription()
		{
			return metaDescription;
		}

		public void setMetaDescription(String metaDescription)
		{
			this.metaDescription = metaDescription;
		}

		public Date getCreated()
		{
			return created;
		}

		public void setCreated(Date created)
		{
			this.created = created;
		}

		public Date getModified()
		{
			return modified;
		}

		public void setModified(Date modified)
		{
			this.modified = modified;
		}

		public Boolean getCenterRegions()
		{
			return centerRegions;
		}

		public void setCenterRegions(Boolean centerRegions)
		{
			this.centerRegions = centerRegions;
		}

		public String getCustomLoginHtml()
		{
			return customLoginHtml;
		}

		public void setCustomLoginHtml(String customLoginHtml)
		{
			this.customLoginHtml = customLoginHtml;
		}

		public Integer getMailPlatform()
		{
			return mailPlatform;
		}

		public void setMailPlatform(Integer mailPlatform)
		{
			this.mailPlatform = mailPlatform;
		}

		public String getMailUser()
		{
			return mailUser;
		}

		public void setMailUser(String mailUser)
		{
			this.mailUser = mailUser;
		}

		public String getMailUserPassword()
		{
			return mailUserPassword;
		}

		public void setMailUserPassword(String mailUserPassword)
		{
			this.mailUserPassword = mailUserPassword;
		}

		@SuppressWarnings("unused")
		@PrePersist
		private void onInsert() {
			this.created = new Date();
			this.modified = this.created;
		}

		@SuppressWarnings("unused")
		@PreUpdate
		private void onUpdate() {
			this.modified = new Date();
		}

		/**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.siteID != null ? this.siteID.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this Site.  The result is 
         * <code>true</code> if and only if the argument is not null and is a Site object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof Site)) {
                        return false;
                }
                Site other = (Site)object;
                if (this.siteID != other.siteID && (this.siteID == null || !this.siteID.equals(other.siteID))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.Site[siteID=" + siteID + "]";
        }
        
}
