/*
 * SiteMembership.java
 *
 * Created on May 19, 2007, 9:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "sitemembership")
public class SiteMembership implements Serializable
{

        @Id
        @Column(name = "membershipID", nullable = false)
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        private Long membershipID;

        @Column(name = "cancelDate")
        @Temporal(TemporalType.DATE)
        private Date cancelDate;

        @Column(name = "onlineDate")
        @Temporal(TemporalType.DATE)
        private Date onlineDate;

        @Column(name = "signupDate", nullable = false)
        @Temporal(TemporalType.DATE)
        private Date signupDate;

        @JoinColumn(name = "userId", referencedColumnName = "userID")
        @ManyToOne
        private User user;

        @JoinColumn(name = "siteID", referencedColumnName = "siteID")
        @ManyToOne
        private Site site;

        @JoinTable(name = "membershiproles", joinColumns =
                {
                        @JoinColumn(name = "membershipId", referencedColumnName = "membershipID")
                }, inverseJoinColumns =
                {
                        @JoinColumn(name = "roleId", referencedColumnName = "id")
                })
        @ManyToMany
        private Collection<Role> roles;

		@Column(name = "created")
		@Temporal(TemporalType.TIMESTAMP)
		private Date created;

		@Column(name = "modified")
		@Temporal(TemporalType.TIMESTAMP)
		private Date modified;
        
        /** Creates a new instance of SiteMembership */
        public SiteMembership()
        {
        }

        /**
         * Creates a new instance of SiteMembership with the specified values.
         * @param membershipID the membershipID of the SiteMembership
         */
        public SiteMembership(Long membershipID)
        {
                this.membershipID = membershipID;
        }

        /**
         * Creates a new instance of SiteMembership with the specified values.
         * @param membershipID the membershipID of the SiteMembership
         * @param signupDate the signupDate of the SiteMembership
         */
        public SiteMembership(Long membershipID, Date signupDate)
        {
                this.membershipID = membershipID;
                this.signupDate = signupDate;
        }

        /**
         * Gets the membershipID of this SiteMembership.
         * @return the membershipID
         */
        public Long getMembershipID()
        {
                return this.membershipID;
        }

        /**
         * Sets the membershipID of this SiteMembership to the specified value.
         * @param membershipID the new membershipID
         */
        public void setMembershipID(Long membershipID)
        {
                this.membershipID = membershipID;
        }

        /**
         * Gets the cancelDate of this SiteMembership.
         * @return the cancelDate
         */
        public Date getCancelDate()
        {
                return this.cancelDate;
        }

        /**
         * Sets the cancelDate of this SiteMembership to the specified value.
         * @param cancelDate the new cancelDate
         */
        public void setCancelDate(Date cancelDate)
        {
                this.cancelDate = cancelDate;
        }

        /**
         * Gets the onlineDate of this SiteMembership.
         * @return the onlineDate
         */
        public Date getOnlineDate()
        {
                return this.onlineDate;
        }

        /**
         * Sets the onlineDate of this SiteMembership to the specified value.
         * @param onlineDate the new onlineDate
         */
        public void setOnlineDate(Date onlineDate)
        {
                this.onlineDate = onlineDate;
        }

        /**
         * Gets the signupDate of this SiteMembership.
         * @return the signupDate
         */
        public Date getSignupDate()
        {
                return this.signupDate;
        }

        /**
         * Sets the signupDate of this SiteMembership to the specified value.
         * @param signupDate the new signupDate
         */
        public void setSignupDate(Date signupDate)
        {
                this.signupDate = signupDate;
        }

        /**
         * Gets the clientID of this SiteMembership.
         * @return the clientID
         */
        public User getUser()
        {
                return this.user;
        }

        /**
         * Sets the clientID of this SiteMembership to the specified value.
         * @param user the new clientID
         */
        public void setUser(User user)
        {
                this.user = user;
        }

        /**
         * Gets the siteID of this SiteMembership.
         * @return the siteID
         */
        public Site getSite()
        {
                return this.site;
        }

        /**
         * Sets the siteID of this SiteMembership to the specified value.
         * @param site the new siteID
         */
        public void setSite(Site site)
        {
                this.site = site;
        }

        public Collection<Role> getRoles()
        {
                return roles;
        }

        public void setRoles(Collection<Role> roles)
        {
                this.roles = roles;
        }

		public Date getCreated()
		{
			return created;
		}

		public void setCreated(Date created)
		{
			this.created = created;
		}

		public Date getModified()
		{
			return modified;
		}

		public void setModified(Date modified)
		{
			this.modified = modified;
		}

		@SuppressWarnings("unused")
		@PrePersist
		private void onInsert() {
			this.created = new Date();
			this.modified = this.created;
		}

		@SuppressWarnings("unused")
		@PreUpdate
		private void onUpdate() {
			this.modified = new Date();
		}

		/**
         * Returns a hash code value for the object.  This implementation computes 
         * a hash code value based on the id fields in this object.
         * @return a hash code value for this object.
         */
        @Override
        public int hashCode()
        {
                int hash = 0;
                hash += (this.membershipID != null ? this.membershipID.hashCode() : 0);
                return hash;
        }

        /**
         * Determines whether another object is equal to this SiteMembership.  The result is 
         * <code>true</code> if and only if the argument is not null and is a SiteMembership object that 
         * has the same id field values as this object.
         * @param object the reference object with which to compare
         * @return <code>true</code> if this object is the same as the argument;
         * <code>false</code> otherwise.
         */
        @Override
        public boolean equals(Object object)
        {
                // TODO: Warning - this method won't work in the case the id fields are not set
                if (!(object instanceof SiteMembership)) {
                        return false;
                }
                SiteMembership other = (SiteMembership)object;
                if (this.membershipID != other.membershipID && (this.membershipID == null || !this.membershipID.equals(other.membershipID))) return false;
                return true;
        }

        /**
         * Returns a string representation of the object.  This implementation constructs 
         * that representation based on the id fields.
         * @return a string representation of the object.
         */
        @Override
        public String toString()
        {
                return "com.bizznetworx.iberis.core.SiteMembership[membershipID=" + membershipID + "]";
        }
        
}
