/*
 *
 * Created on January 11, 2007, 2:27 PM
 */

package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "contentblocks")
public class ContentBlock implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "sectionId", referencedColumnName = "id")
    @ManyToOne
    private ContentBlockSection contentBlockSection;

    @Column(name = "sequence")
    private Integer sequence;

	@Column(name = "width")
	private Integer width;

	@Column(name = "pageItem")
	private Long pageItem;

	@Column(name = "pageItemInstanceId")
	private Long pageItemInstanceId;

    /** Creates a new instance of Form */
    public ContentBlock()
    {
    }

    /**
     * Creates a new instance of Form with the specified values.
     * @param id the formId of the Form
     */
    public ContentBlock(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Integer getWidth()
    {
        return width;
    }

    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

	public ContentBlockSection getContentBlockSection()
	{
		return contentBlockSection;
	}

	public void setContentBlockSection(ContentBlockSection contentBlockSection)
	{
		this.contentBlockSection = contentBlockSection;
	}

	public Long getPageItem()
	{
		return pageItem;
	}

	public void setPageItem(Long pageItem)
	{
		this.pageItem = pageItem;
	}

	public Long getPageItemInstanceId()
	{
		return pageItemInstanceId;
	}

	public void setPageItemInstanceId(Long pageItemInstanceId)
	{
		this.pageItemInstanceId = pageItemInstanceId;
	}

	/**
     * Returns a hash code value for the object.  This implementation computes 
     * a hash code value based on the id fields in this object.
     * @return a hash code value for this object.
     */
    @Override
    public int hashCode() 
    {
        int hash = 0;
        hash += (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    /**
     * Determines whether another object is equal to this Form.  The result is 
     * <code>true</code> if and only if the argument is not null and is a Form object that 
     * has the same id field values as this object.
     * @param object the reference object with which to compare
     * @return <code>true</code> if this object is the same as the argument;
     * <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object object) 
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContentBlock))
        {
            return false;
        }
        
        ContentBlock other = (ContentBlock)object;
        
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a string representation of the object.  This implementation constructs 
     * that representation based on the id fields.
     * @return a string representation of the object.
     */
    @Override
    public String toString() 
    {
        return "com.bizznetworx.iberis.core.ContentBlock[id=" + id + "]";
    }
    
}
