package com.jimwebguru.iberis.core.persistence;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Entity
@Table(name = "users")
public class User
{
	@Id
	@Column(name = "userID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long userId;

	@Column(name = "userName")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "email")
	private String email;

	@Column(name = "userSecretQuestion")
	private Integer userSecretQuestion;

	@Column(name = "userSecretAnswer")
	private String userSecretAnswer;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "middleName")
	private String middleName;

	@Column(name = "enabled")
	private Boolean enabled;

	@Column(name = "globalAdmin")
	private Boolean globalAdmin;

	@JoinTable(name = "userstogroups", joinColumns =
	{
			@JoinColumn(name = "userId", referencedColumnName = "userID")
	}, inverseJoinColumns =
	{
			@JoinColumn(name = "groupId", referencedColumnName = "groupID")
	})
	@ManyToMany
	private Collection<UserGroup> groups;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private Collection<SiteMembership> siteMemberships;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(name = "modified")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public Integer getUserSecretQuestion()
	{
		return userSecretQuestion;
	}

	public void setUserSecretQuestion(Integer userSecretQuestion)
	{
		this.userSecretQuestion = userSecretQuestion;
	}

	public String getUserSecretAnswer()
	{
		return userSecretAnswer;
	}

	public void setUserSecretAnswer(String userSecretAnswer)
	{
		this.userSecretAnswer = userSecretAnswer;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public Collection<UserGroup> getGroups()
	{
		return groups;
	}

	public void setGroups(Collection<UserGroup> groups)
	{
		this.groups = groups;
	}

	public Collection<SiteMembership> getSiteMemberships()
	{
		return siteMemberships;
	}

	public void setSiteMemberships(Collection<SiteMembership> siteMemberships)
	{
		this.siteMemberships = siteMemberships;
	}

	public Boolean getEnabled()
	{
		return enabled;
	}

	public void setEnabled(Boolean enabled)
	{
		this.enabled = enabled;
	}

	public Boolean getGlobalAdmin()
	{
		return globalAdmin;
	}

	public void setGlobalAdmin(Boolean globalAdmin)
	{
		this.globalAdmin = globalAdmin;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Date getModified()
	{
		return modified;
	}

	public void setModified(Date modified)
	{
		this.modified = modified;
	}

	@SuppressWarnings("unused")
	@PrePersist
	private void onInsert() {
		this.created = new Date();
		this.modified = this.created;
	}

	@SuppressWarnings("unused")
	@PreUpdate
	private void onUpdate() {
		this.modified = new Date();
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		User user = (User) o;

		if (userId != null ? !userId.equals(user.userId) : user.userId != null)
		{
			return false;
		}
		if (userName != null ? !userName.equals(user.userName) : user.userName != null)
		{
			return false;
		}
		if (password != null ? !password.equals(user.password) : user.password != null)
		{
			return false;
		}
		if (email != null ? !email.equals(user.email) : user.email != null)
		{
			return false;
		}

		if (userSecretQuestion != null ? !userSecretQuestion.equals(user.userSecretQuestion) : user.userSecretQuestion != null)
		{
			return false;
		}
		if (userSecretAnswer != null ? !userSecretAnswer.equals(user.userSecretAnswer) : user.userSecretAnswer != null)
		{
			return false;
		}
		if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null)
		{
			return false;
		}
		if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null)
		{
			return false;
		}
		if (middleName != null ? !middleName.equals(user.middleName) : user.middleName != null)
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = userId != null ? userId.hashCode() : 0;
		result = 31 * result + (userName != null ? userName.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (userSecretQuestion != null ? userSecretQuestion.hashCode() : 0);
		result = 31 * result + (userSecretAnswer != null ? userSecretAnswer.hashCode() : 0);
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
		return result;
	}
}
